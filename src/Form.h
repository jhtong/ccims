/**
 * @file Form.h
 * @brief Form.h is an abstraction for a form.
 *
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-17
 */
/* Copyright (c) <2012> Joel Haowen TONG
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */


#pragma once 
#ifndef _FORM_H
#define _FORM_H

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

/* FOR TESTING, DO NOT INCLUDE Gui.h */
#include "Gui.h"
#include "Screen.h"
#include "FormUI.h"

using namespace std;

namespace gui
{
    namespace form 
    {
        
        /* ----------------------------------------------------------------*/
        /**
         * @brief Form.h is an abstraction for a form.
         *
         * The Form class is able to contain derivatives of FormUI objects.  For
         * example, the Form class is able to hold components of type textfield and
         * label.  This is made possible through the internal usage of a stack, which
         * stores the form components.
         *
         * If a GUI object is passed, centering of the object on screen is possible.
         *
         * @code 
         * // Create a testForm of size 60 * 10 at the point (0,0) with a padding of 2
         * Form* testForm = new Form(10,60,0,0,2);
         *
         * // Write some instruction to screen 
         * textForm->push_static_label("This is some instruction");
         *
         * // Push a blank box
         * testForm->push_static_label("");
         *
         * // Push input fields of width 15, right-centered.
         * testForm->push_field("Name", 15);
         * testForm->push_field("Age", 15);
         * testForm->push_field("Description", 15);
         *
         * // Push a button labelled "Submit"
         * testForm->push_btn("Submit");
         *
         * // Change the position of the form to (10,10)
         * testForm->set_yx(10,10);
         *
         * // start running the testForm code
         * testForm->start();
         *
         * // Get the data from test form, stored in a vector called data 
         * vector<string> data;
         * testForm->get_data(data);
         *
         * // Input data is now stored in a vector of strings, called "data".
         *
         * @endcode
         *
         */
        /* ------------------------------------------------------------------*/
        class Form
        {
            public:
                Form(unsigned int height    = 0,            // Create a form with dimensions at (y,x)
                    unsigned int width      = 0,
                    unsigned int y          = 0,
                    unsigned int x          = 0,
                    unsigned int padding    = 0,
                    gui::Gui* root = NULL);

                ~Form();
                void            start();                    // start process
                void            go_next();                  // go to next item
                void            go_prev();                  // go to previous item 
                

                /* For creation of UI widgets */
                void            push(FormUI* arg);          // push UI to stack
                void            pop();                      // pop UI from stack 

                /* For custom creation of forms */
                void            push_field(string id, string label, 
                                            unsigned int field_padding);
                void            push_btn(string label);
                void            push_btn(string label, 
                                            bool (*actionFn)(vector<string>));
                void            push_static_label(string label);

                void            get_data(vector<string>& data_ref);

                void            set_yx(unsigned int y, unsigned int x);
                void            set_yx_center();


            private:
                bool            (*action)(vector<string>);
                void            make_data();
                void            load(unsigned int ref);     // load specified idx
                void            arrange();                  // Arrange items

                unsigned int    idx;                        // idx of stack
                gui::Screen*    _WIN_SELF;                  // Screen instance of Form.
                gui::Gui*       _ROOT;                      // Pointer to ROOT (Gui instance)
                unsigned int    _padding;                   // amount of padding to use
                vector<FormUI*> _ui_stack;                  // stack of all UI items 
                                                            // (textfield, buttons, etc.) 
                                                            // in running order
                                                            // Note these items are
                                                            // derived from a
                                                            // common parent class,
                                                            // FormUI
                                                            // (inheritance).
                vector<string>  _data_vec;
        };
    }
}

#endif

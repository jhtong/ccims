/**
 * @file Interface.h
 * @brief The Interface class acts as a facilitator between components in the
 * gui and logic namespaces.  It abstracts communication between the two
 * namespaces, allowing for them to be decoupled.
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-23
 */
/* Copyright (c) <2012> <C01-05>
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */
#pragma once
#ifndef _INTERFACE_H
#define _INTERFACE_H

#include <string.h>

#include "PrintObject.h"
#include "Gui.h"
#include "Logic.h"


namespace common
{

    /* ----------------------------------------------------------------*/
    /**
     * @brief The Interface class acts as a facilitator between components in the
     * gui and logic namespaces.  It abstracts communication between the two
     * namespaces, allowing for them to be decoupled.
     */
    /* ------------------------------------------------------------------*/
    class Interface 
    {
        public:
            static Interface* get_instance();
            void start();
            void quit();

            /* CLASSES FROM LOGIC --> GUI */
            bool logToGui_redraw_screen          (PrintObject new_list);
            bool logToGui_update_status          (string new_status, unsigned int printType = 0);
            void logToGui_load_file();

            /* CLASSES FROM GUI --> LOGIC */
            bool guiToLog_add_item               (vector<string> arg);
            bool guiToLog_scrap_item             (vector<string> arg);
            bool guiToLog_specify_sales          (vector<string> arg);
            bool guiToLog_restock_item           (vector<string> arg);
            bool guiToLog_search_terms           (vector<string> arg);

            bool guiToLog_best_product();
            bool guiToLog_best_manufacturer();
            bool guiToLog_best_product_category();

            /* For batch job */
            bool guiToLog_run_batch();

            /* For form validation stuff */
            bool guiToLog_validate_unique(vector<string> arg);

            bool guiToLog_toggle_dbMode();

            void wait();


        private:
            Interface();
            ~Interface();

            /* Not to be implemented */
            Interface(Interface const&);            // copy constructor
            static Interface* interface;

            static gui::Gui*                   gui;
            static logic::Logic*               logic;

    };

}
#endif

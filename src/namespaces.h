/**
 * @file namespaces.h
 * @brief List of namespaces.  Centralized solely for Doxygen documentation.
 * @author CCIMS 01/05
 * @version 0.0.001
 * @date 2013-02-21
 */


/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _NAMESPACES_H
#define _NAMESPACES_H

/* ----------------------------------------------------------------*/
/**
 * @brief This namespace holds the classes of the GUI
 */
/* ------------------------------------------------------------------*/
namespace gui 
{
    /* ----------------------------------------------------------------*/
    /**
     * @brief This namespace holds the classes associated with the form
     */
    /* ------------------------------------------------------------------*/
    namespace form {}
}


/* ----------------------------------------------------------------*/
/**
 * @brief This namespace contains all the classes, flags and functions
 * associated with the backend.  Not to be accessed drectly, and should only be
 * accessed via common::Interface outside the namespace.
 */
/* ------------------------------------------------------------------*/
namespace logic 
{
    /* ----------------------------------------------------------------*/
    /**
     * @brief This namespace holds all the abstract data types associated with
     * the databases.
     */
    /* ------------------------------------------------------------------*/
    namespace adt {}
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This namespace defines the global color pairs 
 * that are used by ncurses.  To use these color pairs, call
 * ColorPairs::init() to generate the color pairs.  Then, use 
 * the color pairs defined below.
 *
 * Each color pair has a signature of starting with the name 
 * COLOR_PAIR .
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
/* ----------------------------------------------------------------------------*/
namespace ColorPairs {}


/* ----------------------------------------------------------------*/
/**
 * @brief This namespace holds the common classes shared between the GUI and
 * backend.
 */
/* ------------------------------------------------------------------*/
namespace common 
{
    /* ----------------------------------------------------------------*/
    /**
     * @brief This namespace holds the auxiliary functions used by every class,
     * in general (For example, type-conversion functions, splice functions,
     * etc.)
     */
    /* ------------------------------------------------------------------*/
    namespace utils 
    {
        /* ----------------------------------------------------------------*/
        /**
         * @brief This namespace holds the utility functions used in form.
         */
        /* ------------------------------------------------------------------*/
        namespace form {}
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief This namespace holds all the error-checking functions of ccims.
     */
    /* ------------------------------------------------------------------*/
    namespace errorChecker {}
}

#endif

/**
 * @file MenuHandler.h
 * @brief This class handles keyboard call events for the GUI class
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _MENU_HANDLER_H
#define _MENU_HANDLER_H


namespace gui {
    
    class Gui;
    namespace form 
    {
        class FormFactory;
    }

    /* ----------------------------------------------------------------*/
    /**
     * @brief This class handles the keyboard interrupts generated by the GUI
     * frontend.  Interrupts are generated by the ScrollList instance, which
     * are then transferred to the MenuHandler instance.
     *
     * In a way, the MenuHandler is an event listener.
     *
     */
    /* ------------------------------------------------------------------*/
    class MenuHandler {
        public:
            MenuHandler();
            MenuHandler(Gui* root);
            ~MenuHandler();

            void set_root(Gui* _ROOT);
            bool process();

        private:
            Gui*                                _ROOT;
            form::FormFactory*                  _formFactory;
    
    };

}

#endif

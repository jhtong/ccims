/**
* @file BatchJob.h
* @brief Batch Job class for loading batch job instructions and executing them
* in CCIMS
* @author Yew Kai, Joel Haowen TONG
* @version 0.0.003
* @date 2013-04-03
*/

/* Copyright (c) <2012> <CCIMS C01-05>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following 
conditions: 

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/


#pragma once
#ifndef _BATCH_JOB_H
#define _BATCH_JOB_H

#include <fstream>
#include <string>
#include <vector>
#include <stack>

using namespace std;


namespace logic {
	class Logic;            // Circular include

	/* ----------------------------------------------------------------*/
	/**
	* @brief This class will handle all the batch process.
	* @details
	* @code
	*	//create an instance from BatchJob class
	*	BatchJob *bj = new BatchJob();
	*
	*	//Start loading the batch file and executing them
	*	bj->start();
	* @endcode
	*/
	/* ------------------------------------------------------------------*/
	class BatchJob 
	{
	public:
		BatchJob();
		~BatchJob();
		BatchJob(Logic* root);

		bool                start();

	private:
		struct Transaction {
			string id;
			vector< vector<string> > jobs;
		};

		Logic*              _root;
		ifstream*           _batchFile;
		ofstream*           _errFile;
		stack<Transaction>  _tr_stack;

                int        _totalTrans;

		/* Internal functions */
		void                load_root(Logic* root);
		void				loadBatchToStack();
		void                free_mem();
		void                write_error(string transaction_id,
										const vector<string>& instruction);
		vector<string>      get_job();

	};

}

#endif

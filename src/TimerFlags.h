/**
 * @file TimerFlags.h
 * @brief This file contains the flags used by the timer.
 * @author Joel Haowen TONG
 * @version 0.0.002
 * @date 2013-03-16
 */

/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _TIMER_FLAGS_H
#define _TIMER_FLAGS_H
namespace common {
    namespace utils {
        /* ----------------------------------------------------------------*/
        /**
         * @brief This namespace contains flags used by the Timer class.  Note
         * that the Timer class is not located in this namespace.
         */
        /* ------------------------------------------------------------------*/
        namespace timer {
            const unsigned short    PREC_SECS           = 0;
            const unsigned short    PREC_MILI           = 1;
            const unsigned short    PREC_MICRO          = 2;
            const unsigned short    PREC_NANO           = 3;
            const unsigned short    PREC_UNSET          = 4;
            const unsigned short    TIMER_ERROR         = 5;
        
        }
    }
}

#endif

/**
 * @file FormUtils.h
 * @brief This file contains utility functions used for form checking.
 * @author Joel Haowen TONG
 * @version 0.0.002
 * @date 2013-03-20
 */

/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _FORM_UTILS
#define _FORM_UTILS

#include <string>
#include <vector>

#include "Product.h"
#include "ErrorChecker.h"
#include "Interface.h"

using namespace common::errorChecker;

namespace common 
{
    namespace utils
    {
        namespace form 
        {
            /* ----------------------------------------------------------------*/
            /**
             * @brief When called, uses an intelligent filter that parses a 2 * N vector
             * (from gui::Form) into a Product object.  Fields left unspecified are empty
             * strings.  Please see ProductFlags.h for a list of available flag IDs.
             *
             * @param input the 2 * N vector of strings to parse
             *
             * @return The converted Product object
             */
            /* ------------------------------------------------------------------*/
            logic::Product Logic::convertToProd(vector<string>& input) 
            {
                Product temp;

                for (unsigned int i = 0; i < input.size(); i+= 2) {
                    string id = input[i];
                    string val = input[i + 1];

                    if (id == logic::PROD_NAME) {
                        temp.setName(val);

                    } else if (id == logic::PROD_CAT) {
                        temp.setCat(val);

                    } else if (id == logic::PROD_BARCODE) {
                        temp.setBarcode(val);

                    } else if (id == logic::PROD_PRICE) {
                        temp.setPrice(common::utils::str_atof(val));

                    } else if (id == logic::PROD_MANUFACTURER) {
                        temp.setManufacturer(val);

                    } else if (id == logic::PROD_STOCK) {
                        temp.setNoOfStock(common::utils::str_atoi(val));

                    } else if (id == logic::PROD_SOLD) {
                        temp.setNoSold(common::utils::str_atoi(val));

                    } else if (id == logic::PROD_RESTOCK) {

                    }
                }

                return temp;
            }

            
            inline bool formAction_search(vector<string> arg)
            {
                Logic::Product prod = convert_to_prod(arg);
                bool result = check_valid_barcode(prod.getBarcode());

                /* Print out error message if need be */
                if (!result) {
                    common::Interface::get_instance()->logToGui_update_status("Barcode is invalid!");
                    common::Interface::get_instance()->wait();
                
                }

                return result;

            }
        };
    };
};
#endif

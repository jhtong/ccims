/**
 * @file Form.cpp
 * @brief This file implements the Form class container.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
#include "Form.h"

#include <vector>
#include <string>

#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif

#include "Screen.h"
#include "TextField.h"
#include "Button.h"
#include "Form_constants.h"
#include "UIText.h"
#include "Gui.h"


using namespace std;
using namespace gui::form;

/* ----------------------------------------------------------------*/
/**
 * @brief Constructor for the Form instance.
 *
 * @param height The height of the form
 * @param width The Width of the form
 * @param y The Y-position of the form 
 * @param x The X-position of the form
 * @param padding The padding of each form component
 * @param root The reference to the GUI object
 */
/* ------------------------------------------------------------------*/
Form::Form(unsigned int height,
            unsigned int width,
                unsigned int y,
                unsigned int x,
                unsigned int padding,
                gui::Gui* root)
{
    _ROOT = root;
    _WIN_SELF = new gui::Screen(height, width, y, x);
    _WIN_SELF->redraw();
    _padding = padding;
    idx = 0;
    box(_WIN_SELF->get_win(),0,0);
    wrefresh(_WIN_SELF->get_win());

}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the form instance.
 */
/* ------------------------------------------------------------------*/
Form::~Form() 
{
    /* Free the memory */
    for (unsigned int i = 0; i < _ui_stack.size(); i++)
    {
        delete _ui_stack[i];
        _ui_stack[i] = NULL;
    }

    /* Clear the parent form window */
    delete _WIN_SELF;
    _WIN_SELF = NULL;
    _ROOT = NULL;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Push a FormUI component.  Only to be used if the UI is a foreign
 * object, that is, there is no wrapper available in the Form object.
 *
 * @param arg A pointer to the FormUI to push
 */
/* ------------------------------------------------------------------*/
void Form::push(FormUI* arg)
{
    _ui_stack.push_back(arg);
    arrange();
    _WIN_SELF->redraw();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Pop a FormUI derivative from the stack, and delete it.
 */
/* ------------------------------------------------------------------*/
void Form::pop()
{
    delete _ui_stack[_ui_stack.size() - 1];
    _ui_stack[_ui_stack.size() - 1] = NULL;
    _ui_stack.pop_back();
    arrange();
    _WIN_SELF->redraw();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Go to the next item on the stack.  This must be called by FormUI
 * derivatives to access the next FormUI derivative.
 */
/* ------------------------------------------------------------------*/
void Form::go_next()
{
    idx = (idx + 1) % (_ui_stack.size());
    load(idx);
}


/* ----------------------------------------------------------------*/
/**
 * @brief Go to the previous item on the stack.  This must be called by FormUI
 * derivatives to access the previous FormUI derivative.
 */
/* ------------------------------------------------------------------*/
void Form::go_prev()
{
    if (idx == 0) {
        idx = (_ui_stack.size() - 1);

    } else {
        idx = (idx - 1) % (_ui_stack.size());
    
    }
    load(idx);
}


/* ----------------------------------------------------------------*/
/**
 * @brief Start gathering data from the Form object.
 * Must be called by the programmer, after adding the UI components.
 */
/* ------------------------------------------------------------------*/
void Form::start()
{
    idx = 0;
    load(idx);
}


/* ----------------------------------------------------------------*/
/**
 * @brief Private function, called by go_next() and go_prev() functions to load
 * the next UIForm on the stack.
 *
 * @param ref The stack index to load.
 */
/* ------------------------------------------------------------------*/
void Form::load(unsigned int ref)
{
    _ui_stack[ref]->focus();
    _WIN_SELF->redraw();
    return;
}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a private function used internally.  When called, it arranges
 * and redraws the FormUI components on the stack, based on their precedence on
 * the stack.
 */
/* ------------------------------------------------------------------*/
void Form::arrange()
{
    unsigned int y,x;
    y = _WIN_SELF->get_y();
    x = _WIN_SELF->get_x();

    unsigned long acc = 0;          // current Y position
    for (unsigned int i = 0; i < _ui_stack.size(); i++)
    {
        _ui_stack[i]->set_yx(_padding + acc + y, 
                            _padding + x);

        acc += _ui_stack[i]->get_height(); // new Y position
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief Set the YX position of the Form
 *
 * @param y The absolute Y position to move to
 * @param x The absolute X position to move to
 */
/* ------------------------------------------------------------------*/
void Form::set_yx(unsigned int y, unsigned int x)
{
    _WIN_SELF->set_yx(y,x);
    arrange();
    _WIN_SELF->redraw();

}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a wrapper function to push a new field UI compoenent to the
 * stack.  To be called by programmer.
 *
 * @param label The label of the input field.
 * @param field_padding The field padding of the input field.
 */
/* ------------------------------------------------------------------*/
void Form::push_field(string id, string label, unsigned int field_padding)
{
    unsigned int field_width = _WIN_SELF->get_width() - 2 * _padding;
    push(new TextField(1, field_width, 0,0, this, id, label, field_padding, ""));
}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a wrapper function to push a button label UI compoenent to the
 * stack.  To be called by programmer.
 *
 * @param label The label of the button 
 */
/* ------------------------------------------------------------------*/
void Form::push_btn(string label)
{
    unsigned int field_width = _WIN_SELF->get_width() - 2 * _padding;
    push(new Button(1, field_width, 0,0, label, this));

}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a wrapper function to push a button label UI compoenent to the
 * stack.  To be called by programmer.
 *
 * @param label The label of the button 
 * @param *actionFn An pointer to a function action to execute.  This function
 * should return TRUE if successfully executed, or FALSE otherwise.  The
 * function signature is of the type:
 * @code
 * name_of_function(vector<string> args);
 * @endcode
 *
 * Where args is a 2 * N vector of form fields, formatted as a string.
 *
 */
/* ------------------------------------------------------------------*/
void Form::push_btn(string label, bool (*actionFn)(vector<string>))
{
    unsigned int field_width = _WIN_SELF->get_width() - 2 * _padding;
    Button *btn = new Button(1, field_width, 0,0, label, this);
    btn->set_action(actionFn);
    push(btn);
    btn = NULL;


}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a wrapper function to push a static label UI compoenent to the
 * stack.  To be called by programmer.
 *
 * If a newline is required, pass an empty string to this functtion.
 *
 * @param label
 */
/* ------------------------------------------------------------------*/
void Form::push_static_label(string label)
{
    unsigned int field_width = _WIN_SELF->get_width() - 2 * _padding;
    push(new UIText(1, field_width, 0,0, label, this));

}


/* ----------------------------------------------------------------*/
/**
 * @brief This function crunches the data and pushes it to a vector.  This is a
 * private function.
 */
/* ------------------------------------------------------------------*/
void Form::make_data()
{
    /* Create the vector of data to return */
    for (unsigned int i = 0; i < _ui_stack.size(); i++)
    {
        string data = _ui_stack[i]->get_data();
        string id   = _ui_stack[i]->get_id();
        if (data != form::EMPTY) {
            _data_vec.push_back(id);
            _data_vec.push_back(data);
        }

    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief Retrieve the data from the form and store the data to an argument
 * vector by pass-by-reference.
 *
 * @param data_ref The reference vector to write to.
 */
/* ------------------------------------------------------------------*/
void Form::get_data(vector<string>& data_ref)
{
    make_data();
    data_ref = _data_vec;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, centers the form object on the screen.
 */
/* ------------------------------------------------------------------*/
void Form::set_yx_center()
{
    /* For safety */
    if (_ROOT != NULL)
    {
        unsigned int root_height, root_width;
        root_height = _ROOT->get_scr()->get_height();
        root_width  = _ROOT->get_scr()->get_width();

        unsigned int win_height, win_width;
        win_height = _WIN_SELF->get_height();
        win_width = _WIN_SELF->get_width();

        set_yx( (root_height - win_height) / 2,
                (root_width - win_width) / 2);
    }
}


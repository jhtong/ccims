/**
* @file BasicDatabaseList.cpp
* @brief Implementation for BasicDatabase.h
* @author Alex Peh, Joel Haowen TONG, Leslie Tan
* @version 0.0.002
* @date 2013-03-07
*/



#include "BasicDatabaseList.h"


using namespace std;
using namespace logic::adt;


/* ----------------------------------------------------------------*/
/**
* @brief Default constructor.  
*/
/* ------------------------------------------------------------------*/
BasicDatabaseList::BasicDatabaseList() : BasicDatabase() {

	_tail = NULL;
	_size = 0;

}


/* ----------------------------------------------------------------*/
/**
* @brief Destructor
*/
/* ------------------------------------------------------------------*/
BasicDatabaseList::~BasicDatabaseList() {

	ProductList *remove, *temp;

	if(!isEmpty())
	{
		remove = _tail->next;
		_tail->next = NULL;

		while (remove != NULL)
		{
			temp = remove;
			remove = remove->next;
			delete temp;
			_size--;
		}
	}

}

/* ----------------------------------------------------------------*/
/**
* @brief When called, inserts a product entry into the sorted database
*		according to its sorting type(name, barcode,etc) and order(ascending, descending)
* 
* @param product   The entry (of type logic::Product) to insert
*
* @return TRUE if successful
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::insert(logic::Product product)
{
	ProductList *new_prod = new ProductList;

	new_prod->Prod = product;
	new_prod->next = NULL;

	if(isEmpty())		//if database is empty, just insert, don't need to init stuffs below
	{
		_tail = new_prod;
		_tail->next = _tail;
		_size++;
		return true;
	}

	ProductList *curr = _tail->next;

	if(compare(new_prod->Prod,curr->Prod))		//if inserts at head
	{
		_tail->next = new_prod;
		new_prod->next = curr;
		_size++;
	}
	else				//if inserts in between 
	{
		ProductList *prev = _tail->next;
		curr = curr->next;
		while(curr != _tail->next)
		{
			if(compare(new_prod->Prod, curr->Prod))
			{
				prev->next = new_prod;
				new_prod->next = curr;
				_size++;
				return true;
			}
			else
			{
				prev = prev->next;
				curr = curr->next;
			}
		}
		//if the program reaches here meaning to insert at the tail
		new_prod->next = _tail->next;
		_tail->next = new_prod;
		_tail = new_prod;
		_size++;
	}
	return true;
}

/* ----------------------------------------------------------------*/
/**
* @brief When called, sorts the database by the given header category, by
* either ASCENDING OR DESCENDING. 
* Used merge sort algorithm, its breaks down the list to sorting blocks of 2 products 
* and sorts among the 2 products.
* it then merge and sort the 2 sorting blocks to make a bigger sorting block(4 products). 
* This process is contiuned until the list is fully merged into 1 block.
*
* @param header The header category to be sorted.
* @param sort_type Either ASCENDING or DESCENDING
*
* @return True when sorted
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::sort_by(string header, string sort_order)
{
	ProductList *p, *q, *e, *tail, *oldhead,*head=_tail->next;
	int insize, nmerges, psize, qsize, i;

	_sort_type = header;
	_sort_order = sort_order;
	
	if (isEmpty())
		return false;

	insize = 1;	         //initial sorting block size

	while (1) {
		p = head;
		oldhead = head;	  //to retain the original head position, use for checking if a pointer cycles back to head
		head = NULL;
		tail = NULL;

		nmerges = 0;  // count number of merges we do in this pass

		while (p) 
		{
			nmerges++;  // there exists a merge to be done
			/* step `insize' places along from p*/
			q = p;
			psize = 0;
			for (i = 0; i < insize; i++)   //move q to its starting pos, the start of 2nd sort blk
			{
				psize++;
				if (q->next == oldhead)   //if it reaches the end of list
					q = NULL;
				else
					q = q->next;
				if (!q) break;
			}

			/* if q hasn't fallen off end, we have two lists to merge */
			qsize = insize;

			/* now we have two lists; merge them */
			while (psize > 0 || (qsize > 0 && q)) 
			{
				/* decide whether next element, e, of merge comes from p or q */
				if (psize == 0) // p is empty; e must come from q.
				{
					e = q; 
					q = q->next; 
					qsize--;
					if (q == oldhead)   //reaches the end of list
						q = NULL;
				} 
				else if (qsize == 0 || !q) // q is empty; e must come from p.
				{
					e = p; 
					p = p->next;
					psize--;
					if (p == oldhead) 
						p = NULL;
				} 
				else if(compare(q->Prod,p->Prod))  	// First element of q is lower(higher if descend); e must come from q.   
				{
					e = q; 
					q = q->next; 
					qsize--;
					if (q == oldhead) 
						q = NULL;
				}
				else                                // First element of p is lower(higher if descend) (or same)
				{                                   // e must come from p.
					e = p; 
					p = p->next; 
					psize--;
					if (p == oldhead) 
						p = NULL;
				} 

				/* add the next element to the merged list*/
				if (tail) 
				{
					tail->next = e;
				} 
				else 
				{
					head = e;
				}
				tail = e;
			}

			/*finish moving through all the 2nd sublist(q)*/
			p = q;          //assign p with q(=null);
		}
		/*shift the tail and head pointers to the correct position*/
		_tail = tail;
		_tail->next = head;

		if (nmerges <= 1)  //If we have done only one merge, means we have fully merge back into 1 list
			return true;

		insize *= 2;       // Otherwise merge again with twice the block size
	}
}

/* ----------------------------------------------------------------*/
/**
* @brief Swaps the products between two given indexes
*
* @param idx1 First index
* @param idx2 Second index
*
* @return True if the swap is sucessful, else false
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::swap(unsigned long idx1, unsigned long idx2) 
{
	Product temp;

	if (idx1 < size() && idx2 < size() ) 
	{
		if(idx1 == idx2)
			return true;

		ProductList *pos1 = _tail->next;
		ProductList *pos2 = _tail->next;
		//this 2 for loops need to make into a trasverseTo() function
		for(unsigned long i=0; i<idx1; i++)
		{
			pos1 = pos1->next;
		}
		for(unsigned long i=0; i<idx2; i++)
		{
			pos2 = pos2->next;
		}

		temp = pos1->Prod;
		pos1->Prod = pos2->Prod;
		pos2->Prod = temp;

		return true;
	}
	return false;
}


/* ----------------------------------------------------------------*/
/**
* @brief When called, inserts an entry at the end of the database.
*
* @param product The product to insert
*
* @return TRUE when product is inserted.
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::push(const logic::Product& arg) 
{
	ProductList *newProd = new ProductList;
	newProd->Prod = arg;

	if(isEmpty())
	{
		_tail = newProd;
		_tail->next = _tail;
		_size++;
	}
	else
	{
		newProd->next = _tail->next;
		_tail->next = newProd;
		_tail = newProd;
		_size++;
	}
	return true;
}

/* ----------------------------------------------------------------*/
/**
* @brief When called, returns the number of product in database.
*
* @return the amount of products in datebase.
*/
/* ------------------------------------------------------------------*/
unsigned long BasicDatabaseList::size()
{
	return _size;
}

/* ----------------------------------------------------------------*/
/**
* @brief When called, return if the database is empty.
*
* @return TRUE when database is empty.
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::isEmpty()
{
	return _size == 0; 
}

/* ----------------------------------------------------------------*/
/**
* @brief When called, retrieves the ith element, given by the index i.
*
* @param idx The index to retrieve
*
* @return The ith element, typecasted as a product.
*/
/* ------------------------------------------------------------------*/
logic::Product* BasicDatabaseList::get(unsigned long idx)
{
	if(isEmpty() || idx >= size())	//out of bounds or nothing in list
		return NULL;
	else
	{
		ProductList *curr = _tail->next;

		for(unsigned long skip = 0; skip < idx; skip++)
			curr = curr->next;

		return &(curr->Prod);
	}
}


/* ----------------------------------------------------------------*/
/**
* @brief When called, deletes the ith element, given by the index i.
*
* @param idx The index to delete.  Must be between 1 and size() - 1.
*
* @return TRUE if successful, FALSE otherwise
*/
/* ------------------------------------------------------------------*/
bool BasicDatabaseList::delete_at(unsigned long idx) 
{
	if (idx >= size() || isEmpty())
	{
		return false;
	} 
	else
	{
		ProductList *curr, *prev;
		curr = prev = _tail->next;

		if(idx == 0)
		{
			_tail->next = curr->next;
		}
		else
		{
			for(unsigned long hop=0; hop<idx-1; hop++)
				prev = prev->next;

			curr = prev->next;
			prev->next = curr->next;
			if(idx == size()-1)		//if deleted the tail node, adjusts _tail to prev node
				_tail = prev;
		}

		delete curr;
		_size--;
		return true;
	}
}

/* ----------------------------------------------------------------*/
/**
 * @brief Searches the given strings and saves the index in vector
 *
 * @param Term to be searched. Type of header to be searched under. 
 * Vector for storing index of matched items
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/

bool BasicDatabaseList::search(string term, string search_type, vector<int> &index){

	int size = _size, i = 0;
	ProductList *curr;
	string barcode, price, stock, sold;
	regex rxTerm("(.*)" + term + "(.*)");

	index.clear();

	if(search_type != _sort_type)
		sort_by(search_type, SORT_ASCENDING);
	
	curr = _tail->next;

	if(search_type == PROD_NAME){
		while(i < size){
			if(regex_match(curr->Prod.getName(),rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_CAT){
		while(i < size){
			if(regex_match(curr->Prod.getCat(), rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_BARCODE){
		rxTerm = term;
		while(i < size){
			barcode = curr->Prod.getBarcode();
			if(regex_match(barcode, rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_PRICE){
		while(i < size){
			price = common::utils::dtoa(curr->Prod.getPrice());
			if(regex_match(price, rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_MANUFACTURER){
		while(i < size){
			if(regex_match(curr->Prod.getManufacturer(), rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_STOCK){
		stock = common::utils::itoa(curr->Prod.getNoOfStock());
		while(i < size){

			if(regex_match(stock, rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;
		}

		return true;
	}
	else if(search_type == PROD_SOLD){
		sold = common::utils::itoa(curr->Prod.getNoSold());
		while(i < size){

			if(regex_match(sold, rxTerm))
				index.push_back(i);

			i++;
			curr = curr->next;

		}

		return true;
	}
	else
		return false;

}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, searches the entire database by ANDing the terms in the
 * specified input Product search term.  Writes the index of the matched terms
 * to idx_vector.
 *
 * @param term The term to match, typecast as a Product instance.
 * @param idx_vector The vector to match
 *
 * @return TRUE when complete.
 */
/* ------------------------------------------------------------------*/


bool BasicDatabaseList::search_by_prod(logic::Product term, vector<unsigned long>& idx_vector)
{
	vector<string> searches;

	idx_vector.clear();

	searches.push_back(term.getName());
    searches.push_back(term.getCat());
    searches.push_back(term.getBarcode());
    searches.push_back(common::utils::dtoa(term.getPrice()));
    searches.push_back(term.getManufacturer());
	searches.push_back(common::utils::itoa(term.getNoOfStock()));
    searches.push_back(common::utils::itoa(term.getNoSold()));

	regex 
        regexTerm0("(.*)" + searches[0] + "(.*)"),
        regexTerm1("(.*)" + searches[1] + "(.*)"),
        regexTerm2("(.*)" + searches[2] + "(.*)"),
        regexTerm3("(.*)" + searches[3] + "(.*)"),
        regexTerm4("(.*)" + searches[4] + "(.*)"),
        regexTerm5("(.*)" + searches[5] + "(.*)"),
        regexTerm6("(.*)" + searches[6] + "(.*)");

    string all = "(.*)";

    if (term.getBarcode().compare(string("000000000")) == 0) {
        regexTerm2 = all;
    }

    if (term.getPrice() == 0) {
        regexTerm3 = all;
    }

    if (term.getNoOfStock() == 0) {
        regexTerm5 = all;
    }

    if (term.getNoSold() == 0) {
        regexTerm6 = all;
    }


	if(isEmpty())	//out of bounds or nothing in list
		return false;
	else
	{
		unsigned long idx = 0;
		ProductList *curr = _tail->next;
		
		while(idx < size()){
			vector<string> entries;
			entries.push_back(curr->Prod.getName());
			entries.push_back(curr->Prod.getCat());
			entries.push_back(curr->Prod.getBarcode());
			entries.push_back(common::utils::dtoa(curr->Prod.getPrice()));
			entries.push_back(curr->Prod.getManufacturer());
			entries.push_back(common::utils::itoa(curr->Prod.getNoOfStock()));
			entries.push_back(common::utils::itoa(curr->Prod.getNoSold()));
			
			if (regex_match(entries[0], regexTerm0) &&
            regex_match(entries[1], regexTerm1) && 
            regex_match(entries[2], regexTerm2) && 
            regex_match(entries[3], regexTerm3) && 
            regex_match(entries[4], regexTerm4) && 
            regex_match(entries[5], regexTerm5) && 
            regex_match(entries[6], regexTerm6)) {
            idx_vector.push_back(idx);
			}

			curr = curr->next;
			idx++;

		}
	}

	return true;

}


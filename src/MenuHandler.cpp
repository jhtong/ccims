/**
 * @file MenuHandler.cpp
 * @brief This file contains the implementation for the MenuHandler class,
 * which listens for menu input and does the respective action.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
#include "Gui.h"            // Circular include 

#include "MenuHandler.h"

#include "Form.h"
#include "FormFactory.h"
#include "Form_constants.h"

#include "Interface.h"

#include <string>
#include <vector>

using namespace std;
using namespace gui;

/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for the MenuHandler instance.  set_root() must
 * be called thereafter.
 */
/* ------------------------------------------------------------------*/
MenuHandler::MenuHandler() 
{
    _ROOT = NULL;
    _formFactory = new gui::form::FormFactory(_ROOT);

}


/* ----------------------------------------------------------------*/
/**
 * @brief Passes a pointer to the parent Gui instance.
 *
 * @param root A pointer to the parent Gui instance.
 */
/* ------------------------------------------------------------------*/
MenuHandler::MenuHandler(Gui* root)
{
    _ROOT = root;
    _formFactory = new gui::form::FormFactory(_ROOT);
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the MenuHandler instance
 */
/* ------------------------------------------------------------------*/
MenuHandler::~MenuHandler()
{
    _ROOT = NULL;
    delete _formFactory;
    _formFactory = NULL;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Sets the root of the MenuHandler instance
 *
 * @param root A pointer to the parent Gui instance
 */
/* ------------------------------------------------------------------*/
void MenuHandler::set_root(Gui* root)
{
    _ROOT = root;
    _formFactory->set_root(_ROOT);
    
}


/* ----------------------------------------------------------------*/
/**
 * @brief This function contains the core instructions to execute, based on the 
 * key pattern passed.  When called, executes a series of steps based on 
 * a switch-case pattern
 *
 * @return TRUE if there is a signal to quit, FALSE otherwise
 *
 */
/* ------------------------------------------------------------------*/
bool MenuHandler::process()
{
    unsigned int key = getch();

    if (key == '1') {
        _ROOT->status_print(string("Add new item"));
        gui::form::Form* testForm = _formFactory->generate(gui::form::GEN_ADD_FORM, 15, 70, 5, 5, 2, 20);
        testForm->set_yx_center();
        testForm->start();
        vector<string> args;
        testForm->get_data(args);
        delete testForm;
        testForm = NULL;
        common::Interface::get_instance()->guiToLog_add_item(args);
        
    } else if (key == '2') {
        _ROOT->status_print(string("Scrap item"));
        gui::form::Form* testForm = _formFactory->generate(gui::form::GEN_SCRAP_FORM, 15, 70, 5, 5, 2, 20);
        testForm->set_yx_center();
        testForm->start();
        vector<string> args;
        testForm->get_data(args);
        delete testForm;
        testForm = NULL;
        common::Interface::get_instance()->guiToLog_scrap_item(args);
        
    } else if (key == '3') {
        _ROOT->status_print(string("Specify sales"));
        gui::form::Form* testForm = _formFactory->generate(gui::form::GEN_SALE_FORM, 15, 70, 5, 5, 2, 20);
        testForm->set_yx_center();
        testForm->start();
        vector<string> args;
        testForm->get_data(args);
        delete testForm;
        testForm = NULL;
        common::Interface::get_instance()->guiToLog_specify_sales(args);

    } else if (key == '4') {
        _ROOT->status_print(string("Restock items"));
        gui::form::Form* testForm = _formFactory->generate(gui::form::GEN_RESTOCK_FORM, 15, 70, 5, 5, 2, 20);
        testForm->set_yx_center();
        testForm->start();
        vector<string> args;
        testForm->get_data(args);
        delete testForm;
        testForm = NULL;
        common::Interface::get_instance()->guiToLog_restock_item(args);

    } else if (key == '5') {
        _ROOT->status_print(string("Search for item"));
        gui::form::Form* testForm = _formFactory->generate(gui::form::GEN_SEARCH_FORM, 15, 70, 5, 5, 2, 20);
        testForm->set_yx_center();
        testForm->start();
        vector<string> args;
        testForm->get_data(args);
        delete testForm;
        testForm = NULL;
        common::Interface::get_instance()->guiToLog_search_terms(args);

    } else if (key == '6') {
        common::Interface::get_instance()->guiToLog_best_product();

    } else if (key == '7') {
        common::Interface::get_instance()->guiToLog_best_manufacturer();

    } else if (key == '8') {
        common::Interface::get_instance()->guiToLog_best_product_category();

    } else if (key == '9') {
        common::Interface::get_instance()->guiToLog_run_batch();

    } else if (key == '`') {
        common::Interface::get_instance()->guiToLog_toggle_dbMode();

    } else if (key == 'q') {
        /* Return true and exit, bubble properly */
        return true;
    }

    _ROOT->status_print(string(""));
    return false;
}


/**
 * @file FormUI.cpp
 * @brief This class is the base class for all Form UI components.  Member
 * functions are meant to be overridden by derived classes, save for the
 * constructor.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-22
 */

#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif

#include "FormUI.h"
#include "Form_constants.h"
#include "Form.h"

#include <vector>
#include <string>

#include "Screen.h"

using namespace std;
using namespace gui::form;

/* ----------------------------------------------------------------*/
/**
 * @brief Constructor for the FormUI base class.
 *
 * @param height Height of the form component
 * @param width Width of the form component
 * @param y Y position of the form component 
 * @param x X position of the form component
 * @param form_ref Pointer to the parent form class, of type Form.
 */
/* ------------------------------------------------------------------*/
FormUI::FormUI(unsigned int height,     // Create UI with dimensions height, width, y, x
                    unsigned int width,
                    unsigned int y,
                    unsigned int x,
                    Form* form_ref)
{
    /* Assign vars */
    _WIN_SELF = new gui::Screen(height, width, y, x);
    _FORM = form_ref;

    /* Print a message to the screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 0, 0, "%s", "out of focus");
    _WIN_SELF->redraw();

}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the FormUI.
 */
/* ------------------------------------------------------------------*/
FormUI::~FormUI()
{
    delete _WIN_SELF;
    _WIN_SELF = NULL;
    _FORM     = NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, focuses the UI component till a suitable key is
 * returned.
 */
/* ------------------------------------------------------------------*/
void FormUI::focus()
{   

    /* Print a message to the screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 0, 0, "%s", "in focus");
    _WIN_SELF->redraw();

    int keyPressed;

    keypad(_WIN_SELF->get_win(), TRUE);
    
    while(1) {
       keyPressed = wgetch(_WIN_SELF->get_win());

       if (keyPressed == KEY_UP || keyPressed == KEY_DOWN ||
               keyPressed == '\n')
       {
           break;
       }
    }

    /* Print a message to the screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 0, 0, "%s", "out of focus");
    _WIN_SELF->redraw();

    /* Call the PARENT FORM to go to the next object on stack */
    if (keyPressed == KEY_UP)
    {
        _FORM->go_prev();

    } else if (keyPressed == KEY_DOWN || keyPressed == '\n') {
        _FORM->go_next();
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the yx position of the FormUI instance.
 *
 * @param y
 * @param x
 */
/* ------------------------------------------------------------------*/
void FormUI::set_yx(unsigned int y, unsigned int x)
{
    _WIN_SELF->set_yx(y,x);
    _WIN_SELF->redraw();
}


/* Getters and setters */

/* ----------------------------------------------------------------*/
/**
 * @brief Gets the width of the FormUI component.
 *
 * @return The width of the FormUI component.
 */
/* ------------------------------------------------------------------*/
unsigned int FormUI::get_width()
{
    return _WIN_SELF->get_width();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Gets the height of the FormUI component
 *
 * @return The height of the FormUI component
 */
/* ------------------------------------------------------------------*/
unsigned int FormUI::get_height()
{
    return _WIN_SELF->get_height();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Gets the X-position of the FormUI component
 *
 * @return The X-position of the FormUI component
 */
/* ------------------------------------------------------------------*/
unsigned int FormUI::get_x()
{
    return _WIN_SELF->get_x();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Gets the Y-position of the FormUI component
 *
 * @return The Y-position of the FormUI component
 */
/* ------------------------------------------------------------------*/
unsigned int FormUI::get_y()
{
    return _WIN_SELF->get_y();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the data stored by the FormUI component
 *
 * @return The default value for the FormUI base class, EMPTY.

 */
/* ------------------------------------------------------------------*/
string FormUI::get_data()
{
    return form::EMPTY;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Gets the ID of the UI.
 *
 * @return The set ID of the UI.
 */
/* ------------------------------------------------------------------*/
string FormUI::get_id() 
{
    return "";
}


void FormUI::focus(bool (*actionFn)(vector<string>)) 
{
}



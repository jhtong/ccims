/**
* @file File.cpp
* @brief This file contains the implementation for the File class.  
* @author C01-05
* @version 0.0.001
* @date 2013-03-22
*/

#include <vector>
#include <string>

#include "File.h"
#include "Utils.h"
#include "BasicDatabase.h"
#include "BasicDatabaseVector.h"
#include "BasicDatabaseList.h"

/* For outputting errors */
#include "Interface.h"

using namespace std;
using namespace logic;
using namespace logic::adt;

/* --------------------------------------------------------------------------*/
/**
* @brief Constructor
*/
/* ----------------------------------------------------------------------------*/
File::File()
{

}

/* --------------------------------------------------------------------------*/
/**
* @brief Destructor
*/
/* ----------------------------------------------------------------------------*/
File::~File()
{

}


/* ----------------------------------------------------------------*/
/**
* @brief This method is to convert product information into a product.
* @param input A vector of strings containing particulars of a product.
* @return A product.
*/
/* ------------------------------------------------------------------*/
Product File::stringToProd(vector<string>& input)	
{
	Product temp;
	string val;
	for (unsigned int i = 0; i < input.size(); i++)
	{
		val = input[i];

        if ( i == 0) {
            temp.setName(val);

        } if ( i == 1) {
            temp.setCat(val);

        } if ( i == 2) {
            temp.setBarcode(val);

        } if ( i == 3) {
            temp.setPrice(common::utils::str_atof(val));

        } if ( i == 4) {
            temp.setManufacturer(val);

        } if ( i == 5) {
            temp.setNoOfStock(common::utils::str_atoi(val));

        } if ( i == 6) {
            temp.setNoSold(common::utils::str_atoi(val));
        
        }
	}
	return temp;
}


/* ----------------------------------------------------------------*/
/**
* @brief This function converts a product into a string of information based on header given
* @param hdrIdx Header index such that 0=Name, 1=Category, 2=Barcode, etc.
* @param prodPtr Pointer to a particular product
* @return A string of product information based on header given
*/
/* ------------------------------------------------------------------*/
string File::prodToString(int hdrIdx, Product* prodPtr){
	string line;
	switch (hdrIdx)
	{
	case 0: return (prodPtr->getName());
	case 1: return (prodPtr->getCat()); 
	case 2: return (prodPtr->getBarcode()); 
	case 3: return (common::utils::dtoa(prodPtr->getPrice())); 
	case 4: return (prodPtr->getManufacturer()); 
	case 5: return (common::utils::itoa(prodPtr->getNoOfStock())); 
	case 6: return (common::utils::itoa(prodPtr->getNoSold())); 

	default: break;

	}
	return NULL;
}


/* ----------------------------------------------------------------*/
/**
* @brief When called, the text file will be read and loaded into database
* @param dB The database that will be used during runtime
* @return Status for indication of saving
* @retval bool Program status.
*                      <ul>
*                         <li> 0 = Failure
*                         <li> 1 = Success
*                      </ul>
*/
/* ------------------------------------------------------------------*/
bool File::load(BasicDatabase& dB)
{
	int numOfProd;
	string num;
	string line,space;
	ifstream readFile("product.txt");
	vector<string> prodInfo;

	Product tempProd;

	if (readFile.is_open() && readFile.good())
	{
		
		getline(readFile,num);				// get the 1st line //
		numOfProd = common::utils::str_atoi(num);

		
		while(getline(readFile, space))     // get all the products //        
		{
			for(int i=0; i<7; i++){			// get and push the prod information line by line //
				getline(readFile,line);		// into temp vector //
				prodInfo.push_back(common::utils::dosToUnix(line)); 
			}

			tempProd = stringToProd(prodInfo);
			dB.insert(tempProd);
			prodInfo.clear();				// clear the vector for new set of prod info //
		}

		readFile.close();

        common::Interface::get_instance()->logToGui_update_status("product.txt input file successfully loaded.", 2);

		return true;
	}
	else
	{
        common::Interface::get_instance()->logToGui_update_status("ERROR: Cannot open product.txt for reading.  New file will be created.", 2);
		return false;
	}
}


/* ----------------------------------------------------------------*/
/**
* @brief When called, it writes all the products into text file
* @param dB The database that has been used during runtime
* @return Status for indication of saving
* @retval bool Program status
*                      <ul>
*                         <li> 0 = Failure
*                         <li> 1 = Success
*                      </ul>
*/
/* ------------------------------------------------------------------*/
bool File::save(logic::adt::BasicDatabase& dB)
{
	int numOfProd = dB.size(); //1st slot for no of prod
	string line;
	ofstream writeFile("product.txt");

	Product* tempProd;

	if (writeFile.is_open())
	{
		writeFile << numOfProd << endl; //write the very 1st line
		//assume 1st slot of database store its own size

		for(int i=0; i<numOfProd; i++)
		{
			tempProd = dB.get(i);
			writeFile << endl;
			for(int i=0; i<7; i++){		
				line = prodToString(i, tempProd);
				writeFile << line << endl;
			}
		}


		writeFile.close();

		return true;
	}
	else
	{
		return false;
	}
}

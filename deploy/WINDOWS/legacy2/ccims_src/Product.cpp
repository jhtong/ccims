/**
 * @file Product.cpp
 * @brief This file contains the implementation for the Product class.  An ADT
 * of these Products is handled by logic and will be replaced in the future,
 * should the need arise.
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-25
 */


#include "Product.h"

using namespace std;
using namespace logic;


/* --------------------------------------------------------------------------*/
/**
* @brief Constructor
*/
/* ----------------------------------------------------------------------------*/
Product::Product()
{
    _name = _cat = _manufacturer = "";
    _barcode = "000000000";
    _noOfStock = _noSold = 0;
    _price = 0;
}


/* --------------------------------------------------------------------------*/
/**
* @brief Destructor
*/
/* ----------------------------------------------------------------------------*/
Product::~Product()
{
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, clones an existing Product instance.
 *
 * @param arg The Product instance to clone.
 */
/* ------------------------------------------------------------------*/
Product::Product(const Product& arg)
{
    this->copy(arg);
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, creates a clone of the passed Product instance.
 *
 * @param arg The Product instance to copy.
 */
/* ------------------------------------------------------------------*/
void Product::copy(const Product& arg) 
{
    setName(arg.getName());
    setCat(arg.getCat());
    setBarcode(arg.getBarcode());
    setPrice(arg.getPrice());
    setManufacturer(arg.getManufacturer());
    setNoOfStock(arg.getNoOfStock());
    setNoSold(arg.getNoSold());
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, retrieves the name of the product.
 *
 * @return the name of the product
 */
/* ------------------------------------------------------------------*/
string Product::getName() const {
        return _name;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, retrieves the category of the product.
 *
 * @return the category of the product
 */
/* ------------------------------------------------------------------*/
string Product::getCat() const {
        return _cat;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, retrieves the barcode of the product.
 *
 * @return The barcode of the product
 */
/* ------------------------------------------------------------------*/
string Product::getBarcode() const {
        return _barcode;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, gets the price 
 *
 * @return The price, as a double.
 */
/* ------------------------------------------------------------------*/
double Product::getPrice() const 
{
    return _price;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, gets the manufacturer.
 *
 * @return The menufacturer, as a string.
 */
/* ------------------------------------------------------------------*/
string Product::getManufacturer() const  
{
    return _manufacturer;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, gets number of stock
 *
 * @return The current number in stock 
 */
/* ------------------------------------------------------------------*/
int Product::getNoOfStock()  const 
{
    return _noOfStock;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the number of products sold
 *
 * @return The number of products sold
 */
/* ------------------------------------------------------------------*/
int Product::getNoSold()  const 
{
    return _noSold;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the name of the product
 *
 * @param arg The name of the product
 */
/* ------------------------------------------------------------------*/
void Product::setName(string arg){
    _name = arg;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the category of the product
 *
 * @param string The category of the product
 */
/* ------------------------------------------------------------------*/
void Product::setCat(string arg){
    _cat = arg;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the barcode of the product.
 *  If the given barcode is less than 9 digits, automatic
 *  adds leading zeros to barcode's max length.
 *
 * @param arg The barcode of the product
 */
/* ------------------------------------------------------------------*/
void Product::setBarcode(string arg){
	if(common::errorChecker::within_barcode_len(arg))
	{
		string temp = "";
		int len = arg.length();

		for(int i=0; i<9-len; i++)
			temp += "0";

		_barcode = temp + arg;
	}
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the price of the product
 *
 * @param price the price of the product
 */
/* ------------------------------------------------------------------*/
void Product::setPrice(double arg){
    _price = arg;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the manufacturer of the product
 *
 * @param string The manufacturer of the product
 */
/* ------------------------------------------------------------------*/
void Product::setManufacturer(string arg){
    _manufacturer = arg;
}


/* ----------------------------------------------------------------*/
/**
 * @brief when called, sets the product stock
 *
 * @param num The number of product stock
 */
/* ------------------------------------------------------------------*/
void Product::setNoOfStock(int arg){
        _noOfStock = arg;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the number of sets of the product sold 
 *
 * @param itemSold The number of product items sold
 */
/* ------------------------------------------------------------------*/
void Product::setNoSold(int arg){
        _noSold = arg;
}



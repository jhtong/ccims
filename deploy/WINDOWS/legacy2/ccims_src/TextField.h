/**
 * @file TextField.h
 * @brief Derived class from FormUI.  Used as a text input form component.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-20
 */

/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _TEXTFIELD_H
#define _TEXTFIELD_H

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"

using namespace std;

namespace gui 
{
    namespace form
    {
        /* ----------------------------------------------------------------*/
        /**
         * @brief The TextField class is a form component, inheriting methods from
         * the FormUI base class.  It is able to get single-line input, and display
         * input that is the width of the box.
         *
         * @todo Add support for input that is longer than the width of the textfield.
         */
        /* ------------------------------------------------------------------*/
        class TextField : public FormUI
        {
            public:
                TextField(unsigned int      height,                 // Create UI with dimensions height, width, y, x
                            unsigned int    width,
                            unsigned int    y,
                            unsigned int    x,
                            Form*           form_ref    = NULL,
                            string          id          = "",
                            string          label       = "",
                            unsigned int    label_width = 10,
                            string          input       = "");

                virtual             ~TextField();
                virtual void        focus();
                virtual string      get_data();
                virtual string      get_id();

            
            private:
                string          _label;
                string          _id;
                unsigned int    _label_width;
                string          _input;
                unsigned int    _cur_idx;

                string          get_pretty_label();
                void            make_display();

        };
    }
} 

#endif

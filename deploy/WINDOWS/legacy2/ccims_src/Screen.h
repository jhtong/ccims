/**
 * @file Screen.h
 * @brief The Screen class is a container class abstraction of both WINDOW
 * and panel for Ncurses.  The width, height, WINDOW, and panel for each screen
 * can be accessed from the Screen class.
 *
 * This class should be used over creating the SCREEN and panel
 * instances individually.
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */


#pragma once
#ifndef _SCREEN_H
#define _SCREEN_H

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* End ncurses definition */

#include <panel.h>


namespace gui 
{
    /* --------------------------------------------------------------------------*/
    /**
     * @brief The Screen class is a container class abstraction of both WINDOW
     * and PANEL for Ncurses.  The width, height, WINDOW, and panel for each screen
     * can be accessed from the Screen class.
     *
     * This class should be used over creating the WINDOW and PANEL
     * instances individually.
     * 
     * Note that once the Screen instance is created, the width and height
     * cannot (and should not) be modified.
     *
     * EXAMPLE OF USAGE
     * 
     * @code
     * ...
     * // Create new Screen instance of width 50 X 50, at the point (0,0)
     * Screen* bla = new Screen(50, 50, 0, 0);
     *
     * // Print "hello world" in the bla screen at the point (5,5), relative to 
     * // screen.
     * mvwprintw(bla->get_win(), 0, 0, "%s", "hello world")
     *
     * // display the "hello world" on screen 
     * bla->redraw();
     *
     * // Hide screen 
     * bla.hide(); 
     *
     * // Show screen 
     * bla.show();
     *
     * // Check if bla is hidden 
     * bla.is_hidden()                  // Returns false
     *
     * // Move bla to the point (15, 15)
     * bla.set_yx(15,15);
     * // Can change y, x coordinates using set_y() and set_x() accordingly.
     *
     * // Delete bla and call mem dellocation
     * delete bla;
     *
     * // set bla to NULL for garbage collection
     * bla = NULL;
     * ...
     * @endcode
     * @author TONG Haowen Joel
     * @version 0.0.001
     * @date 2013-02-15
     */
    /* ----------------------------------------------------------------------------*/
    class Screen 
    {
    public:
        Screen( unsigned int height = 0, 
                unsigned int width = 0, 
                unsigned int y = 0, 
                unsigned int x = 0, 
                WINDOW* target = NULL);
        ~Screen();

        /* Getters and setters */
        WINDOW*         get_win();

        unsigned int    get_height();
        unsigned int    get_width();

        void            set_x(unsigned int arg);
        unsigned int    get_x();

        void            set_y(unsigned int arg);
        unsigned int    get_y();

        void            set_yx(unsigned int y, unsigned int x);
        
        PANEL*          get_panel();                    // Return panel

        void            hide();
        void            show();
        bool            is_hidden();

        void            redraw();


    private:
        unsigned int _height;                           // Height of the window
        unsigned int _width;                            // Width of the window
        unsigned int _x;                                // x Position
        unsigned int _y;                                // y Position
        WINDOW*      _window;                           // instance of the window
        PANEL*       _panel;                            // Pointer to panel.  XY should be controlled from here

        void        free_mem();                         // Utility to free memory

    };
}

#endif

/**
 * @file ErrorChecker.h
 * @brief error checking functions library
 * @author 
 * @version 0.0.001
 * @date 2013-03-17
 */
/* Copyright (c) <2013> 
*/
#ifndef _ERROR_H
#define _ERROR_H

#include <string>
using namespace std;

namespace common
{
	namespace errorChecker
	{
		/* ----------------------------------------------------------------*/
        /**
         * @brief given a string, checks if it only contains numbers
         *
         * @param arg the given string
         *
         * @return true if its all numbers, else false
         */
        /* ------------------------------------------------------------------*/
		inline bool is_num(string num)
		{
			unsigned found = num.find_first_not_of("1234567890.");
			if(found == string::npos)
				return true;
			else 
				return false;
		}

		/* ----------------------------------------------------------------*/
        /**
         * @brief given a string, checks if it is within the barcode max length,
		 *  which is 9 digits. 
         *
         * @param arg the given barcode in string 
         *
         * @return true if its within length, else false
         */
        /* ------------------------------------------------------------------*/
		inline bool within_barcode_len(string num)
		{
			unsigned int len = num.length();
			return (len <= 9);
		}


        /* ----------------------------------------------------------------*/
        /**
         * @brief When called, checks if a given string is a valid barcode.
         *
         * @param barcode The barcode to check, typecasted as a string.
         *
         * @return TRUE if valid, FALSE otherwise.
         */
        /* ------------------------------------------------------------------*/
        inline bool check_valid_barcode(string barcode)
        {
            return is_num(barcode) && within_barcode_len(barcode);
        }

	};
};

#endif

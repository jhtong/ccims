/**
 * @file Screen.cpp
 * @brief This file contains the implementation of the Screen class, an
 * abstraction that combines both the WINDOW and Panel objects of ncurses.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */


// TODO: Revamp Screen.h to automatically make its own window and panel

#include "Screen.h"

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end compile curses definition */
#include <panel.h>

using namespace std;

namespace gui
{
    /* --------------------------------------------------------------------------*/
    /**
     * @brief           Public constructor
     *
     * @param height    Height of the screen 
     * @param width     Width of the screen 
     * @param y         y-position of the screen
     * @param x         x-position of the screen
     * @param target    If specified, generates a Screen object from a window
     * pointer.
     */
    /* ----------------------------------------------------------------------------*/
    Screen::Screen( unsigned int height, 
                    unsigned int width, 
                    unsigned int y,
                    unsigned int x,
                    WINDOW* target)
        {
            if (target == NULL) {
                _height  = height;
                _width   = width;
                _x       = x;
                _y       = y;
                _window  = newwin(_height, _width, 0, 0);
                _panel   = new_panel(_window);
                this->set_yx(_y, _x);
                top_panel(_panel);

            } else {
                /* Implies this is stdscr 
                * Therefore, do not create panel
                * */
                _window = target;
                getmaxyx(target, _height, _width);
                _x = _y = 0;

            }
            redraw();
        }
        
    /* ----------------------------------------------------------------*/
    /**
     * @brief Destructor
     */
    /* ------------------------------------------------------------------*/
    Screen::~Screen() 
    {
        free_mem();
    }

    /* Getters and setters */
    /* --------------------------------------------------------------------------*/
    /**
     * @brief   Gets the window of the Screen instance.
     *
     * @return  The window of the Screen instance, of type WINDOW*
     */
    /* ----------------------------------------------------------------------------*/
    WINDOW*         Screen::get_win()                       { return _window;   }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief    Gets the height of the current Screen instance
     *
     * @return   The height of the current Screen instance
     */
    /* ----------------------------------------------------------------------------*/
    unsigned int    Screen::get_height()                    { return _height;   }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Gets the width of the current Screen instance
     *
     * @return The width of the current Screen instance
     */
    /* ----------------------------------------------------------------------------*/
    unsigned int    Screen::get_width()                     { return _width;    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief       Sets the X value of screen 
     *
     * @param arg   The new X value of the screen 
     */
    /* ------------------------------------------------------------------*/
    void Screen::set_x(unsigned int arg)    
    {
        _x = arg; 
        move_panel(_panel, _y, _x);
        redraw();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief       Sets the y value of screen 
     *
     * @param arg   The new Y value of the screen 
     */
    /* ------------------------------------------------------------------*/
    void Screen::set_y(unsigned int arg)    
    {
        _y = arg; 
        move_panel(_panel, _y, _x);
        redraw();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief       Sets the YX position of the screen 
     *
     * @param y     The new Y value of the screen
     * @param x     The new X value of the screen 
     */
    /* ------------------------------------------------------------------*/
    void Screen::set_yx(unsigned int y,
                        unsigned int x) 
    {
        _x = x;
        _y = y;
        move_panel(_panel, _y, _x);
        redraw();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief       Get the X position of the screen
     *
     * @return      Get the X position of the screen
     */
    /* ------------------------------------------------------------------*/
    unsigned int Screen::get_x()            {return _x; }


    /* ----------------------------------------------------------------*/
    /**
     * @brief       Get the Y position of the screen
     *
     * @return      Get the Y position of the screen
     */
    /* ------------------------------------------------------------------*/
    unsigned int Screen::get_y()            {return _y; }


    /* ----------------------------------------------------------------*/
    /**
     * @brief   Get the panel of the screen 
     *
     * @return  A pointer to the screen panel
     */
    /* ------------------------------------------------------------------*/
    PANEL* Screen::get_panel()
    {
        return _panel;
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Hides the screen, when called
     */
    /* ------------------------------------------------------------------*/
    void Screen::hide()
    {
        hide_panel(_panel);
        redraw();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Shows the screen, when called
     */
    /* ------------------------------------------------------------------*/
    void Screen::show()
    {
        show_panel(_panel);
        redraw();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief   Check if screen is hidden
     *
     * @return  TRUE if screen is hidden, FALSE otherwise
     */
    /* ------------------------------------------------------------------*/
    bool Screen::is_hidden()
    {
        return (bool) panel_hidden(_panel);
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Redraws / refreshes the screen 
     */
    /* ------------------------------------------------------------------*/
    void Screen::redraw()
    {
        update_panels();
        doupdate();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Frees the memory of the Screen instance, called by the destructor
     */
    /* ------------------------------------------------------------------*/
    void Screen::free_mem()
    {
        if (_window != stdscr) {
            del_panel(_panel);
            delwin(_window);
            _panel = NULL;
            _window = NULL;
        
        } else {
          //  del_panel(_panel);
            delwin(_window);
           // _panel = NULL;
            _window = NULL;
            endwin();
        
        }
    }
}

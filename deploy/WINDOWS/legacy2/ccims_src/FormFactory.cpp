/**
 * @file FormFactory.cpp
 * @brief This file implements the FormFactory class, according to the Factory
 * pattern.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */

#include "FormFactory.h"
#include "Form_constants.h"
#include "ProductFlags.h"
#include "Interface.h"
//#include "FormUtils.h"

#include <vector>
#include <string>

using namespace std;
using namespace gui::form;


inline bool forFun(vector<string> arg)
{
    //TODO: THIS IS A TEST FUNCTION.  PASS A FUNCTION TO THE SEARCH TO VALIDATE
    //FIELDS.
    //common::Interface::get_instance()->logToGui_update_status("Form has been checked");
    //common::Interface::get_instance()->wait();

    return true;
    /* return false if invalid */
}


/* ----------------------------------------------------------------*/
/**
 * @brief This function checks if the current barcode is unique.
 *
 * @param arg The list of form arguments to input, from form.
 *
 * @return TRUE if valid, false otherwise.
 */
/* ------------------------------------------------------------------*/
inline bool check_existing_barcode(vector<string> arg) {
    /* Return TRUE if barcode is unique and not yet in database */
    bool retVal = common::Interface::get_instance()->guiToLog_validate_unique(arg);
    if (!retVal) {
    common::Interface::get_instance()->logToGui_update_status("ERROR: Barcode is not unique.  Please re-enter!", 1);
    common::Interface::get_instance()->wait();
        
    }

    return retVal;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for the FormFactory object.
 */
/* ------------------------------------------------------------------*/
FormFactory::FormFactory() 
{
    _ROOT = NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the FormFactory object.
 */
/* ------------------------------------------------------------------*/
FormFactory::~FormFactory() 
{
    _ROOT = NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief This constructor allows one to pass the Gui pointer straight to the
 * FormFactory instance
 *
 * @param root A pointer to the parent Gui instance
 */
/* ------------------------------------------------------------------*/
FormFactory::FormFactory(gui::Gui* root)
{
    set_root(root);
}


/* ----------------------------------------------------------------*/
/**
 * @brief This root sets the parent Gui object pointer for the FormFactory instance.
 *
 * @param root
 */
/* ------------------------------------------------------------------*/
void FormFactory::set_root(gui::Gui* root)
{
    _ROOT = root;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, generates a Form object, based on the OPCode passed.  
 * OPCodes can be found in the file Form_constants.h.  
 *
 * The function returns a pointer to the created Form object.
 *
 * @param form_opcode OPCode OPCode to pass, defined in Form_constants.h
 * @param height Height of the form object
 * @param width Width of the form object
 * @param y Y position of the form object
 * @param x X position of the form object
 * @param padding Padding to use, for the form object
 * @param field_padding Field padding to use, for all input fields
 *
 * @return A pointer to the created Form instance.
 */
/* ------------------------------------------------------------------*/
Form* FormFactory::generate(const int form_opcode,
                            unsigned int height,
                            unsigned int width,
                            unsigned int y,
                            unsigned int x,
                            unsigned int padding,
                            unsigned int field_padding)
{
    Form* tempForm = new Form(height, width, y, x, padding, _ROOT);

    if (form_opcode == form::GEN_ADD_FORM) {
        tempForm->push_static_label("ADD A NEW ITEM:");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_NAME, "Name", field_padding);
        tempForm->push_field(logic::PROD_CAT, "Category", field_padding);
        tempForm->push_field(logic::PROD_BARCODE, "Barcode #", field_padding);
        tempForm->push_field(logic::PROD_PRICE, "Price", field_padding);
        tempForm->push_field(logic::PROD_MANUFACTURER, "Manufacturer", field_padding);
        tempForm->push_field(logic::PROD_STOCK, "# in stock", field_padding);
        tempForm->push_field(logic::PROD_SOLD, "# sold", field_padding);
        tempForm->push_btn("Submit", check_existing_barcode);

    } else if (form_opcode == form::GEN_SCRAP_FORM) {
        tempForm->push_static_label("SCRAP ITEM");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_BARCODE, "Barcode #", field_padding);
        tempForm->push_btn("Submit");

    } else if (form_opcode == form::GEN_SALE_FORM) {
        tempForm->push_static_label("SPECIFY SALE OF PRODUCTS");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_BARCODE, "Barcode #", field_padding);
        tempForm->push_field(logic::PROD_SOLD, "# new sold", field_padding);
        tempForm->push_btn("Submit");

    } else if (form_opcode == form::GEN_RESTOCK_FORM) {
        tempForm->push_static_label("SPECIFY RESTOCK OF PRODUCTS");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_BARCODE, "Barcode #", field_padding);
        tempForm->push_field(logic::PROD_STOCK, "# to restock", field_padding);
        tempForm->push_btn("Submit");

    } else if (form_opcode == form::GEN_SEARCH_FORM) {
        tempForm->push_static_label("SPECIFY SEARCH TERMS");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_NAME, "Name", field_padding);
        tempForm->push_field(logic::PROD_CAT, "Category", field_padding);
        tempForm->push_field(logic::PROD_BARCODE, "Barcode #", field_padding);
        tempForm->push_field(logic::PROD_PRICE, "Price", field_padding);
        tempForm->push_field(logic::PROD_MANUFACTURER, "Manufacturer", field_padding);
        tempForm->push_field(logic::PROD_STOCK, "# in stock", field_padding);
        tempForm->push_field(logic::PROD_SOLD, "# sold", field_padding);
        tempForm->push_btn("Submit", forFun);

    } else if (form_opcode == form::GEN_STATS_FORM) {
        tempForm->push_static_label("STATISTICS");
        tempForm->push_static_label("");
        tempForm->push_field(logic::PROD_NAME, "Best-selling product", field_padding);
        tempForm->push_field(logic::PROD_STOCK, "Best-selling manufacturer", field_padding);
        tempForm->push_btn("Submit");
    }


    return tempForm;
}



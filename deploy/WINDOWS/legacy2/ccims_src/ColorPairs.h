/**
 * @file ColorPairs.h
 * @brief This namespace defines the global color pairs 
 * that are used by ncurses.  To use these color pairs, call
 * ColorPairs::init() to generate the color pairs.  Then, use 
 * the color pairs defined below.
 *
 * Each color pair has a signature of starting with the name 
 * COLOR_PAIR .
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _COLOR_PAIRS_H
#define _COLOR_PAIRS_H

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */


namespace ColorPairs {
    /* Colors */
	/* Color pair numbers on windows
	 * Found by trial and error
	 * 1 - Dark blue
	 * 2 - Dark green
	 * 3 - Dark cyan
	 * 4 - Red-brown
	 * 5 - Purple
	 * 6 -  Brown-yellow
	 * 7 -  Light gray
	 * 8 -  Gray
	 * 9 -  Blue
	 * 10 - Light green
	 * 11 - Cyan
	 * 12 - Red
	 * 13 - Pink
	 * 14 - Yellow
	 * 15 - White
	 * 16 - Black
	 */
    const unsigned int COLOR_ORANGE                     = 0;
	const unsigned int COLOR_BLACK2                     = 0;
	const unsigned int COLOR_RED2                       = 12;
	const unsigned int COLOR_DARK_RED2	                = 4;
	const unsigned int COLOR_DARK_CYAN2	                = 3;
	const unsigned int COLOR_GREEN2                     = 10;
	const unsigned int COLOR_DARK_GREEN2	            = 2;
	const unsigned int COLOR_YELLOW2                    = 14;
	const unsigned int COLOR_DARK_YELLOW2               = 6;
	const unsigned int COLOR_BLUE2                      = 9;
	const unsigned int COLOR_GREY2                      = 7;
	const unsigned int COLOR_DARK_GREY2	                = 8;
	const unsigned int COLOR_MAGENTA2					= 5;
	const unsigned int COLOR_CYAN2                      = 11;
	const unsigned int COLOR_WHITE2                     = 15;

    /* BEGIN  DEFINITION OF COLOR PAIRS ************************/
    const unsigned int COLOR_PAIR_STATUS_NORMAL         = 1;
    const unsigned int COLOR_PAIR_SCROLL_HEADER         = 2;
    const unsigned int COLOR_PAIR_SCROLL_SEL            = 3;
    const unsigned int COLOR_PAIR_SCROLL_0              = 4;
    const unsigned int COLOR_PAIR_SCROLL_1              = 5;
    const unsigned int COLOR_PAIR_FORM_SEL              = 6;
    const unsigned int COLOR_PAIR_FORM_NORM             = 7;
    const unsigned int COLOR_PAIR_SCROLL_SEARCH         = 8;
    const unsigned int COLOR_PAIR_STATUS_ERROR          = 9;

    /* For the Header in GUI */
    const unsigned int COLOR_PAIR_TOP_WIN               = 9;
    const unsigned int COLOR_PAIR_TOP_MENU              = 10;
    /* END DEFINITION OF COLOR PAIRS ***************************/
    

    /* ----------------------------------------------------------------*/
    /**
     * @brief Called to make custom colors, if supported
     */
    /* ------------------------------------------------------------------*/
    inline void make_colors()
    {
       if (can_change_color()) {
           /* Force colors in Windows / DOS systems to follow *NIX ones ;) */
           init_color(COLOR_ORANGE, 180, 180, 0); 
           init_color(COLOR_BLACK2, 157, 149, 149);
           init_color(COLOR_RED2, 949, 133, 184);
           init_color(COLOR_GREEN2, 0, 1000, 267);
           init_color(COLOR_YELLOW2, 1000, 647, 0);
           init_color(COLOR_BLUE2, 184, 404, 914);
           init_color(COLOR_MAGENTA2, 314, 220, 702);
           init_color(COLOR_CYAN2, 271, 898, 792);
           init_color(COLOR_WHITE2, 0, 0, 0);
       }
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Called to make color pairs.  Called in ColorPairs::init()
     */
    /* ------------------------------------------------------------------*/
    inline void make_color_pairs()
    {
        /* Usually the case for PDcurses on Windows systems */
       if (can_change_color()) {

            init_pair(COLOR_PAIR_STATUS_NORMAL,         COLOR_WHITE2,            COLOR_BLUE2);

            /* For scroll list */
            init_pair(COLOR_PAIR_SCROLL_HEADER,         COLOR_WHITE2,            COLOR_MAGENTA2);
            init_pair(COLOR_PAIR_SCROLL_SEL,            COLOR_DARK_GREEN2,       COLOR_YELLOW2);
            init_pair(COLOR_PAIR_SCROLL_0,              COLOR_WHITE2,            COLOR_DARK_CYAN2);
            init_pair(COLOR_PAIR_SCROLL_1,              COLOR_WHITE2,            COLOR_DARK_GREY2);
            init_pair(COLOR_PAIR_FORM_SEL,              COLOR_WHITE2,            COLOR_RED2);
            init_pair(COLOR_PAIR_FORM_NORM,             COLOR_WHITE2,            COLOR_BLACK2);
            init_pair(COLOR_PAIR_TOP_WIN,               COLOR_MAGENTA2,			 COLOR_WHITE2);
            init_pair(COLOR_PAIR_TOP_MENU,              COLOR_WHITE2,            COLOR_RED2);
            init_pair(COLOR_PAIR_SCROLL_SEARCH,         COLOR_WHITE2,            COLOR_RED2);
            init_pair(COLOR_PAIR_STATUS_ERROR,          COLOR_WHITE2,            COLOR_RED2);

            /* Usually Bash shell on *NIX systems */
       } else {
            init_pair(COLOR_PAIR_STATUS_NORMAL,         COLOR_WHITE,            COLOR_BLUE);

            /* For scroll list */
            init_pair(COLOR_PAIR_SCROLL_HEADER,         COLOR_WHITE,            COLOR_MAGENTA);
            init_pair(COLOR_PAIR_SCROLL_SEL,            COLOR_BLACK,            COLOR_YELLOW);
            init_pair(COLOR_PAIR_SCROLL_0,              COLOR_WHITE,            COLOR_BLUE);
            init_pair(COLOR_PAIR_SCROLL_1,              COLOR_WHITE,            COLOR_BLACK);
            init_pair(COLOR_PAIR_FORM_SEL,              COLOR_WHITE,            COLOR_RED);
            init_pair(COLOR_PAIR_FORM_NORM,             COLOR_WHITE,            COLOR_BLACK);
            init_pair(COLOR_PAIR_TOP_WIN,               COLOR_MAGENTA, COLOR_WHITE);
            init_pair(COLOR_PAIR_TOP_MENU,              COLOR_WHITE, COLOR_RED);
            init_pair(COLOR_PAIR_SCROLL_SEARCH,         COLOR_WHITE, COLOR_RED);
            init_pair(COLOR_PAIR_STATUS_ERROR,          COLOR_WHITE, COLOR_RED);

       }
    
    }

    /* --------------------------------------------------------------------------*/
    /**
     * @brief This function generates the color pairs, and makes them
     * available for calling.  Color pairs can hence be called using 
     * COLOR_PAIR_XXXXX constants defined above.
     */
    /* ----------------------------------------------------------------------------*/
    inline void init()
    {
        make_colors();
        make_color_pairs();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief When called, this function changes the default color values for windows systems.  To be called solely once, in main() function.
     */
    /* ------------------------------------------------------------------*/
    inline void change_default_colors()
    {
       if (can_change_color()) {
           /* Force colors in Windows / DOS systems to follow *NIX ones ;) */
		   start_color();
           init_color(COLOR_BLACK2, 157, 149, 149);
		   init_color(COLOR_RED2, 949, 133, 184);
           init_color(COLOR_GREEN2, 0, 1000, 267);
           init_color(COLOR_YELLOW2, 1000, 647, 0);
           init_color(COLOR_BLUE2, 184, 404, 914);
           init_color(COLOR_MAGENTA2, 314, 220, 702);
           init_color(COLOR_CYAN2, 271, 898, 792);
           init_color(COLOR_WHITE2, 0, 0, 0);
       }

       
    }
}

#endif

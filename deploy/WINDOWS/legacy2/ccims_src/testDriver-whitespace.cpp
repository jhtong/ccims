#include <string>
#include <iostream>

using namespace std;

string trim(string s) {
    size_t p = s.find_first_not_of(" \t\n");
    s.erase(0,p);

    p = s.find_last_not_of(" \t\n");
    if (string::npos != p) {
        s.erase(p+1);
    }
    return s;
}

int main(int argc, const char *argv[])
{
    cout << trim("            hello world                             ") << "a" << endl;
    return 0;
}

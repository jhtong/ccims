/**
 * @file Product.h
 * @brief   This class contains all the attributes of products with getters and setters
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-25
 */
/* Copyright (c) <2012> <C01-05>
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _PRODUCT_H
#define _PRODUCT_H

#include <string>
#include <stdio.h>
#include "ErrorChecker.h"


namespace logic
{

    /* ----------------------------------------------------------------*/
    /**
     * @brief The basic entity of the database.  The logic class stores a
     * collection of these objects.
     */
    /* ------------------------------------------------------------------*/
	class Product
	{
	private:
		/* attributes of product */
		string _name, _cat, _manufacturer, _barcode;
		int _noOfStock, _noSold;
		double _price;

	public:
                Product();
		Product(const Product& arg);
		~Product();
                

		/* getters */
		string  getName() const; 
		string  getCat() const;
		string  getBarcode() const;
                double  getPrice() const;
                string  getManufacturer() const;
                int     getNoOfStock() const;
                int     getNoSold() const;


		/* setters */
		void setName(string arg); 
		void setCat(string arg);
		void setBarcode(string arg);
		void setPrice(double arg);
		void setManufacturer(string arg);
		void setNoOfStock(int arg);
		void setNoSold(int arg);

        void copy(const Product& arg);

	};

}
#endif

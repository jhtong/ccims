/**
 * @file Header.h
 * @brief This file creates a header to display at the top of the GUI
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _HEADER_H
#define _HEADER_H


/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* End include header of curses library */
#include <string>

#include "Screen.h"


using namespace std;

namespace gui 
{
    class Gui;
    /* ----------------------------------------------------------------*/
    /**
     * @brief The header class is specific to the CCIMS GUI frontend.  It
     * should only be instantiated once.
     *
     * Text diplayed in this class is static and should only be used for
     * decorative purposes.
     */
    /* ------------------------------------------------------------------*/
    class Header 
    {
        public:
            Header(unsigned int height, 
                unsigned int width, 
                unsigned int y, 
                unsigned int x, 
                Gui* root);
            ~Header();
            unsigned int    get_width();
            unsigned int    get_height();
            unsigned int    get_y();
            unsigned int    get_x();
            void            set_yx(unsigned int y, 
                                    unsigned int x);

        private:
            Gui*            _ROOT;
            Screen*         _WIN_SELF;
            Screen*         _WIN_MENU_TEXT;
            void            redraw();
            void            init();

    };
}
#endif

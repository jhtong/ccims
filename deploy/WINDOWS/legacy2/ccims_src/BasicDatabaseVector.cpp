/**
 * @file BasicDatabaseVector.cpp
 * @brief This file implements the BasicDatabaseVector class.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-03-10
 */


#include <string>
#include <vector>
#include <regex>

#include "BasicDatabase.h"
#include "BasicDatabaseVector.h"
#include "Product.h"
#include "ProductFlags.h"
#include "AdtFlags.h"
#include "Utils.h"

using namespace std;
using namespace logic::adt;


/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for BasicDatabaseVector
 */
/* ------------------------------------------------------------------*/
BasicDatabaseVector::BasicDatabaseVector() : BasicDatabase() 
{
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor
 */
/* ------------------------------------------------------------------*/
BasicDatabaseVector::~BasicDatabaseVector() 
{
    clear();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Insert element at a given index.  Will not insert, if specified index
 * does not fall within the current bounds of the database.
 *
 * @param idx An index to insert, from 0 to size() - 1.
 * @param product The product to insert
 *
 * @return TRUE if successful, FAlSE otherwise
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::insert_at(unsigned long idx,
                                    logic::Product product) 
{
    if (idx < 0 || idx >= productVec.size() ) {
        return false;

    } else {
        Product temp = Product(product);
        productVec.insert(productVec.begin() + idx, temp);
        return true;
    }

    return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, inserts a product entry into the sorted database
 *		according to its sorting type(name, barcode,etc) and order(ascending, descending)
 * 
 * @param product   The entry (of type logic::Product) to insert
 *
 * @return TRUE if successful
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::insert(logic::Product product)
{
	unsigned long idx = 0;

	if(productVec.empty())
	{
		productVec.push_back(product);
		return true;
	}

	for(idx = 0; idx<productVec.size(); idx++)	//if insert in between 
	{
		if(compare(product, productVec[idx]))
		{
			productVec.insert(productVec.begin() + idx, product);
			return true;
		}
	}
	//if the program reaches here meaning to insert at the back
	productVec.push_back(product);
	return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, sorts the database according to a specified key (header)
 * , and a specified type (ASECNDING OR DESCENDING).  Constants for header is
 * found in ProductFlags.h, while constants for sort order are found in
 * AdtFlags.h.
 *
 * @param header The key to sort by
 * @param sort_type The sort order, either ASCENDING or DESCENDING.
 *
//TODO: BUG: FIX THE COMPARE.  CANNOT SORT ASCENDING, ONLY DESCENDING WORKS
 * @return TRUE when sorted
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::sort_by(string header, string sort_order) 
{
    logic::Product key;

	_sort_order = sort_order;
	_sort_type = header;

    for (unsigned long j = 1; j < productVec.size(); j++) 
	{
        key = productVec[j];
        /* Cannot be unsigned - somehow becomes an underflow upon hitting 0 */
        for (long i=j-1; i >= 0 ; i--)
		{
			if (compare(productVec[i+1],productVec[i])) 
			{
				productVec[i+1] = productVec[i];
				productVec[i] = key;
			}
		}
	}
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns a pointer to the current element at the specified
 * index.  If index is invalid, returns NULL
 *
 * @param idx The index to retrieve
 *
 * @return A pointer of type object, referencing the given index.  If out of
 * bounds, returns NULL.
 */
/* ------------------------------------------------------------------*/
logic::Product* BasicDatabaseVector::get(unsigned long idx) 
{

    /* If error */
    if (idx < 0 || idx >= productVec.size() ) {
        return NULL;
    }

    return &productVec.at(idx);
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, swaps 2 elements at 2 specified indexes.
 *
 * @param idx1 The index of the first element to swap 
 * @param idx2 The index of the second element to swap
 *
 * @return TRUE if swapped, FALSE if indexes specified are out of bounds
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::swap(unsigned long idx1, unsigned long idx2) 
{
    logic::Product temp;

    if (idx1 >= 0 && idx1 < productVec.size() && 
            idx2 >= 0 && idx2 < productVec.size() ) {

        if (idx1 == idx2) {
            return true;
        }

        temp = productVec[idx1];
        productVec[idx1] = productVec[idx2];
        productVec[idx2] = temp;
        return true;
    
    } 

    return false;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, clears the database.
 *
 * @return TRUE if cleared successfully
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::clear() 
{
    productVec.clear();
    return true;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, pushes the specified product to the end of the database.
 *
 * @param product The product to push
 *
 * @return TRUE if successful
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::push(const logic::Product& arg)
{
    logic::Product temp;
    temp.copy(arg);
    productVec.push_back(temp);
    return true;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, deletes the last element in the database
 *
 * @return TRUE if deleted successfully.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::pop() 
{
    productVec.pop_back();
    return true;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the size of the BasicDatabaseVector instance.
 *
 * @return The number of elements in the BasicDatabaseVector instance.
 */
/* ------------------------------------------------------------------*/
unsigned long BasicDatabaseVector::size() 
{
    return productVec.size();

}


/* ----------------------------------------------------------------*/
/**
 * @brief Test function, to be deleted.  To called, prints the elements in the
 * database to cout.
 */
/* ------------------------------------------------------------------*/
void BasicDatabaseVector::print() 
{
    for (unsigned long i = 0 ; i < productVec.size(); i++) {
        cout << productVec[i].getName() << " ";
    }
    cout << endl << endl;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, deletes the ith element, given by the index i.
 *
 * @param idx The index to delete.  Must be between 1 and size() - 1.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/

bool BasicDatabaseVector::delete_at(unsigned long idx)
{
    if (idx < 0 || idx >= size())
	{
        return false;
    } 
	else
	{
        productVec.erase(productVec.begin() + idx);
        return true;
    }

    /* For compiling safety */
    return false;
}

/* ----------------------------------------------------------------*/
/**
 * @brief Searches the given strings and saves the index in vector
 *
 * @param Term to be searched. Type of header to be searched under. 
 * Vector for storing index of matched items
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/

bool BasicDatabaseVector::search(string term, string search_type, vector<int> &index)
{
		
	int size = productVec.size(), i = 0;
	string barcode, price, stock, sold;
	regex rxTerm("(.*)" + term + "(.*)");

	index.clear();

	if(search_type != _sort_type){

		sort_by(search_type, SORT_ASCENDING);

	}
	
	if(search_type == PROD_NAME){
		while(i < size){

			if(regex_match(productVec.at(i).getName(),rxTerm))
				index.push_back(i);

			i++;

		}

		return true;
	}
	else if(search_type == PROD_CAT){
		while(i < size){

			if(regex_match(productVec.at(i).getCat(), rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else if(search_type == PROD_BARCODE){
        rxTerm = term;
		while(i < size){
			barcode = productVec.at(i).getBarcode();
			if(regex_match(barcode, rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else if(search_type == PROD_PRICE){
		
		while(i < size){
			price = common::utils::dtoa(productVec.at(i).getPrice());
			if(regex_match(price, rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else if(search_type == PROD_MANUFACTURER){
		while(i < size){

			if(regex_match(productVec.at(i).getManufacturer(), rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else if(search_type == PROD_STOCK){
		stock = common::utils::itoa(productVec.at(i).getNoOfStock());
		while(i < size){

			if(regex_match(stock, rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else if(search_type == PROD_SOLD){
		sold = common::utils::itoa(productVec.at(i).getNoSold());
		while(i < size){

			if(regex_match(sold, rxTerm))
				index.push_back(i);

			i++;

		}
		return true;
	}
	else
		return false;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, searches the entire database by ANDing the terms in the
 * specified input Product search term.  Writes the index of the matched terms
 * to idx_vector.
 *
 * @param term The term to match, typecast as a Product instance.
 * @param idx_vector The vector to match
 *
 * @return TRUE when complete.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabaseVector::search_by_prod(logic::Product term, vector<unsigned long>& idx_vector)
{
    vector<string> searches;

    idx_vector.clear();

    searches.push_back(term.getName());
    searches.push_back(term.getCat());
    searches.push_back(term.getBarcode());
    searches.push_back(common::utils::dtoa(term.getPrice()));
    searches.push_back(term.getManufacturer());
    searches.push_back(common::utils::itoa(term.getNoOfStock()));
    searches.push_back(common::utils::itoa(term.getNoSold()));

    regex 
        regexTerm0("(.*)" + searches[0] + "(.*)"),
        regexTerm1("(.*)" + searches[1] + "(.*)"),
        regexTerm2("(.*)" + searches[2] + "(.*)"),
        regexTerm3("(.*)" + searches[3] + "(.*)"),
        regexTerm4("(.*)" + searches[4] + "(.*)"),
        regexTerm5("(.*)" + searches[5] + "(.*)"),
        regexTerm6("(.*)" + searches[6] + "(.*)");

    string all = "(.*)";

    if (term.getBarcode().compare(string("000000000")) == 0) {
        regexTerm2 = all;
    }

    if (term.getPrice() == 0) {
        regexTerm3 = all;
    }

    if (term.getNoOfStock() == 0) {
        regexTerm5 = all;
    }

    if (term.getNoSold() == 0) {
        regexTerm6 = all;
    }



    for (unsigned long i = 0 ; i < size(); i++) {
        vector<string> entries;
        entries.push_back(get(i)->getName());
        entries.push_back(get(i)->getCat());
        entries.push_back(get(i)->getBarcode());
        entries.push_back(common::utils::dtoa(get(i)->getPrice()));
        entries.push_back(get(i)->getManufacturer());
        entries.push_back(common::utils::itoa(get(i)->getNoOfStock()));
        entries.push_back(common::utils::itoa(get(i)->getNoSold()));

        if (regex_match(entries[0], regexTerm0) &&
            regex_match(entries[1], regexTerm1) && 
            regex_match(entries[2], regexTerm2) && 
            regex_match(entries[3], regexTerm3) && 
            regex_match(entries[4], regexTerm4) && 
            regex_match(entries[5], regexTerm5) && 
            regex_match(entries[6], regexTerm6)) {
            idx_vector.push_back(i);
        }

    
    }

    return true;
}

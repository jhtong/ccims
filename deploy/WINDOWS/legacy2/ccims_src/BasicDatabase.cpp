/**
 * @file BasicDatabase.c
 * @brief Implementation for BasicDatabase class.
 * @author Joel Haowen TONG
 * @version 0.0.002
 * @date 2013-03-07
 */

#include <string.h>
#include <iostream>
#include <vector>

#include "BasicDatabase.h"
#include "BasicDatabaseList.h"
#include "Product.h"
#include "ProductFlags.h"
#include "AdtFlags.h"
#include "Utils.h"

using namespace std;
using namespace logic::adt;


/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor.  
 */
/* ------------------------------------------------------------------*/
BasicDatabase::BasicDatabase() 
{
	_sort_type = PROD_NAME;
	_sort_order = SORT_ASCENDING;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor
 */
/* ------------------------------------------------------------------*/
BasicDatabase::~BasicDatabase() {}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, inserts a product entry at the given index
 *
 * @param idx       The index to insert
 * @param product   The entry (of type logic::Product) to insert
 *
 * @return TRUE if successful
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::insert_at(unsigned long idx, 
                                logic::Product product)
{
    return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, inserts a product entry into the sorted database
 *		according to its sorting type(name, barcode,etc) and order(ascending, descending)
 * 
 * @param product   The entry (of type logic::Product) to insert
 *
 * @return TRUE if successful
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::insert(logic::Product product)
{
	return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, sorts the database by the given header category, by
 * either ASCENDING OR DESCENDING.
 *
 * @param header The header category to be sorted.
 * @param sort_type Either ASCENDING or DESCENDING
 *
 * @return True when sorted
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::sort_by(string header, string sort_order)
{
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, empties the database.
 *
 * @return TRUE when database is cleared.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::clear()
{
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, inserts an entry at the end of the database.
 *
 * @param product The product to insert
 *
 * @return TRUE when product is inserted.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::push(const logic::Product& arg) 
{
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, deletes the last entry from the database.
 *
 * @return TRUE when database is popped.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::pop() 
{
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, retrieves the ith element, given by the index i.
 *
 * @param idx The index to retrieve
 *
 * @return The ith element, typecasted as a product.
 */
/* ------------------------------------------------------------------*/
logic::Product* BasicDatabase::get(unsigned long idx)
{
    return NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, deletes the ith element, given by the index i.
 *
 * @param idx The index to delete.  Must be between 1 and size() - 1.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::delete_at(unsigned long idx) 
{
    return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, restocks the ith element, given by the index i.
 *
 * @param idx The index to restock.  Must be between 1 and size() - 1.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::restock_for(unsigned long idx, int val)
{
	Product *prod = get(idx);

	int stockRemain = prod->getNoOfStock();

	prod->setNoOfStock(val + stockRemain);

	return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, specify sales of the ith element, given by the index i.
 *
 * @param idx The index to edit the sales.  Must be between 1 and size() - 1.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::specifySale_for(unsigned long idx, int val)
{
	//no error checking done
	int total_val;
	Product *prod;
	prod = get(idx);
	
	total_val = prod->getNoSold() + val;
	prod->setNoSold(total_val);
	return true;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the size of the database
 *
 * @return The number of Product elements in the database.
 */
/* ------------------------------------------------------------------*/
unsigned long BasicDatabase::size() 
{
    return 0;
}

/* ----------------------------------------------------------------*/
/**
 * @brief When called, compares 2 products with a given header.
 *  the comparison results factors in the current sorting order.
 *
 * @param prod1-first product, prod2-second product
 *
 * @return if the sorting order is ascending, returns TRUE if prod1 < prod2, else FALSE.
 *         if the sorting order is descending, returns TRUE if prod1 > prod2, else FALSE.
 *         if prod1 == prod2, returns FALSE.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::compare(logic::Product &prod1, logic::Product &prod2)
{
	bool sortand = true;
	if (_sort_order == SORT_DESCENDING) 
        sortand = false;

	if(_sort_type == logic::PROD_NAME || _sort_type == logic::PROD_CAT||
		_sort_type == logic::PROD_MANUFACTURER || _sort_type == logic::PROD_BARCODE)
	{
		string cmp1 = "", cmp2 = "";

		if (_sort_type == logic::PROD_NAME) {
			cmp1 = prod1.getName();
			cmp2 = prod2.getName();

		} else if (_sort_type == logic::PROD_CAT) {
			cmp1 = prod1.getCat();
			cmp2 = prod2.getCat();

		} else if (_sort_type == logic::PROD_MANUFACTURER) {
			cmp1 = prod1.getManufacturer();
			cmp2 = prod2.getManufacturer();

		} else if (_sort_type == logic::PROD_BARCODE) {
			cmp1 = prod1.getBarcode();
			cmp2 = prod2.getBarcode();
		}
		/*see the description at the top of this function*/
		return ((sortand ^ (cmp1 > cmp2)) && (cmp1 != cmp2));

	} 
	else if(_sort_type == logic::PROD_PRICE ||
		_sort_type == logic::PROD_STOCK || _sort_type == logic::PROD_SOLD)
	{
		double cmp1 = 0, cmp2 = 0;

		if (_sort_type == logic::PROD_PRICE) {
			cmp1 = prod1.getPrice();
			cmp2 = prod2.getPrice();

		}  else if (_sort_type == logic::PROD_STOCK) {
			cmp1 = prod1.getNoOfStock();
			cmp2 = prod2.getNoOfStock();

		} else if (_sort_type == logic::PROD_SOLD) {
			cmp1 = prod1.getNoSold();
			cmp2 = prod2.getNoSold();
		}
		/*see the description at the top of this function*/
		return ((sortand ^ (cmp1 > cmp2)) && (cmp1 != cmp2));
	}
	
	return false;
}


unsigned long BasicDatabase::search_by(string header, string term)
{
    return 0;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, searches the entire database by ANDing the terms in the
 * specified input Product search term.  Writes the index of the matched terms
 * to idx_vector.
 *
 * @param term The term to match, typecast as a Product instance.
 * @param idx_vector The vector to match
 *
 * @return TRUE when complete.
 */
/* ------------------------------------------------------------------*/
bool BasicDatabase::search_by_prod(logic::Product term, vector<unsigned long>& idx_vector)
{
    return true;
}

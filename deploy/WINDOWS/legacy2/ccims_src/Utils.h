/**
 * @file Utils.h
 * @brief Common utils library
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-25
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>

using namespace std;


namespace common {
    namespace utils {
        /* ----------------------------------------------------------------*/
        /**
         * @brief This routine converts a double to
         * a string form.
         *
         * @param arg The number to typecast
         *
         * @return The number, typecasted as a string.
         */
        /* ------------------------------------------------------------------*/
        inline string dtoa(double arg)
        {
            stringstream aa;
            aa << arg;
            return aa.str();
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief This routine converts an integer to
         * a string form.
         *
         * @param arg The number to typecast
         *
         * @return The number, typecasted as a string.
         */
        /* ------------------------------------------------------------------*/
        inline string itoa(int arg)
        {
            stringstream aa;
            aa << arg;
            return aa.str();
        }
        

        inline string itoa(long arg)
        {
            stringstream aa;
            aa << arg;
            return aa.str();
        }


        inline string itoa(unsigned long arg)
        {
            stringstream aa;
            aa << arg;
            return aa.str();
        }
        

        /* ----------------------------------------------------------------*/
        /**
         * @brief This routine converts an unsigned integer to
         * a string form.
         *
         * @param arg The number to typecast
         *
         * @return The number, typecasted as a string.
         */
        /* ------------------------------------------------------------------*/
        inline string itoa(unsigned int arg)
        {
            stringstream aa;
            aa << arg;
            return aa.str();
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief This function converts a string to an int.
         *
         * @param arg The string to convert
         *
         * @return A signed int
         */
        /* ------------------------------------------------------------------*/
        inline int str_atoi(string arg)
        {
            return atoi(arg.c_str());
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief This function converts a string to a double.
         *
         * @param arg The string to convert
         *
         * @return A double
         */
        /* ------------------------------------------------------------------*/
        inline double str_atof(string arg)
        {
            return (double) atof(arg.c_str());
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief When called, strips the carriage return char '\r' from the
         * end of the string.
         *
         * This is needed due to Windows formatting the EOF character as \r\n,
         * while the UNIX equivalent is \n.  Will cause problems during
         * formatting by Ncurses.
         *
         * @param arg string to format
         *
         * @return the input string, without the '\r' character.
         */
        /* ------------------------------------------------------------------*/
        inline string dosToUnix(string arg) {
            if (!arg.empty() && arg[arg.size()-1] == '\r') {
                return arg.erase(arg.size() - 1);
            }
            return arg;
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief When called, this function strips trailing and leading
         * whitespaces from a string, including carriage returns.
         *
         * @param s The string to trim
         *
         * @return The trimmed string.
         */
        /* ------------------------------------------------------------------*/
        inline string trim(string s) 
        {
            if (s.empty()) {
                return s;
            }

            size_t p = s.find_first_not_of(" \t\n");
            s.erase(0,p);

            p = s.find_last_not_of(" \t\n");
            if (string::npos != p) {
                s.erase(p+1);
            }

            return s;
        }


        /* ----------------------------------------------------------------*/
        /**
         * @brief When called, this method truncates the string if it exceeds
         * the given width.
         *
         * @param s The string 
         * @param max The maximum width of the string to return 
         *
         * @return The truncated string.
         */
        /* ------------------------------------------------------------------*/
        inline string truncate(string s, unsigned long max) 
        {
            if (s.size() > max) {
                return s.substr(0, max - 2) + "..";
            }

            return s;
        }

    }
}

#endif

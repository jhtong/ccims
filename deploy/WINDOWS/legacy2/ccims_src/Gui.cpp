/**
 * @file Gui.cpp
 * @brief This class SHOULD be created at first at runtime, to initiate the CCIS GUI.
 * Use start() to start the GUI, and redraw() to refresh the GUI.  To print a
 * message to the status box, use status_print( (string) "String to print").
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */


#include "Gui.h"

/* TODO: Included for testing purposes */
#include "PrintObject.h"
#include "MenuHandler.h"
#include "Header.h"
#include "Interface.h"
#include "ColorPairs.h"


using namespace std;

namespace gui 
{
    /* --------------------------------------------------------------------------*/
    /**
     * @brief Constructor
     */
    /* ----------------------------------------------------------------------------*/
    Gui::Gui() {
        }

    /* --------------------------------------------------------------------------*/
    /**
     * @brief Destructor
     */
    /* ----------------------------------------------------------------------------*/
    Gui::~Gui() 
    {
        quit();
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Quit the GUI.  This function HAS to be called before deleting the
     * GUI object.
     */
    /* ----------------------------------------------------------------------------*/
    void Gui::quit()
        {
            free_mem();     // Free mem and kill whatever windows there are
        }



    /* --------------------------------------------------------------------------*/
    /**
     * @brief Start the GUI.  Has to be called after the GUI is initialized.
     */
    /* ----------------------------------------------------------------------------*/
    void Gui::start()
        {
            init();
            /* Call the Logic to load the file here, via Interface singleton */
            common::Interface::get_instance()->logToGui_load_file();

            /* Start listening */
            WIN_MID_LST_VIEW->run_listener();
        }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief When called, function redraws the GUI
     */
    /* ----------------------------------------------------------------------------*/
    void Gui::redraw()
    {
        WIN_STD_SCREEN->redraw();
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Internal function called by start().  When called, init() initializes
     * the window accordingly.
     */
    /* ----------------------------------------------------------------------------*/
    void Gui::init() 
        {
            WINDOW* std_screen;
            
            /* Do the routine stuff */
            std_screen = initscr();
			ColorPairs::change_default_colors();
            cbreak();
            clear();
            noecho();
            

            /* Create screen object */
            WIN_STD_SCREEN = new Screen( 0,
                                         0,
                                         0,
                                         0,
                                        std_screen);
            
            /* Initialize objects */
            HANDLER_MENU     = new MenuHandler(this);
            WIN_BTM_STAT_BOX = new Status(this);

            /* Create top menu object */
            WIN_TOP_MENU    = new Header(5,
                                WIN_STD_SCREEN->get_width(),
                                0,
                                0,
                                this);

            WIN_MID_LST_VIEW = new ScrollList(this,
                                WIN_STD_SCREEN->get_height() - 4 - 5,
                                WIN_STD_SCREEN->get_width(),
                                5, 0);

            /* TODO: For testing purposes - initialize test driver */
            common::PrintObject testObj;
            vector<string> temp_headers, temp_element;
            temp_headers.push_back("NAME:");
            temp_headers.push_back("CATEGORY:");
            temp_headers.push_back("BARCODE #:");
            temp_headers.push_back("PRICE:");
            temp_headers.push_back("MANUFACTURER:");
            temp_headers.push_back("# IN STOCK:");
            temp_headers.push_back("# SOLD:");
            testObj.make_headers(temp_headers);

            /* Start */
            temp_element.push_back("START");
            temp_element.push_back("Dried food");
            temp_element.push_back("12345678");
            temp_element.push_back("24.95");
            temp_element.push_back("geelee");
            temp_element.push_back("25");
            temp_element.push_back("90");
            testObj.insert(temp_element);
            temp_element.clear();

            for (unsigned int i = 0; i < 30; i++) {
                temp_element.push_back("Kimchi");
                temp_element.push_back("Preserved food");
                temp_element.push_back("2902913");
                temp_element.push_back("24.95");
                temp_element.push_back("Hee Hee");
                temp_element.push_back("1000");
                temp_element.push_back("500");
                testObj.insert(temp_element);
                temp_element.clear();

                temp_element.push_back("Milk");
                temp_element.push_back("Preserved food");
                temp_element.push_back("2902913");
                temp_element.push_back("24.95");
                temp_element.push_back("Hee Hee");
                temp_element.push_back("1000");
                temp_element.push_back("500");
                testObj.insert(temp_element);
                temp_element.clear();
            }
            status_print(string(testObj.get_field(10,1)));
        WIN_BTM_STAT_BOX->redraw();

            /* END */
            temp_element.push_back("END");
            temp_element.push_back("Preserved food");
            temp_element.push_back("2902913");
            temp_element.push_back("24.95");
            temp_element.push_back("Hee Hee");
            temp_element.push_back("1000");
            temp_element.push_back("500");
            testObj.insert(temp_element);
            temp_element.clear();

            /* For setting proportions */
            vector<float> vec_proportions;
            vec_proportions.push_back((float) 0.15);
            vec_proportions.push_back((float) 0.20);
            vec_proportions.push_back((float) 0.1);
            vec_proportions.push_back((float) 0.1);
            vec_proportions.push_back((float) 0.2);
            vec_proportions.push_back((float) 0.10);
            vec_proportions.push_back((float) 0.15);

            testObj.set_mask_at(10, true);
            testObj.set_mask_at(14, true);
            testObj.set_mask_at(5, true);
            testObj.set_mask_at(17, true);
            testObj.set_mask_at(60, true);
            testObj.set_mask_at(61, true);
            testObj.set_mask_at(30, true);

            WIN_MID_LST_VIEW->load(testObj);
            WIN_MID_LST_VIEW->set_proportions(vec_proportions);

            WIN_MID_LST_VIEW->init();
            /* End driver test */

        }

    void Gui::free_mem()
    {
        /* Kill the child screens */

        /* Free memory */
        delete WIN_TOP_MENU;
        WIN_TOP_MENU = NULL;

        delete WIN_MID_LST_VIEW;
        WIN_MID_LST_VIEW = NULL;
        
        delete WIN_BTM_STAT_BOX;
        WIN_BTM_STAT_BOX = NULL;

        delete WIN_STD_SCREEN;
        WIN_STD_SCREEN = NULL;

        delete HANDLER_MENU;
        HANDLER_MENU = NULL;
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Function allows user to access status print function from ROOT for 
     * proper OOP encapsulation
     *
     * @param arg String to display in status bar
     * @param printType if 0, message printed normally.  If 1, message printed
     * as an error.
     *
     */
    /* ----------------------------------------------------------------------------*/
    void Gui::status_print(string arg, unsigned int printType)
    {
        WIN_BTM_STAT_BOX->print(arg, printType);
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief This function allows another object to access the screen of the
     * current gui object. 
     *
     * @return the current parent screen of the Gui object
     */
    /* ----------------------------------------------------------------------------*/
    Screen* Gui::get_scr()
    {
        return WIN_STD_SCREEN;
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief When called, bubbles and transfers current focus to the menu handler.
     *
     * @return TRUE if there is a signal to quit, FALSE otherwise
     */
    /* ------------------------------------------------------------------*/
    bool Gui::proc_action()
    {
        return HANDLER_MENU->process();
    }


    /* ----------------------------------------------------------------*/
    /**
     * @brief Top-member function.  Meant to be called from the Interface
     * class.  When called, the ScrollList instance within the GUI is loaded
     * with a new PrintObject data object. 
     *
     * @param arg The new PrintObject instance to load
     */
    /* ------------------------------------------------------------------*/
    void Gui::update_scrollList(common::PrintObject& arg) 
    {
        WIN_MID_LST_VIEW->load(arg);
    }


    void Gui::clear_scrollList()
    {
        WIN_MID_LST_VIEW->clear_scr();
    }
}

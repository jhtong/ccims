/**
 * @file Header.cpp
 * @brief Implementation file for Header.cpp.  To be displayed as top portion
 * of GUI
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Gui.h"
#include "Header.h"
#include "Screen.h"
#include "ColorPairs.h"

#include <string>

using namespace std;
using namespace gui;


/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for the Header object.  When called, creates a
 * header of the specified height, width at the position (y,x).
 *
 * @param height Height of the header object
 * @param width Width of the header object
 * @param y Y position of the header object
 * @param x X position of the header object
 * @param root Pointer to the parent Gui instance.
 */
/* ------------------------------------------------------------------*/
Header::Header(unsigned int height, 
                unsigned int width, 
                unsigned int y, 
                unsigned int x, 
                Gui* root)
{
    _ROOT = root;
    _WIN_SELF = new Screen(height, width, y, x);
    _WIN_MENU_TEXT = new Screen(1, width, y + get_height() - 1, x);
    init();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Internal function, used to redraw the screen.
 */
/* ------------------------------------------------------------------*/
void Header::redraw()
{
    _WIN_SELF->redraw();
    _WIN_MENU_TEXT->redraw();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Moves the header to the position (y,x).
 *
 * @param y New Y position to move to
 * @param x New X position to move to
 */
/* ------------------------------------------------------------------*/
void Header::set_yx(unsigned int y, unsigned int x)
{
    _WIN_SELF->set_yx(y,x);
    _WIN_MENU_TEXT->set_yx(y + get_height() - 1, x);
    redraw();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, gets the width of the Header instance 
 *
 * @return Width of the header object
 */
/* ------------------------------------------------------------------*/
unsigned int Header::get_width()
{
    return _WIN_SELF->get_width();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns the height of the Header instance
 *
 * @return The height of the Header instance
 */
/* ------------------------------------------------------------------*/
unsigned int Header::get_height()
{
    return _WIN_SELF->get_height();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns the Y coordinate of the Header instance
 *
 * @return The Y coordinate of the Header instance
 */
/* ------------------------------------------------------------------*/
unsigned int Header::get_y()
{
    return _WIN_SELF->get_y();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns the X coordinate of the Header instance
 *
 * @return The X coordinate of the Header instance
 */
/* ------------------------------------------------------------------*/
unsigned int Header::get_x()
{
    return _WIN_SELF->get_x();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor of the Header instance
 */
/* ------------------------------------------------------------------*/
Header::~Header()
{
    delete      _WIN_SELF;
    delete      _WIN_MENU_TEXT;
    _ROOT          = NULL;
    _WIN_SELF      = NULL;
    _WIN_MENU_TEXT = NULL;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Internal function, used to initialize the Header instance.
 */
/* ------------------------------------------------------------------*/
void Header::init()
{
    ColorPairs::init();
    wbkgd(_WIN_SELF->get_win(), COLOR_PAIR(ColorPairs::COLOR_PAIR_TOP_WIN));
    wbkgd(_WIN_MENU_TEXT->get_win(), COLOR_PAIR(ColorPairs::COLOR_PAIR_TOP_MENU));
    mvwprintw(_WIN_SELF->get_win(), 0,0,"%s",  "   ___ ___ ___ __  __ ___ \n  / __/ __|_ _|  \\/  / __|\n | (_| (__ | || |\\/| \\__ \\\n  \\___\\___|___|_|  |_|___/");

    wattron(_WIN_MENU_TEXT->get_win(), A_BOLD);
    mvwprintw(_WIN_MENU_TEXT->get_win(), 0,0, "%s", "<1> Add | <2> Scrap | <3> Specify sales | <4> Restock | <5> Search | <6> Top-selling product | <7> Top manufacturer | <8> Top product by cat");
    wattroff(_WIN_MENU_TEXT->get_win(), A_BOLD);
    redraw();
}

/**
 * @file Button.h
 * @brief Derived class from FormUI.  Used as a form component.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-20
 */

#pragma once
#ifndef _BUTTON_H
#define _BUTTON_H

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"


using namespace std;

namespace gui 
{
    namespace form 
    {
        /* ----------------------------------------------------------------*/
        /**
         * @brief The Button class is a derived class of the FormUI component.
         * It enables the user to press buttons (such as submit, refresh, etc.) in
         * the form.
         */
        /* ------------------------------------------------------------------*/
        class Button : public FormUI
        {
            public:
                Button(unsigned int         height,         // Create UI with dimensions height, width, y, x
                            unsigned int    width,
                            unsigned int    y,
                            unsigned int    x,
                            string          label,
                            Form*           form_ref = NULL);

                ~Button();

                virtual void        focus(bool (*actionFn)(vector<string>));
                virtual void        focus();
                virtual string      get_data();
                void                set_action(bool (*actionFn)(vector<string>));
                void                remove_action();
            
            private:
                virtual void    make_display();
                string          _label;
                bool            (*_action)(vector<string>);

        };
    }
} 

#endif

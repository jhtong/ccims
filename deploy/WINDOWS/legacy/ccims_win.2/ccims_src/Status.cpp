/**
 * @file Status.cpp
 * @brief This file contains the implementation for the Status box class,
 * located at the bottom of the interface.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Gui.h"                        // Circularly-linked class 

#include <string>

#include "Status.h"
#include "ColorPairs.h"

using namespace std;


namespace gui 
{

    /* --------------------------------------------------------------------------*/
    /**
     * @brief Default contructor
     */
    /* ----------------------------------------------------------------------------*/
    Status::Status() {}


    /* --------------------------------------------------------------------------*/
    /**
     * @brief        Constructor that takes an argument of root.
     *
     * @param root      Parent GUI to reference from
     */
    /* ----------------------------------------------------------------------------*/
    Status::Status(Gui* root) 
        {
            _ROOT = root;
            init();
        }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief        Set the parent GUI to reference from
     *
     * @param root      Parent GUI to reference from
     */
    /* ----------------------------------------------------------------------------*/
    void Status::set_root(Gui* root)
    {
        _ROOT = root;
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Destructor
     */
    /* ----------------------------------------------------------------------------*/
    Status::~Status() 
    {
        quit();
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Clears the output of the status bar
     */
    /* ----------------------------------------------------------------------------*/
    void Status::clear_scr()
    {
        wclear(STATUS_SCR->get_win());
        wbkgd(STATUS_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_STATUS_NORMAL));
        redraw();
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief    Displays a string in Status bar
     *
     * @param arg   String to display
     * @param printType if 0, message printed normally.  If 1, message printed
     * as an error.
     */
    /* ----------------------------------------------------------------------------*/
    void Status::print(string arg, unsigned int printType)
        {
            clear_scr();

            if (printType == 1) {
                wattron(STATUS_SCR->get_win(), COLOR_PAIR(ColorPairs::COLOR_PAIR_STATUS_ERROR));
                mvwprintw(STATUS_SCR->get_win(), 1, 1, arg.c_str());
                wattroff(STATUS_SCR->get_win(), COLOR_PAIR(ColorPairs::COLOR_PAIR_STATUS_ERROR));

            } else if (printType == 2) {
                wattron(STATUS_SCR->get_win(), A_BOLD);
                mvwprintw(STATUS_SCR->get_win(), 1, 1, arg.c_str());
                wattroff(STATUS_SCR->get_win(), A_BOLD);

            } else {
                mvwprintw(STATUS_SCR->get_win(), 1, 1, arg.c_str());
            }

            redraw();
        }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Redraws the screen.  Call when necessary.
     */
    /* ----------------------------------------------------------------------------*/
    void Status::redraw()
        {
            _ROOT->redraw();
            STATUS_SCR->redraw();

        }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief return the screen object of the Status class
     *
     * @return Screen object of the Status class
     */
    /* ----------------------------------------------------------------------------*/
    Screen* Status::get_scr()
    {
        return STATUS_SCR;
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Initializes the Status bar
     */
    /* ----------------------------------------------------------------------------*/
    void Status::init()
    {
        STD_SCR = _ROOT->get_scr();
        create_win(3);

    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief        Create a window of height pixels high, and as wide as stdscr is.
     *
     * @param height    Desired height of the status bar bar to create
     */
    /* ----------------------------------------------------------------------------*/
    void Status::create_win(int height)
    {
        ColorPairs::make_colors();
        _width = STD_SCR->get_width();
        _height = height;

        STATUS_SCR = new Screen(_height, 
                                _width, 
                                STD_SCR->get_height() - _height, 
                                0);

        ColorPairs::init();
        print("Status box ready");
        redraw();
    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Frees the memory of the Status bar
     */
    /* ----------------------------------------------------------------------------*/
    void Status::free_mem()
    {
        /* do not delete root pointer; let GUI handle this */
        _ROOT = NULL;

        delete STATUS_SCR;
        STATUS_SCR = NULL;

    }


    /* --------------------------------------------------------------------------*/
    /**
     * @brief Delete the status bar and perform relevant dellocation of memory
     * tasks
     */
    /* ----------------------------------------------------------------------------*/
    void Status::quit()
    {
        free_mem();
    }
}

/**
 * @file main.cpp
 * @brief This is the where main() is located.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-18
 */


#include <stdio.h>
/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
#include <string>

//#include "Gui.h"
#include "Interface.h"

using namespace std;


int main(int argc, const char *argv[])
{
    //gui::Gui gui;
    //gui.start();
    common::Interface::get_instance()->start();
    common::Interface::get_instance()->quit();
    return 0;
}

/**
 * @file ScrollList.h
 * @brief The ScrollList class is a scrolllist UI created for the purposes
 * of displaying tabular data to the end-user.  The scroll list features
 * current selection, and alternate coloring of rows.  
 *
 * The ScrollList instance only accepts tabular data input in the form of the
 * ScrollList object.  Hence, it is necessary that such an object be created
 * and loaded to the ScrollList object using load(), when necessary.
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SCROLL_LIST_H
#define _SCROLL_LIST_H

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* End header include for ncurses */
#include <string>

#include "Screen.h"
#include "PrintObject.h"


using namespace std;

namespace gui 
{
    class Gui;                          // Forward declaration for cyclical reference to Gui.h

    /* ----------------------------------------------------------------*/
    /**
     * @brief The ScrollList class is an Ncurses UI widget, that allows one to
     * draw a list box.  It features selection highlighting, alternate coloring
     * and N * M rows and columns.  
     *
     * The ScrollList class accepts input in the format of a PrintObject instance.
     *
     * @author TONG Haowen Joel
     * @version 0.0.001
     * @date 2013-02-15
     *
     */
    /* ------------------------------------------------------------------*/
    class ScrollList 
    {
    public:
        ScrollList();
        ScrollList(Gui* root, 
                    unsigned int height,
                    unsigned int width,
                    unsigned int y,  
                    unsigned int x);


        ~ScrollList();

        void                    set_root(Gui* root);
        void                    quit();

        void                    print(unsigned int y, unsigned int x, string msg);
        void                    redraw();
        void                    clear_scr();
        void                    load(common::PrintObject arg);
        void                    run_listener();
        void                    set_proportions(vector<float> args);
        void                    init();


    private:
        Screen*                 SCRL_LIST_SCR;
        Gui*                    _ROOT;
        common::PrintObject*    _printObject;
        vector<float>           _proportions;
        unsigned int            _width, _height;
        unsigned int            _scroll_height; // not including the headers
        unsigned int            _x, _y;

        void                    free_mem();
        void                    print_line(unsigned int y_pos, 
                                            unsigned int color_state,
                                            unsigned long list_sel_pos,
                                string msg);

        void                    print(unsigned int list_start_pos, 
                                        unsigned long list_sel_pos,
                                        unsigned short max_height);

        void                    reset();

        unsigned int            get_search_index(bool next, 
                                                unsigned int cur_pos);

        unsigned int            get_search_max();

        /* for the scrollbar */
        unsigned int            box_cur_pos;    // selection wrt _printObject idx
        unsigned int            box_cur_min;    // 0th entry of scroll list_sel_pos
                                                // idx wrt _printObject idx


    };
}
#endif

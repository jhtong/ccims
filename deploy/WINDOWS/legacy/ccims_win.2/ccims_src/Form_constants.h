/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * @file Form_constants.h
 * @brief This header file stores the flags used by the Form (and its
 * associated) classes.  Flags include those to be used by the FormFactory 
 * singleton class.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */
#pragma once
#ifndef _FORM_CONSTANTS_H
#define _FORM_CONSTANTS_H

#include <string>

using namespace std;


namespace gui
{
    namespace form 
    {
       const    string  EMPTY                   = "EMPTY";

       /* For the Form factory generation */
       const    int     GEN_ADD_FORM            = 0;
       const    int     GEN_SCRAP_FORM          = 1;
       const    int     GEN_SALE_FORM           = 2;
       const    int     GEN_RESTOCK_FORM        = 3;
       const    int     GEN_SEARCH_FORM         = 4;
       const    int     GEN_STATS_FORM          = 5;

    }

}

#endif

/**
 * @file Logic.h
 * @brief This file contains the declaration of the Logic class, used for the
 * backend.
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-25
 */
/* Copyright (c) <2012> <C01-05> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _LOGIC_H
#define _LOGIC_H

#include <vector>
#include <string>
#include "Product.h"
#include "PrintObject.h"
#include "BasicDatabaseVector.h"
#include "BasicDatabaseList.h"

#include "Timer.h"
#include "File.h"


using namespace std;


namespace logic 
{
    /* ----------------------------------------------------------------*/
    /**
     * @brief The Logic class functions as both the backend and database of
     * CICMS.  This should be the base for all operations dealing with logic.
     */
    /* ------------------------------------------------------------------*/
    class Logic
    {
        public:
            Logic();
            ~Logic();

            void                quit();
            bool                load();
            bool                save();

            bool                addProduct      (vector<string> arg);
            bool                delProduct      (vector<string> arg);
            bool                specifySale     (vector<string> arg);
            bool                restock         (vector<string> arg);
            bool                searchProduct   (vector<string> arg);

            bool                sortBestProduct();
            bool                sortBestManufacturer();
            bool                sortBestProductCategory();
            bool                sortBestSellingProduct();

            bool                get_dbMode();
            void                set_dbMode(bool arg);
            void                toggle_dbMode();

            void                genStats();

			bool				checkBarcodeUnique(Product prod);
            bool                is_unique(vector<string> arg);

        private:
            common::PrintObject convertToPrintObj(logic::adt::BasicDatabase& input);
            string                              _filename;

            Product                             convertToProd(vector<string>& input);

            void                                update_gui_scroll(vector<unsigned long> masks = vector<unsigned long>());
            void                                update_gui_status(string arg, unsigned int printType = 0);
            bool                                update_gui_scroll_mask(vector<unsigned long>& indexes, common::PrintObject& printObject);
            void                                call_wait();

            /* for the logic ADTs */
            bool                                _dbMode;
            logic::adt::BasicDatabaseVector     _db_vec;
            logic::adt::BasicDatabaseList       _db_list;

            /* For reading and writing to file */
            logic::File                         _file;

            struct manufacturerElement {
                string name;
                unsigned long qty;
            };


            /* Timer for testing */
            common::utils::Timer                _timer;

    };
}
#endif

/**
 * @file UIText.h
 * @brief Derived class from FormUI.  Used as a form component.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-20
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _UI_TEXT_H
#define _UI_TEXT_H

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"

using namespace std;

namespace gui 
{
    namespace form 
    {
        /* ----------------------------------------------------------------*/
        /**
         * @brief The UIText class creates static text for the Form.  It is a
         * derived class from the FormUI base class.
         *
         * When called with an empty string, the UIText behaves like a separator
         * form component.
         */
        /* ------------------------------------------------------------------*/
        class UIText : public FormUI 
        {
            public:
                UIText(unsigned int height,
                        unsigned int width,
                        unsigned int y,
                        unsigned int x,
                        string label,
                        Form*form_ref = NULL);
                ~UIText();
                virtual void    focus();
                virtual void    set_label(string label);
                virtual string  get_label();

            private:
                string  _label;
                void    make_display();

        };
    }
}

#endif

/**
 * @file ProductFlags.h
 * @brief Flags file that contains the IDs for all product flags.  To be used
 * by the gui::form::FormFactory, for ID identification.
 * @author C01-05 
 * @version 0.0.001
 * @date 2013-02-27
 */
/* Copyright (c) <2012> <C01-05>
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _PRODUCT_FLAGS_H
#define _PRODUCT_FLAGS_H

#include <string>


using namespace std;

namespace logic 
{
    const string        PROD_NAME           = "NAME";
    const string        PROD_CAT            = "CAT";
    const string        PROD_BARCODE        = "BARCODE";
    const string        PROD_PRICE          = "PRICE";
    const string        PROD_MANUFACTURER   = "MANUFACTURER";
    const string        PROD_STOCK          = "STOCK";
    const string        PROD_SOLD           = "SOLD";
    const string        PROD_SUBMIT         = "SUBMIT";
    const string        PROD_RESTOCK        = "RESTOCK";
}

#endif

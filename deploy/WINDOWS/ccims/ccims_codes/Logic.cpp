/**
 * @file Logic.cpp
 * @brief Implementation file for Logic class
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-25
 */
#include <vector>
#include <string>
#include "Logic.h"
#include "Product.h"
#include "Interface.h"
#include "PrintObject.h"
#include "Utils.h"
#include "ProductFlags.h"

#include "BasicDatabase.h"
#include "BasicDatabaseVector.h"
#include "BasicDatabaseList.h"

#include "Timer.h"
#include "TimerFlags.h"
#include "File.h"

#include "BatchJob.h"

using namespace std;
using namespace logic;


/* ----------------------------------------------------------------*/
/**
 * @brief Constructor
 */
/* ------------------------------------------------------------------*/
Logic::Logic() 
{
    _dbMode = true;
    _timer.set_precision(common::utils::timer::PREC_MICRO);
    _timer.reset();
    
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor
 */
/* ------------------------------------------------------------------*/
Logic::~Logic() 
{
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, deallocates memory pointers and closes all Logic
 * operations.
 */
/* ------------------------------------------------------------------*/
void Logic::quit() 
{
    save();

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, loads information from a target file, and parses it.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool Logic::load() 
{
    _file.load(_db_vec);
    _file.load(_db_list);

    /* Call GUI to redraw the screen, when this function is called */
    /* This function should be called only once */
    if (_dbMode) {
        common::Interface::get_instance()->logToGui_redraw_screen(convertToPrintObj(_db_vec));
    
    } else {
        common::Interface::get_instance()->logToGui_redraw_screen(convertToPrintObj(_db_list));
    
    }
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, saves information to a target file.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool Logic::save() 
{
    return _file.save(_db_list);
}


/* ----------------------------------------------------------------*/
/**
 * @brief Adds a new product to the database with checking of
 * unique barcode
 *
 * @param arg The information to add.
 *
 * @return TRUE if success, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::addProduct(vector<string> arg, bool printFlag) 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

        if(is_unique(arg)){

                /* Insert code to add product to database */

                Product temp = convertToProd(arg);
                _timer.reset();
                _timer.start();
                _db_vec.insert(temp);
                _timer.end();
                elapsed1 = _timer.elapsedTimeStr();

                _timer.reset();
                _timer.start();
                _db_list.insert(temp);
                _timer.end();
                elapsed2 = _timer.elapsedTimeStr();
    
                if (!_dbMode) {
                        elapsed1 = elapsed2;

                }

                if(printFlag){
                        update_gui_scroll();
                        update_gui_status("Product added in " + elapsed1 + ".", 2);
                        call_wait();
                        return true;
                }

                return true;

        }
        else
                return false;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Deletes a new product to the database with unique barcode
 * barcode checking
 *
 * @param arg The information to delete
 *
 * @return TRUE if success, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::delProduct(vector<string> arg, bool printFlag) 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Remove product from database */
        if(!is_unique(arg)){
                Product temp = convertToProd(arg);
                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                vector<int> searchResult;
                _db_vec.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        _db_vec.delete_at(searchResult[0]);
                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed1 = _timer.elapsedTimeStr();

                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                searchResult.clear();
                _db_list.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        _db_list.delete_at(searchResult[0]);
                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed2 = _timer.elapsedTimeStr();
    
                if (!_dbMode) {
                        elapsed1 = elapsed2;

                }

                if(printFlag){
                        update_gui_scroll();
                        update_gui_status("Product deleted in " + elapsed1 + ".", 2);
                        call_wait();
                        return true;
                }

                return true;
        }
        else 
		{
			if(printFlag){
				update_gui_status("ERROR: Invalid barcode!", 1);
				call_wait();
			}
			return false;
		}
}


/* ----------------------------------------------------------------*/
/**
 * @brief Specifies sale of a new product to the database.
 *
 * @param arg The sales to specify
 *
 * @return TRUE if success, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::specifySale(vector<string> arg, bool printFlag) 
{
        /* String for getting time elapsed */
        string elapsed1 = "x", elapsed2 = "y";

        /* Bool errorFlag */
        bool statusFlag = true;

        if(!is_unique(arg)){
                /* Specify sale of product from database */
                Product temp = convertToProd(arg);

                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                vector<int> searchResult;
                _db_vec.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        Product* aa = _db_vec.get(searchResult[0]);
                        long newSold = aa->getNoSold() + temp.getNoSold();
                        long newStock = aa->getNoOfStock() - temp.getNoSold();
                        if (newStock < 0) {
                            /* output error */
                            statusFlag = false;

                        } else {
                                aa->setNoSold(newSold);
                                aa->setNoOfStock(newStock);
                        }
                        aa = NULL;
                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed1 = _timer.elapsedTimeStr();

                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                searchResult.clear();
                _db_list.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        Product* aa = _db_list.get(searchResult[0]);
                        long newSold = aa->getNoSold() + temp.getNoSold();
                        long newStock = aa->getNoOfStock() - temp.getNoSold();
                        if (newStock < 0) {
                            /* output error */
                            statusFlag = false;

                        } else {
                                aa->setNoSold(newSold);
                                aa->setNoOfStock(newStock);
                        }
                        aa = NULL;
                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed2 = _timer.elapsedTimeStr();

                if (!_dbMode) {
                        elapsed1 = elapsed2;

                }

                if(printFlag) {
                    if (statusFlag) {
                        update_gui_scroll();
                        update_gui_status("Stock modified in " + elapsed1 + ".", 2);
                    
                    } else {
                        update_gui_status("ERROR: Insufficient stock!", 1);
                    
                    }

                    call_wait();
                }

                return statusFlag;
        } else 
		{
			if(printFlag) {
				update_gui_status("ERROR: Invalid barcode!", 1);
				call_wait();
			}
			return false;
        }
}


/* ----------------------------------------------------------------*/
/**
 * @brief Restocks a product in database
 *
 * @param arg The item to restock
 *
 * @return TRUE if success, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::restock(vector<string> arg, bool printFlag) 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Specify sale of product from database */
        if(!is_unique(arg)){
                Product temp = convertToProd(arg);
                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                vector<int> searchResult;
                _db_vec.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        Product* aa = _db_vec.get(searchResult[0]);
                        long newStock = aa->getNoOfStock() + temp.getNoOfStock();
                        aa->setNoOfStock(newStock);
                        aa = NULL;

                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed1 = _timer.elapsedTimeStr();

                _timer.reset();
                _timer.start();
                //-- Insert block of code to execute -------//
                searchResult.clear();
                _db_list.search(temp.getBarcode(), PROD_BARCODE, searchResult);
                if (searchResult.size() == 1) {
                        Product* aa = _db_list.get(searchResult[0]);
                        long newStock = aa->getNoOfStock() + temp.getNoOfStock();
                        aa->setNoOfStock(newStock);
                        aa = NULL;

                }
                //-- End insert block of code to execute --//
                _timer.end();
                elapsed2 = _timer.elapsedTimeStr();
    
                if (!_dbMode) {
                        elapsed1 = elapsed2;

                }

                if(printFlag){
                        update_gui_scroll();
                        update_gui_status("Stock restocked in " + elapsed1 + ".", 2);
                        call_wait();
                        return true;
                }

                return true;
        }
        else
		{
			if(printFlag) {
				update_gui_status("ERROR: Invalid barcode!", 1);
				call_wait();
			}
			return false;
		}
}




/* ----------------------------------------------------------------*/
/**
 * @brief Searches a product in database
 *
 * @param arg The filter to use, in a Product form
 *
 * @return TRUE if success, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::searchProduct(vector<string> arg) 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Remove product from database */

    Product temp = convertToProd(arg);
    vector<unsigned long> searchResult;

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    if (_dbMode) {
        _db_vec.search_by_prod(temp, searchResult);

    }
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed1 = _timer.elapsedTimeStr();

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    if (!_dbMode) {
        searchResult.clear();
        _db_list.search_by_prod(temp, searchResult);
    
    }
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed2 = _timer.elapsedTimeStr();
    
    if (!_dbMode) {
        elapsed1 = elapsed2;

    }

    update_gui_scroll(searchResult);
	if (searchResult.size() == 0)
		update_gui_status("ERROR: No products matched the keyword(s)!", 1);
	else
		update_gui_status("Products searched in " + elapsed1 + ".", 1);
    call_wait();

    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, converts the database to a PrintObject instance for use
 * in the GUI.
 *
 * @param input A vector of Products to convert
 * @param output A modifiable PrintObject instance used for display
 */
/* ------------------------------------------------------------------*/
common::PrintObject Logic::convertToPrintObj( 
            logic::adt::BasicDatabase& input)
{

    common::PrintObject print_object;
    /* Create the headers */
    vector<string> temp_headers;
    temp_headers.push_back("NAME:");
    temp_headers.push_back("CATEGORY:");
    temp_headers.push_back("BARCODE #:");
    temp_headers.push_back("PRICE:");
    temp_headers.push_back("MANUFACTURER:");
    temp_headers.push_back("# IN STOCK:");
    temp_headers.push_back("# SOLD:");
    print_object.make_headers(temp_headers);

    /* Iterate, typecast as string if number */
    for (unsigned long i = 0; i < _db_vec.size(); i++) {
        Product* pdt = _db_vec.get(i);

        
        vector<string> temp_element;
        temp_element.push_back(pdt->getName());
        temp_element.push_back(pdt->getCat());
        temp_element.push_back(pdt->getBarcode());
        temp_element.push_back(common::utils::dtoa(pdt->getPrice()));
        temp_element.push_back(pdt->getManufacturer());
        temp_element.push_back(common::utils::itoa(pdt->getNoOfStock()));
        temp_element.push_back(common::utils::itoa(pdt->getNoSold()));

        print_object.insert(temp_element, false);
        pdt = NULL;
    }

    /* Proportions do not need to be set.  This is done by Gui class. */

    return print_object;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, uses an intelligent filter that parses a 2 * N vector
 * (from gui::Form) into a Product object.  Fields left unspecified are empty
 * strings.  Please see ProductFlags.h for a list of available flag IDs.
 *
 * @param input the 2 * N vector of strings to parse
 *
 * @return The converted Product object
 */
/* ------------------------------------------------------------------*/
Product Logic::convertToProd(vector<string>& input) 
{
    Product temp;

    for (unsigned int i = 0; i < input.size(); i+= 2) {
        string id = input[i];
        string val = input[i + 1];

        if (id == logic::PROD_NAME) {
            temp.setName(val);

        } else if (id == logic::PROD_CAT) {
            temp.setCat(val);

        } else if (id == logic::PROD_BARCODE) {
            temp.setBarcode(val);

        } else if (id == logic::PROD_PRICE) {
            temp.setPrice(common::utils::str_atof(val));

        } else if (id == logic::PROD_MANUFACTURER) {
            temp.setManufacturer(val);

        } else if (id == logic::PROD_STOCK) {
            temp.setNoOfStock(common::utils::str_atoi(val));

        } else if (id == logic::PROD_SOLD) {
            temp.setNoSold(common::utils::str_atoi(val));

        } else if (id == logic::PROD_RESTOCK) {

        }
    }

    return temp;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, gets the current database mode.
 *
 * @return TRUE if vector, FALSE if linked list
 */
/* ------------------------------------------------------------------*/
bool Logic::get_dbMode()
{
    return _dbMode;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the current database mode
 *
 * @param arg TRUE if vector, FALSE if linked list.
 */
/* ------------------------------------------------------------------*/
void Logic::set_dbMode(bool arg) 
{
    _dbMode = arg;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, toggles the database mode
 */
/* ------------------------------------------------------------------*/
void Logic::toggle_dbMode() 
{
    _dbMode = !_dbMode;

    /* Write to screen */
    string mode = "";
    if (get_dbMode()) {
        mode = "vector";

    } else {
        mode = "linked list";

    }

    update_gui_status(string ("TOGGLED: ") + mode + string (" implementation"), 2);
    call_wait();

}


/* ----------------------------------------------------------------*/
/**
 * @brief Private function used by logic to update screen.
 */
/* ------------------------------------------------------------------*/
void Logic::update_gui_scroll(vector<unsigned long> masks) 
{
    /* Make new print object */
    common::PrintObject printObject;

    /* DB flag is vector */
    if (_dbMode) {
        printObject = convertToPrintObj(_db_vec);
        
    /* DB flag is linked list */
    } else {
        printObject = convertToPrintObj(_db_list);
    }

    update_gui_scroll_mask(masks, printObject);

    /* Do the usual stuff */
    common::Interface::get_instance()->logToGui_redraw_screen(printObject);

}


/* ----------------------------------------------------------------*/
/**
 * @brief Private function used by screen to update status.
 *
 * @param arg Message to write.
 * @param printType if 0, message printed normally.  If 1, message printed
 * as an error.
 */
/* ------------------------------------------------------------------*/
void Logic::update_gui_status(string arg, unsigned int printType) 
{
    common::Interface::get_instance()->logToGui_update_status(arg, printType);

}


/* ----------------------------------------------------------------*/
/**
 * @brief Function used to wait for key input before status is erased.
 */
/* ------------------------------------------------------------------*/
void Logic::call_wait() 
{
    /* Wait for key input before clearing */
    common::Interface::get_instance()->wait();
}


bool Logic::update_gui_scroll_mask(vector<unsigned long>& indexes, common::PrintObject& printObject)
{
    for (unsigned long i = 0; i < indexes.size(); i++) {
        printObject.set_mask_at(indexes[i], true);
    }

    return true;
}

/*----------------------------------------------------------------------------
* @brief This method sort the database by best-selling products in
* descending order.
* @details It also get the index of product for updating scroll list.
* @return Status of successful execution.
* -----------------------------------------------------------------------------*/
bool Logic::sortBestProduct() 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Remove product from database */

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_vec.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed1 = _timer.elapsedTimeStr();

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_list.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed2 = _timer.elapsedTimeStr();
    
    if (!_dbMode) {
        elapsed1 = elapsed2;

    }

    
    /* Highlight the first selections with the same number sold */
    vector<unsigned long> searchResult;
    long acc = _db_vec.get(0)->getNoSold();
    for (unsigned long i = 0 ; i < _db_vec.size() && acc == _db_vec.get(i)->getNoSold(); i++) {
        searchResult.push_back(i);
    
    }

    update_gui_scroll(searchResult);
    update_gui_status("Product sorted by # sold in " + elapsed1 + ".", 2);
    call_wait();

    return true;
}

/*----------------------------------------------------------------------------
* @brief This method sort the database by manufacturer in the order of 
* best-selling products.
* @details It also get the index of product for updating scroll list.
* @return Status of successful execution.
* -----------------------------------------------------------------------------*/
bool Logic::sortBestManufacturer() 
{

    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    _timer.reset();
    _timer.start();

    _db_vec.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    _db_vec.sort_by(logic::PROD_MANUFACTURER, logic::adt::SORT_ASCENDING);

    _timer.end();
    elapsed1 = _timer.elapsedTimeStr();

    _timer.reset();
    _timer.start();

    _db_list.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    _db_list.sort_by(logic::PROD_MANUFACTURER, logic::adt::SORT_ASCENDING);

    _timer.end();
    elapsed2 = _timer.elapsedTimeStr();
    
    if (!_dbMode) {
        elapsed1 = elapsed2;
    }

    /* Highlight the first selections with the same number sold */
    vector<unsigned long> searchResult;
    vector<manufacturerElement> man_list;
    manufacturerElement temp;

    for (unsigned long i = 0 ; i < _db_vec.size(); i++){

        if (man_list.empty()) {
            temp.name = _db_vec.get(i)->getManufacturer();
            temp.qty =  _db_vec.get(i)->getNoSold();
            man_list.push_back(temp);

        } else if (man_list.back().name.compare(_db_vec.get(i)->getManufacturer()) == 0) {
            man_list.back().qty += _db_vec.get(i)->getNoSold();

        } else {
            temp.name = _db_vec.get(i)->getManufacturer();
            temp.qty =  _db_vec.get(i)->getNoSold();
            man_list.push_back(temp);
            
        }
    }


    temp = man_list[0];

    for (unsigned long b = 0; b < man_list.size(); b++) {
        if (temp.qty < man_list[b].qty) {
            temp = man_list[b];
        }
    
    }

    searchResult.clear();
    Product tempProd;
    tempProd.setManufacturer(temp.name);

    if (!man_list.empty()) {
        tempProd.setManufacturer(temp.name);
        _db_vec.search_by_prod(tempProd, searchResult);
    }
    

    update_gui_scroll(searchResult);
    update_gui_status("Best Manufacturer is " + temp.name + ".  Sorted in " + elapsed1 + ".", 2);
    call_wait();

    return true;
}

/*----------------------------------------------------------------------------
* @brief This method sort the database by category sold in the order of
* best-selling products.
* @details It also get the index of product for updating scroll list.
* @return Status of successful execution.
* -----------------------------------------------------------------------------*/
bool Logic::sortBestProductCategory() 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Remove product from database */

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_vec.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    _db_vec.sort_by(logic::PROD_CAT, logic::adt::SORT_ASCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed1 = _timer.elapsedTimeStr();

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_list.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    _db_list.sort_by(logic::PROD_CAT, logic::adt::SORT_ASCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed2 = _timer.elapsedTimeStr();
    
    if (!_dbMode) {
        elapsed1 = elapsed2;

    }

    /* Highlight the first selections with the same number sold */
    vector<unsigned long> searchResult;
    long acc             = _db_vec.get(0)->getNoSold();
    string curCat       = _db_vec.get(0)->getCat();

    for (unsigned long i = 0 ; i < _db_vec.size(); i++) {
        if (curCat.compare(_db_vec.get(i)->getCat()) == 0) {
            if (acc == _db_vec.get(i)->getNoSold()) {
                searchResult.push_back(i);

            } else {
                
            }
        } else {
            acc = _db_vec.get(i)->getNoSold();
            curCat = _db_vec.get(i)->getCat();
            searchResult.push_back(i);
        }
    
    }

    update_gui_scroll(searchResult);
    update_gui_status("Top-selling products sorted by category in " + elapsed1 + ".", 2);
    call_wait();

    return true;
}


/*----------------------------------------------------------------------------
* @brief This method sort the database by number of products sold in
* descending order.
* @return Status of successful execution.
* -----------------------------------------------------------------------------*/
bool Logic::sortBestSellingProduct() 
{
    /* String for getting time elapsed */
    string elapsed1 = "x", elapsed2 = "y";

    /* Remove product from database */

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_vec.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed1 = _timer.elapsedTimeStr();

    _timer.reset();
    _timer.start();
    //-- Insert block of code to execute -------//
    _db_list.sort_by(logic::PROD_SOLD, logic::adt::SORT_DESCENDING);
    //-- End insert block of code to execute --//
    _timer.end();
    elapsed2 = _timer.elapsedTimeStr();
    
    if (!_dbMode) {
        elapsed1 = elapsed2;

    }

    update_gui_scroll();
    update_gui_status("Product sorted by # sold in " + elapsed1 + ".", 2);
    call_wait();

    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, determines if a product barcode is unique, i.e. the
 * barcode has not been detected in the database.
 *
 * @param arg A 2 * N array of strings to process, from the form.
 *
 * @return TRUE if barcode is unique, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::is_unique(vector<string> arg) 
{
    Product temp = convertToProd(arg);
    temp.setName("");
    temp.setCat("");
    temp.setManufacturer("");
    temp.setNoSold(0);
    temp.setPrice(0);
    temp.setNoOfStock(0);

    vector<unsigned long> searchResult;
    //-- Insert block of code to execute -------//
    if (_dbMode) {
        _db_vec.search_by_prod(temp, searchResult);
    }
    //-- End insert block of code to execute --//
    //-- Insert block of code to execute -------//
    if (!_dbMode) {
        searchResult.clear();
        _db_list.search_by_prod(temp, searchResult);
    }
    //-- End insert block of code to execute --//
    return searchResult.empty();

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, runs the file batchjob.txt and processes all
 * instructions accordingly, in addition to updating the database.
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Logic::run_batch() 
{
    update_gui_status("Running batch job", 2);

    BatchJob batch(this);
    batch.start();

    update_gui_status("Batch job completed", 2);
    call_wait();

    return true;
}

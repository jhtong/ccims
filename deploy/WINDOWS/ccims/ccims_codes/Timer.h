/**
 * @file Timer.h
 * @brief The Timer class is an object used for timing processes.  It
 * is accurate up to the nano-second scale.  It is configurable with
 * four options, from nano-seconds to up to seconds.
 * @author Joel Haowen TONG, Leslie Tan
 * @version 0.0.002
 * @date 2013-03-16
 */
/* Copyright (c) <2012> <C01-05>
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef _TIMER_2_H
#define _TIMER_2_H

#include <string>
#include <chrono>
#include "TimerFlags.h"

using namespace std;

namespace common {
    namespace utils {

        /* ----------------------------------------------------------------*/
        /**
         * @brief The Timer class is an object used for timing processes.  It
         * is accurate up to the nano-second scale.  It is configurable with
         * four options, from nano-seconds to up to seconds.
         *
         * Note that this file must be compiled with the C++0x standard
         * libraries to run.
         *
         * Example code snippet:
         * @code
         *
#include "Timer.h"
#include <string>
#include <iostream>
#include "TimerFlags.h"

using namespace std;
using namespace common::utils;


int main(int argc, const char *argv[])
{
    Timer a;
    a.set_precision(timer::PREC_MICRO);
    a.reset();
    a.start();
    int b = 2;
    int c = 2;
    int d = 3;
    for (int i = 0; i < 1000; i++) {
        int e = 333;
    }
    a.end();
    cout << a.elapsedTimeStr() << endl;
    
    return 0;
}
         * @endcode
         *
         *
         */
        /* ------------------------------------------------------------------*/
        class Timer 
        {
        public:
            Timer();
            ~Timer();
        
            void start();
            void end();
            void reset();
            unsigned long elapsedTime();
            string elapsedTimeStr();
            bool is_started();
            unsigned short get_precision();
            void set_precision(unsigned short arg);

            

        private:
            std::chrono::time_point<std::chrono::high_resolution_clock>
                                    _start, _end;
            bool _isStarted;
            unsigned short _precision;
        };
    }
    
}
#endif

/**
 * @file FormUI.h
 * @brief This file implements the base class for all Form UIs.
 * @author c01-05
 * @version 0.0.001
 * @date 2013-02-17
 */
/* Copyright (c) <2012> C01-05
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _FORM_UI_H
#define _FORM_UI_H

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Screen.h"


using namespace std;

namespace gui
{
    namespace form 
    {
        class Form;                                         // For circular include

        /* ----------------------------------------------------------------*/
        /**
         * @brief This class is the base class for all Form UI components.  Member
         * functions are meant to be overridden by derived classes, save for the
         * constructor.
         */
        /* ------------------------------------------------------------------*/
        class FormUI
        {
            public:
                /* Some stuff here */
                FormUI(unsigned int height,                 // Create UI with dimensions height, width, y, x
                        unsigned int width,
                        unsigned int y,
                        unsigned int x,
                        Form* form_ref = NULL);
                ~FormUI();
                virtual void focus();                       // make the object active.  Should 
                virtual void        focus(bool (*actionFn)(vector<string>));

                                                            // call an infinite while loop 
                                                            // until defocus()
                                                            // reference to go to
                                                            // the next object
                virtual void            set_yx(unsigned int y, unsigned int x);
                virtual unsigned int    get_width();
                virtual unsigned int    get_height();
                virtual unsigned int    get_x();
                virtual unsigned int    get_y();

                virtual string          get_data();
                virtual string          get_id();

            protected:
                /* Some stuff here */
                gui::Screen*        _WIN_SELF;                  // Screen instance of current form UI.
                Form*               _FORM;                      // Pointer to FORM (parent FORM)

        };
    }
}
#endif

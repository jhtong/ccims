/**
 * @file main.cpp
 * @brief This is the where main() is located.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-18
 */


#include <stdio.h>
/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
#include <string>

//#include "Gui.h"
#include "Interface.h"

using namespace std;

#ifdef _WIN32
#include "Windows.h"

/* This routine maximizes the window.
 * Necessary for Windows-based systems */
void MaximizeWindow()
{
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
	SMALL_RECT rcWin;
	rcWin.Left = rcWin.Top = 0;
	rcWin.Right = (short)(min(info.dwMaximumWindowSize.X, info.dwSize.X) - 1);
	rcWin.Bottom = (short)(min(info.dwMaximumWindowSize.Y, info.dwSize.Y) - 5);
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, &rcWin);
}
#endif

int main(int argc, const char *argv[])
{
    #ifdef _WIN32
    MaximizeWindow();
    #endif
    common::Interface::get_instance()->start();
    common::Interface::get_instance()->quit();
    return 0;
}

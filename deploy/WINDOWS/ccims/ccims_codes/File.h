/**
 * @file File.h
 * @brief This class handles all the file read and write operations between database and text file
 * @author Yew Kai, Joel Haowen TONG
 * @version 0.0.002
 * @date 2013-03-22
 */

/* Copyright (c) <2012> <C01-05>
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _FILE_H
#define _FILE_H

#include <fstream>
#include <string>
#include <vector>

#include "Product.h"
#include "ProductFlags.h"
#include "Utils.h"
#include "BasicDatabase.h"

using namespace std;
using namespace logic::adt;

namespace logic
{

    class Logic;
	/* ----------------------------------------------------------------*/
    /**
     * @brief This class handles all the file read and write 
     * operations between database and text file.
	 *
     * @details The loading and saving are invoked when user exits the program.
     */
    /* ------------------------------------------------------------------*/
	class File
	{

	public:

		File();
		~File();

		bool load(BasicDatabase& dB);
		bool save(BasicDatabase& dB);

		Product stringToProd(vector<string>& input);
		string prodToString(int hdrIdx, Product* prodPtr);

	};
}

#endif

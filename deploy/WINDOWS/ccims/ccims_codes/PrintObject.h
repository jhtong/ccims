/**
 * @file PrintObject.h
 * @brief This class is an intermediary object that facilitates
 * communication between GUI and Logic objects.  In essence, it is a virtual
 * print object that can be used to  access, for example, a ScrollList object.
 * This abstract data type should be used to display data to GUI objects.  
 *
 * In short, the PrintObject is a 2-D spreadsheet similar to how Postscript
 * files are used by physical printers.
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _PRINT_OBJECT_H
#define _PRINT_OBJECT_H

#include <string>
#include <vector>

using namespace std;


namespace common 
{

    /* --------------------------------------------------------------------------*/
    /**
     * @brief This class is an intermediary object that facilitates
     * communication between GUI and Logic objects.  In essence, it is a virtual
     * print object that can be used to  access, for example, a ScrollList object.
     * This abstract data type should be used to display data to GUI objects.  
     *
     * In short, the PrintObject is a 2-D spreadsheet similar to how Postscript
     * files are used by physical printers.
     * 
     * EXAMPLE
     *
     * @code
     *
     *
     * @endcode
     *
     * @author TONG Haowen Joel
     * @version 0.0.001
     * @date 2013-02-15
     */
    /* ----------------------------------------------------------------------------*/
    class PrintObject 
    {
    public:
        PrintObject();
        ~PrintObject();

        /* Get the Nth reference at the Mth field */
        string              get_field(unsigned long ref, unsigned long field); 
        string              get_field(unsigned long ref, unsigned long field) const; 

        string              insert_at(unsigned long idx, vector<string> args, bool mask = false);
        void                insert(vector<string> args, bool state = false);
        void                make_headers(vector<string> args);
        void                set_mask_at(unsigned long idx, bool state = false);
        bool                get_mask_at(unsigned long idx);

        unsigned long       get_size();
        unsigned long       get_size() const;
        
        long                get_max_fields();
        long                get_max_fields() const;
        
        string              pretty_print(unsigned long max_len, 
                                            unsigned long idx,
                                            vector<float> prop_args);

        /* For copy constructor, copy by reference */
        void                copy(const PrintObject& arg);               // copy routine


    private:
        vector<string>      printList;                                  // Actual 2-D table
        vector<string>      headers;                                    // 1-D list of headers
        vector<bool>        mask_sel;                                   // 1-D Array of select masks.  If TRUE, then the object is selected.  False otherwise.
        unsigned long       MAX_FIELDS;                                          // Max no. of columns

        void                copy_masks(vector<bool>& arg) const;
        void                copy_headers(vector<string>& arg) const;
    };
    

}
#endif

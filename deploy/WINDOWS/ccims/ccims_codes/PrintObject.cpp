/**
 * @file PrintObject.cpp
 * @brief This file contains the implementation for the PrintObject class.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */
#include "PrintObject.h"
#include <string>
#include <cstdarg>
#include <sstream>
#include <algorithm>
#include <iomanip>

#include "Utils.h"

/* --------------------------------------------------------------------------*/
/**
 * @brief Constructor
 */
/* ----------------------------------------------------------------------------*/
common::PrintObject::PrintObject() 
{

}


/* --------------------------------------------------------------------------*/
/**
 * @brief This function copies another PrintObject
 *
 * @param arg PrintObject to copy from
 */
/* ----------------------------------------------------------------------------*/
void common::PrintObject::copy(const PrintObject& arg)
{
    /* proper cleaning */
    this->headers.clear();

    /* Copy headers by reference */
    arg.copy_headers(this->headers);

    /* Copy masks by reference */
    arg.copy_masks(this->mask_sel);

    /* Copy MAX_FIELDS into current MAX_FIELDS */
    this->MAX_FIELDS = arg.get_max_fields();

    /* proper cleaning */
    this->printList.clear();

    for (unsigned long i = 0; i < arg.get_size(); i++) {
        for (long m = 0; m < arg.get_max_fields(); m++) {
            printList.push_back((string) arg.get_field(i,m));
        }
    }
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This internal function copies headers from another mask vector.
 *
 * @param arg the header list to copy to.  This list will be modified.
 */
/* ----------------------------------------------------------------------------*/
void common::PrintObject::copy_headers(vector<string>& arg) const
{
    arg = headers;
}


/* ----------------------------------------------------------------*/
/**
 * @brief This internal function copies masks from another mask vector.
 *
 * @param arg The mask list to copy to.  This list will be modified.
 */
/* ------------------------------------------------------------------*/
void common::PrintObject::copy_masks(vector<bool>& arg) const 
{
    arg = mask_sel;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Destructor
 */
/* ----------------------------------------------------------------------------*/
common::PrintObject::~PrintObject() {};


/* --------------------------------------------------------------------------*/
/**
 * @brief This function gets the contents of a cell located at (ref_num,
 * field_num).
 *
 * @param ref_num The y-column of a table
 * @param field_num The x-column of a table
 *
 * @return The field requested.  If the field does not exist, return
 * ERR_HEADER_BOUNDS.
 */
/* ----------------------------------------------------------------------------*/
string common::PrintObject::get_field(unsigned long ref_num, unsigned long field_num) 
{
    if ((field_num < 0) || (field_num >= MAX_FIELDS)) {
        return (std::string) "ERR_HEADER_BOUNDS";
    }

    if ((ref_num < 0) || (ref_num >=  get_size()) ) {
        return (std::string) "ERR_LEN_BOUNDS";
    }
    unsigned long cur_real_idx = ref_num * MAX_FIELDS + field_num;
    std::string temp = (std::string) printList[cur_real_idx];
    
    /* Return trimmed string */
    return common::utils::trim(temp);
}


string common::PrintObject::get_field(unsigned long ref_num, unsigned long field_num) const
{
    if ((field_num < 0) || (field_num >= MAX_FIELDS)) {
        return (std::string) "ERR_HEADER_BOUNDS";
    }

    if ((ref_num < 0) || (ref_num >=  get_size()) ) {
        return (std::string) "ERR_LEN_BOUNDS";
    }
    unsigned long cur_real_idx = ref_num * MAX_FIELDS + field_num;
    std::string temp = (std::string) printList[cur_real_idx];
    
    //TRIM WHITESPACE
    temp = common::utils::trim(temp);
    
    return temp;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This function pushes an entry of fields into the PrintObject. 
 *
 * @param args A vector of strings, containing data for that entry.  Note that
 * args should have the same number of elements as the number of headers in the
 * print object, else this method will fail.
 * @param state The seleted state mask of the new entry.  If TRUE, then the
 * entry is selected.  FALSE otherwise.
 *
 * Note that all elements passed should be of type std::string.
 *
 */
/* ----------------------------------------------------------------------------*/
void common::PrintObject::insert(vector<string> args, bool state) {
    if (args.size() == get_max_fields() ) {
        for (unsigned int i  = 0; i < args.size(); i++) {
            printList.push_back(std::string(args[i]));

        }
        mask_sel.push_back(state);
    }
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Similar to insert(), expect inserts at a given reference, idx.
 *
 * @param idx Index to insert entry of fields
 * @param args A vector of strings to insert.  See insert() for more information.
 * @param state The seleted state mask of the new entry.  If TRUE, then the
 * entry is selected.  FALSE otherwise.
 *
 * @return SUCCESS or ERR_LEN_BOUNDS
 */
/* ----------------------------------------------------------------------------*/
string common::PrintObject::insert_at(unsigned long idx, vector<string> args, bool state)
{
    if ( (idx < 0) || (idx >= get_size()) ) {
        return (std::string) "ERR_LEN_BOUNDS";
    }

    if (args.size() == get_max_fields() ) {
        for (unsigned int i  = 0; i < args.size(); i++) {
            printList.insert(printList.begin() + 
                            MAX_FIELDS * idx + i, 
                            (std::string) args[i]);

        }
        mask_sel.insert(mask_sel.begin() + idx, 
                        state);
    }
    
    return (std::string) "SUCCESS";
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This gets the number of rows of the PrintObject instance.
 *
 * @return The number of rows in the PrintObject instance.
 */
/* ----------------------------------------------------------------------------*/
unsigned long common::PrintObject::get_size()
{
    return (unsigned long) (printList.size() / MAX_FIELDS);
}


/* --------------------------------------------------------------------------*/
/**
 * @brief const function.  Refer to get_size() for more information.
 *
 * @return The number of rows in the PrintObject instance.
 */
/* ----------------------------------------------------------------------------*/
unsigned long common::PrintObject::get_size() const
{
    return (unsigned long) (printList.size() / MAX_FIELDS);
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Get the maximum number of columns of the PrintObject table
 *
 * @return Get the maximum number of columns of the PrintObject table
 */
/* ----------------------------------------------------------------------------*/
long common::PrintObject::get_max_fields()
{
    return MAX_FIELDS;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Const function for get_max_fields().  See get_max_fields().
 *
 * @return Get the maximum number of columns of the PrintObject table
 */
/* ----------------------------------------------------------------------------*/
long common::PrintObject::get_max_fields() const
{
    return MAX_FIELDS;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Make headers for the PrintObject.  This has to be called before
 * any insert() operation is called.  make_headers() also determines the number
 * of columns in the PrintObject table.
 *
 * Accepts a vector of std::string.  Each element should be a header label.
 *
 * @param args A vector of std::string.  Each element should be a header label.
 */
/* ----------------------------------------------------------------------------*/
void common::PrintObject::make_headers(vector<string> args) 
{
    headers.clear();
    for (unsigned int i  = 0; i < args.size(); i++) {
        headers.push_back(std::string(args[i]));

    }

    /* Set the maximum number of cols */
    MAX_FIELDS = (long) args.size();
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This function pretty-prints an entry, with a given width.
 *
 * @param max_len The width of the printed string.  Should not be less than the
 * number of characters.
 * @param idx The index to retrieve and print
 * @param prop_args A vector of floats.  Each element should be a
 * floating-point number between 0 to 1.  Also, all elements should sum to 1.
 *
 * @return The pretty-printed element, as an std::string.
 */
/* ----------------------------------------------------------------------------*/
string common::PrintObject::pretty_print(unsigned long max_len, 
                                            unsigned long idx,
                                            vector<float> prop_args)
{
    std::stringstream aa;
    std::string temp = "";

    /* Error checking */
    if (prop_args.size() != get_max_fields()) {
        return (std::string) "ERR_INVALID_NUM_FIELDS " + common::utils::itoa(get_max_fields());

    } else {
        float acc = 0;
        for (unsigned int i = 0; i < prop_args.size(); i++) {
            acc += (float) prop_args[i];

        }
        if (acc != 1) {
            return (std::string) "ERR_INVALID_PROP";

        }
    }

    /* means that print out certain row in table */
    if (idx != -1) {
        for (unsigned int i = 0; i < prop_args.size(); i++) {
            unsigned int width = (unsigned int) (max_len * prop_args[i]);
            aa << std::setw(width) << common::utils::truncate(string(get_field(idx, i)), (unsigned long) width);
        }

    /* Else it means print out headers */
    } else {
        for (unsigned int i = 0; i < prop_args.size(); i++) {
            unsigned int width = (unsigned int) (max_len * prop_args[i]);
            aa << std::setw(width) << common::utils::truncate((string) headers[i], (unsigned long) width);
        }
    }

    return aa.str();
}


void common::PrintObject::set_mask_at(unsigned long idx, bool state)
{
    /* For safety */
    if ( (idx >= 0) && (idx < get_size() )  ) {
        mask_sel[idx] = state;
    }

}


bool common::PrintObject::get_mask_at(unsigned long idx)
{
    /* For safety */
    if ( (idx >= 0) && (idx < get_size() )  ) {
        return mask_sel[idx];
    }

    /* For safety */
    return false;

}

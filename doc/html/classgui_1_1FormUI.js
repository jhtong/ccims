var classgui_1_1FormUI =
[
    [ "FormUI", "classgui_1_1FormUI.html#ad13558e0f5d7294d8bebc09f1b6db13c", null ],
    [ "~FormUI", "classgui_1_1FormUI.html#ab929ee076650f97570ef49d306b4f554", null ],
    [ "focus", "classgui_1_1FormUI.html#a77fc9e7e555c131f127bbf0ef44e71a8", null ],
    [ "get_data", "classgui_1_1FormUI.html#a5b2881dde30407f0ba3586e1284f5172", null ],
    [ "get_height", "classgui_1_1FormUI.html#a3c6c79fa675e06d965d3798d4b6bb01a", null ],
    [ "get_width", "classgui_1_1FormUI.html#af335d43dcf986bf9e858ecb1b5d1f5d9", null ],
    [ "get_x", "classgui_1_1FormUI.html#aed27b97feac469b4b4eec4d8aefc363f", null ],
    [ "get_y", "classgui_1_1FormUI.html#ab3c12ac9c8e25412a40d17ed620ec2b0", null ],
    [ "set_yx", "classgui_1_1FormUI.html#a0b2bd6d822c8bdff50b27f4f3f47479a", null ],
    [ "_FORM", "classgui_1_1FormUI.html#a380343e29a041b22cc27a2956e580c1c", null ],
    [ "_WIN_SELF", "classgui_1_1FormUI.html#a6d179855ad0270d497ca59fb6a4fc32e", null ]
];
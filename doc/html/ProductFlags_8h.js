var ProductFlags_8h =
[
    [ "PROD_BARCODE", "namespacelogic.html#a85ff8f3d4523f923faac956c9b7c62a9", null ],
    [ "PROD_CAT", "namespacelogic.html#a3711ed753b5eca045d06a52ec4d09f4b", null ],
    [ "PROD_MANUFACTURER", "namespacelogic.html#ad65d072cbc43506880bbcf4171429673", null ],
    [ "PROD_NAME", "namespacelogic.html#a922681072fa12031e0c87f273d05a88f", null ],
    [ "PROD_PRICE", "namespacelogic.html#a937f427747de24f62bc476ebae554fa8", null ],
    [ "PROD_RESTOCK", "namespacelogic.html#a9e0e3406745d75acb693ed6478db80a7", null ],
    [ "PROD_SOLD", "namespacelogic.html#af118987d8df6244146e35dd6e1cb7e38", null ],
    [ "PROD_STOCK", "namespacelogic.html#a2b1ee912df44a4767f1ebe36cd5d3c62", null ],
    [ "PROD_SUBMIT", "namespacelogic.html#abd8976216b01d137e7050fa77da16124", null ]
];
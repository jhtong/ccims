var classlogic_1_1adt_1_1BasicDatabaseList =
[
    [ "BasicDatabaseList", "classlogic_1_1adt_1_1BasicDatabaseList.html#a43cf88e438d79fa08632fab1bb4e6600", null ],
    [ "~BasicDatabaseList", "classlogic_1_1adt_1_1BasicDatabaseList.html#afa4b4822770ce63981d857eb662dc421", null ],
    [ "delete_at", "classlogic_1_1adt_1_1BasicDatabaseList.html#aeaddf202839119c0225fc63ec4a64baa", null ],
    [ "get", "classlogic_1_1adt_1_1BasicDatabaseList.html#a22e2bf7b6f0b84a4a8e1dfb6bd6e1d5f", null ],
    [ "insert", "classlogic_1_1adt_1_1BasicDatabaseList.html#a8ade193c9ae5ac0e45e8e5574361e4d5", null ],
    [ "isEmpty", "classlogic_1_1adt_1_1BasicDatabaseList.html#a3ff61f8179e8c5f4ff227064c926bda7", null ],
    [ "push", "classlogic_1_1adt_1_1BasicDatabaseList.html#aed50b2b1e6d16a6eaed4528d88a2706b", null ],
    [ "search", "classlogic_1_1adt_1_1BasicDatabaseList.html#af50965b6aaa8ce593e52e6c27b2de758", null ],
    [ "search_by_prod", "classlogic_1_1adt_1_1BasicDatabaseList.html#a502e4824300f78dfb4f54fe404b8f947", null ],
    [ "size", "classlogic_1_1adt_1_1BasicDatabaseList.html#af568ee018934cd1078a5271ce503fef4", null ],
    [ "sort_by", "classlogic_1_1adt_1_1BasicDatabaseList.html#aaed7de867c0ec3a4c9ac269c91935489", null ],
    [ "swap", "classlogic_1_1adt_1_1BasicDatabaseList.html#afad6d18433f47a58854c9618d21bfc58", null ],
    [ "_size", "classlogic_1_1adt_1_1BasicDatabaseList.html#ad6b77d1887ce2e46ddcbdb43bcb021a4", null ],
    [ "_tail", "classlogic_1_1adt_1_1BasicDatabaseList.html#adfa0ec1391ef5ebf6ba69728cb4f7327", null ]
];
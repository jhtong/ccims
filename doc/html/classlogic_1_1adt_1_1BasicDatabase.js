var classlogic_1_1adt_1_1BasicDatabase =
[
    [ "BasicDatabase", "classlogic_1_1adt_1_1BasicDatabase.html#a5770e492c7ba9bb1d60103337382bf45", null ],
    [ "~BasicDatabase", "classlogic_1_1adt_1_1BasicDatabase.html#adaa496d3a5747111e4dd66e93f9f2d44", null ],
    [ "compare", "classlogic_1_1adt_1_1BasicDatabase.html#ad2c444963db128629dcd20c59323bd2d", null ],
    [ "delete_at", "classlogic_1_1adt_1_1BasicDatabase.html#aedd88eff2b9e046ebdb1caf192a82320", null ],
    [ "get", "classlogic_1_1adt_1_1BasicDatabase.html#afc6af4e0edd6f53d4a8cff62775f6ed3", null ],
    [ "insert", "classlogic_1_1adt_1_1BasicDatabase.html#a05dbbb8f0520fe24e566ce4f1c0deec2", null ],
    [ "push", "classlogic_1_1adt_1_1BasicDatabase.html#a15fce0a049dece3d743888c872775bb2", null ],
    [ "restock_for", "classlogic_1_1adt_1_1BasicDatabase.html#adae60a356179d79cbae01655cbed8388", null ],
    [ "search_by", "classlogic_1_1adt_1_1BasicDatabase.html#ae45d003c73cc9f4530166938355fabdc", null ],
    [ "search_by_prod", "classlogic_1_1adt_1_1BasicDatabase.html#a6c00f4896ce892bdaec1b55168673d60", null ],
    [ "size", "classlogic_1_1adt_1_1BasicDatabase.html#aa4148301a5b56fe7837c1fe3f81af3e2", null ],
    [ "sort_by", "classlogic_1_1adt_1_1BasicDatabase.html#aa9d139092fdba4a9ee36580dbfcb5b01", null ],
    [ "specifySale_for", "classlogic_1_1adt_1_1BasicDatabase.html#a972681edab251b5ba56a4185a435975d", null ],
    [ "_sort_order", "classlogic_1_1adt_1_1BasicDatabase.html#aa500a149b7aeb0aa598bbe796ddfbf78", null ],
    [ "_sort_type", "classlogic_1_1adt_1_1BasicDatabase.html#a59e9933b74100f015d43a1052edb83b5", null ]
];
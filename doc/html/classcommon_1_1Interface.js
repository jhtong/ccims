var classcommon_1_1Interface =
[
    [ "get_instance", "classcommon_1_1Interface.html#a9ffb5ab871ce0a7c8bd1355b3ef51461", null ],
    [ "guiToLog_add_item", "classcommon_1_1Interface.html#a0c940812f0cf8a7c19be4c1ae37d6ad1", null ],
    [ "guiToLog_best_manufacturer", "classcommon_1_1Interface.html#ac5e8d33d96abb230eb71cc60d3336907", null ],
    [ "guiToLog_best_product", "classcommon_1_1Interface.html#aeef8fe7fcb665ca9f1120734873b6733", null ],
    [ "guiToLog_best_product_category", "classcommon_1_1Interface.html#a3775590779cd446106fe2d933623badc", null ],
    [ "guiToLog_restock_item", "classcommon_1_1Interface.html#a60d939a2edf00fd21b92becede872eab", null ],
    [ "guiToLog_run_batch", "classcommon_1_1Interface.html#a2f43ae6baae3a2e1d5ca9c010baa2f36", null ],
    [ "guiToLog_scrap_item", "classcommon_1_1Interface.html#aeb13bdc440fcfeb2d36994f2dd2b9012", null ],
    [ "guiToLog_search_terms", "classcommon_1_1Interface.html#a369b4734900a2b04887e6ee259528d6d", null ],
    [ "guiToLog_specify_sales", "classcommon_1_1Interface.html#a1ba074bbc74d7b9307d54bd70ee65da7", null ],
    [ "guiToLog_toggle_dbMode", "classcommon_1_1Interface.html#a918950f077c2cd0ee0ec0abc1e50a8b8", null ],
    [ "guiToLog_validate_unique", "classcommon_1_1Interface.html#aa947b8aead322c2b0bf3837fbd680bd9", null ],
    [ "logToGui_load_file", "classcommon_1_1Interface.html#a36518c9019829ec8beed15a85fd931d5", null ],
    [ "logToGui_redraw_screen", "classcommon_1_1Interface.html#a2c884654c75cd22f361652f49486c2a4", null ],
    [ "logToGui_update_status", "classcommon_1_1Interface.html#aea845c9b1046f782c29a8d4b51796fdc", null ],
    [ "quit", "classcommon_1_1Interface.html#a7ba995236d529ab0aee1229eb94b793d", null ],
    [ "start", "classcommon_1_1Interface.html#afe499839ab91ec5bfda822d959f12833", null ],
    [ "wait", "classcommon_1_1Interface.html#a50f453c852f249d24890272265fb2ac8", null ]
];
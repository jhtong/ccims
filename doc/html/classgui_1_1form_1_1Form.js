var classgui_1_1form_1_1Form =
[
    [ "Form", "classgui_1_1form_1_1Form.html#a545b59750c9d9dc8841da3d7019ff08a", null ],
    [ "~Form", "classgui_1_1form_1_1Form.html#a9cda7cce41e81bfaca51e922d4f9b98f", null ],
    [ "get_data", "classgui_1_1form_1_1Form.html#a7f39a086e1c422451b4b2e6d65d77d70", null ],
    [ "go_next", "classgui_1_1form_1_1Form.html#a73a6231c0872d209d1daa5bcaf94ae25", null ],
    [ "go_prev", "classgui_1_1form_1_1Form.html#a61fdb3d1f518517964a2164a0b5c2a78", null ],
    [ "pop", "classgui_1_1form_1_1Form.html#af0af2df90da78edac8214623e0dd6aa7", null ],
    [ "push", "classgui_1_1form_1_1Form.html#a1f9159c6e18c1710ac567557d2215df5", null ],
    [ "push_btn", "classgui_1_1form_1_1Form.html#aba3f35641e4724409e1b4666c5823d09", null ],
    [ "push_btn", "classgui_1_1form_1_1Form.html#a5f5efd8cf6aa4cc48f25d1b12eaa12fe", null ],
    [ "push_field", "classgui_1_1form_1_1Form.html#a8bd67f00a6db4dbf8b4af1e879700e70", null ],
    [ "push_static_label", "classgui_1_1form_1_1Form.html#a0434571f1b5807d239961d53083ac672", null ],
    [ "set_yx", "classgui_1_1form_1_1Form.html#a3de7f6ac358d0722b0fa6ea368851790", null ],
    [ "set_yx_center", "classgui_1_1form_1_1Form.html#aa849e846adac440c83de680929810c56", null ],
    [ "start", "classgui_1_1form_1_1Form.html#a04becda17ebd0890f735abe9fa39fce7", null ]
];
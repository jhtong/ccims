var classgui_1_1form_1_1FormUI =
[
    [ "FormUI", "classgui_1_1form_1_1FormUI.html#ad13558e0f5d7294d8bebc09f1b6db13c", null ],
    [ "~FormUI", "classgui_1_1form_1_1FormUI.html#ab929ee076650f97570ef49d306b4f554", null ],
    [ "focus", "classgui_1_1form_1_1FormUI.html#a77fc9e7e555c131f127bbf0ef44e71a8", null ],
    [ "focus", "classgui_1_1form_1_1FormUI.html#a2d2cab859b3248561945ecf69c67e5f3", null ],
    [ "get_data", "classgui_1_1form_1_1FormUI.html#a5b2881dde30407f0ba3586e1284f5172", null ],
    [ "get_height", "classgui_1_1form_1_1FormUI.html#a3c6c79fa675e06d965d3798d4b6bb01a", null ],
    [ "get_id", "classgui_1_1form_1_1FormUI.html#ac0b1c3c771c45327d7fa3c7327cea773", null ],
    [ "get_width", "classgui_1_1form_1_1FormUI.html#af335d43dcf986bf9e858ecb1b5d1f5d9", null ],
    [ "get_x", "classgui_1_1form_1_1FormUI.html#aed27b97feac469b4b4eec4d8aefc363f", null ],
    [ "get_y", "classgui_1_1form_1_1FormUI.html#ab3c12ac9c8e25412a40d17ed620ec2b0", null ],
    [ "set_yx", "classgui_1_1form_1_1FormUI.html#a0b2bd6d822c8bdff50b27f4f3f47479a", null ],
    [ "_FORM", "classgui_1_1form_1_1FormUI.html#ae50118ea9f8726113d6675c315131bf3", null ],
    [ "_WIN_SELF", "classgui_1_1form_1_1FormUI.html#a9b377a42e28343d72fe695924e945acc", null ]
];
var classlogic_1_1Product =
[
    [ "Product", "classlogic_1_1Product.html#a847c1d85e67ce387166a597579a55135", null ],
    [ "Product", "classlogic_1_1Product.html#a8de946575ed9642fce1c4d3bf54e42bc", null ],
    [ "~Product", "classlogic_1_1Product.html#abe0afd3bea96d979185ec2cfdf681e6f", null ],
    [ "copy", "classlogic_1_1Product.html#a372dc111141ad4087cfc6ba99e2d671d", null ],
    [ "getBarcode", "classlogic_1_1Product.html#a3f2d13d8363846b8713f6d1632e886ac", null ],
    [ "getCat", "classlogic_1_1Product.html#a33f87d2b8bba2c9a5744aea3f2e04b5a", null ],
    [ "getManufacturer", "classlogic_1_1Product.html#a7d3055d79eca18af14169266534e6fce", null ],
    [ "getName", "classlogic_1_1Product.html#afe337fda9b87862958b1f0934fb7f8cd", null ],
    [ "getNoOfStock", "classlogic_1_1Product.html#ab24d5fe63b9dc8513979a51f17fcd78a", null ],
    [ "getNoSold", "classlogic_1_1Product.html#a36a458f2076a3afac78b917acd02d800", null ],
    [ "getPrice", "classlogic_1_1Product.html#a5cb527ac8c2763bb2aa92cd157c69477", null ],
    [ "setBarcode", "classlogic_1_1Product.html#a89e219cedff9c3073c5da4d4f9e84783", null ],
    [ "setCat", "classlogic_1_1Product.html#a51d426bd49a1ce46b99c82e927670b55", null ],
    [ "setManufacturer", "classlogic_1_1Product.html#ab65a1e3fd0281bafb42ded28c26fec0d", null ],
    [ "setName", "classlogic_1_1Product.html#a9f82878389df228bfe84209019aa34f3", null ],
    [ "setNoOfStock", "classlogic_1_1Product.html#ac2aad42e4743651422f2092c7d44813b", null ],
    [ "setNoSold", "classlogic_1_1Product.html#a3817cc55454ccf57513e5914dd33042d", null ],
    [ "setPrice", "classlogic_1_1Product.html#a654417fa179433d05c4e01fc1b0f8668", null ]
];
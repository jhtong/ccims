var namespaces =
[
    [ "ColorPairs", "namespaceColorPairs.html", "namespaceColorPairs" ],
    [ "common", "namespacecommon.html", "namespacecommon" ],
    [ "common::errorChecker", "namespacecommon_1_1errorChecker.html", "namespacecommon_1_1errorChecker" ],
    [ "common::utils", "namespacecommon_1_1utils.html", "namespacecommon_1_1utils" ],
    [ "common::utils::form", "namespacecommon_1_1utils_1_1form.html", "namespacecommon_1_1utils_1_1form" ],
    [ "common::utils::timer", "namespacecommon_1_1utils_1_1timer.html", "namespacecommon_1_1utils_1_1timer" ],
    [ "gui", "namespacegui.html", "namespacegui" ],
    [ "gui::form", "namespacegui_1_1form.html", "namespacegui_1_1form" ],
    [ "logic", "namespacelogic.html", "namespacelogic" ],
    [ "logic::adt", "namespacelogic_1_1adt.html", "namespacelogic_1_1adt" ]
];
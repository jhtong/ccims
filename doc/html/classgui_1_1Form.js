var classgui_1_1Form =
[
    [ "Form", "classgui_1_1Form.html#a9ce78ac92bc5474ebae3a15bee684a9f", null ],
    [ "~Form", "classgui_1_1Form.html#a9cda7cce41e81bfaca51e922d4f9b98f", null ],
    [ "get_data", "classgui_1_1Form.html#a7f39a086e1c422451b4b2e6d65d77d70", null ],
    [ "go_next", "classgui_1_1Form.html#a73a6231c0872d209d1daa5bcaf94ae25", null ],
    [ "go_prev", "classgui_1_1Form.html#a61fdb3d1f518517964a2164a0b5c2a78", null ],
    [ "make_data", "classgui_1_1Form.html#a33a9bd1e0026b37d305618c96998f288", null ],
    [ "pop", "classgui_1_1Form.html#af0af2df90da78edac8214623e0dd6aa7", null ],
    [ "push", "classgui_1_1Form.html#a944ecbb10bec1bab7624eb87e13fa772", null ],
    [ "push_btn", "classgui_1_1Form.html#aba3f35641e4724409e1b4666c5823d09", null ],
    [ "push_field", "classgui_1_1Form.html#a87ac2aef5525be710958a0d42e88426c", null ],
    [ "push_static_label", "classgui_1_1Form.html#a0434571f1b5807d239961d53083ac672", null ],
    [ "set_yx", "classgui_1_1Form.html#a3de7f6ac358d0722b0fa6ea368851790", null ],
    [ "set_yx_center", "classgui_1_1Form.html#aa849e846adac440c83de680929810c56", null ],
    [ "start", "classgui_1_1Form.html#a04becda17ebd0890f735abe9fa39fce7", null ]
];
var namespacecommon_1_1utils_1_1timer =
[
    [ "PREC_MICRO", "namespacecommon_1_1utils_1_1timer.html#a8339ef594e9928c80d7fc3908ce96292", null ],
    [ "PREC_MILI", "namespacecommon_1_1utils_1_1timer.html#a696c79d8c0677968a8eac2a5830e3da0", null ],
    [ "PREC_NANO", "namespacecommon_1_1utils_1_1timer.html#ac9418b09a18c1922c165f7e6a2dc0dd3", null ],
    [ "PREC_SECS", "namespacecommon_1_1utils_1_1timer.html#aa08e90160a633c741135c7bbeb316c85", null ],
    [ "PREC_UNSET", "namespacecommon_1_1utils_1_1timer.html#acf6787de6f8e0939088a839c12e9d048", null ],
    [ "TIMER_ERROR", "namespacecommon_1_1utils_1_1timer.html#ae7c248bd6fda6a04a58497bc7f60e546", null ]
];
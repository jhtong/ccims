var classLogic =
[
    [ "Logic", "classLogic.html#a65e13b69a0b4ae0c31be94bcfe11898a", null ],
    [ "~Logic", "classLogic.html#a406131db5b87e8d4b396aec37f3c1f69", null ],
    [ "addProduct", "classLogic.html#a2c522ce7c0494e1c5d4bacd23a3e7db6", null ],
    [ "delProduct", "classLogic.html#a13c537e8235fe04d4dac5e625990ea6f", null ],
    [ "restock", "classLogic.html#a6c6c9a0edbc8b25e6f5ec373d0d7900a", null ],
    [ "searchProduct", "classLogic.html#a848b6aee25eff66bf3d0f86a8f23c6fd", null ],
    [ "specifySale", "classLogic.html#a2f2fe7671974be3ece5e282da09f7ea2", null ]
];
var classgui_1_1Screen =
[
    [ "Screen", "classgui_1_1Screen.html#a6514a69ae03d4ad53579676872a40dd1", null ],
    [ "~Screen", "classgui_1_1Screen.html#a27e2e015793292ab71c173694d29542d", null ],
    [ "get_height", "classgui_1_1Screen.html#ae1e5c69e971610dfeb5f3abf83aa1a76", null ],
    [ "get_panel", "classgui_1_1Screen.html#a45314ecc1cb1e3c52965d27fb8475338", null ],
    [ "get_width", "classgui_1_1Screen.html#abb0effbfad64ccf7150cd55a8acfe137", null ],
    [ "get_win", "classgui_1_1Screen.html#aaa78bcdab7ccfcf794406eb0a75bedde", null ],
    [ "get_x", "classgui_1_1Screen.html#a917b460b63c03e3167b2ff7eefbc5d29", null ],
    [ "get_y", "classgui_1_1Screen.html#a12fe190aa1de2fd825191a34a8f00786", null ],
    [ "hide", "classgui_1_1Screen.html#a0b3dab7be05f25011c82093f72be653b", null ],
    [ "is_hidden", "classgui_1_1Screen.html#a223841d0184c71ea66fd34bbd8cd27de", null ],
    [ "redraw", "classgui_1_1Screen.html#a1235fb55e21e2e0c0247d465b7f516b3", null ],
    [ "set_x", "classgui_1_1Screen.html#ac8f5e9a2c86f951deda6a1faf552e4b5", null ],
    [ "set_y", "classgui_1_1Screen.html#a6c534060d4f2031803b9a0e1e7aa6039", null ],
    [ "set_yx", "classgui_1_1Screen.html#afe8efc9a713d18975191015b228d1777", null ],
    [ "show", "classgui_1_1Screen.html#a89db9bf9b0ec1ce7cb6f9444544c7705", null ]
];
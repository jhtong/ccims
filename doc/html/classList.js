var classList =
[
    [ "addProduct", "classList.html#af3e666610b2a28158b55e9261a5d0947", null ],
    [ "delProduct", "classList.html#ad054771da5abd6c5b4806320200eb76f", null ],
    [ "loadProducts", "classList.html#aefb78337544515eb609994ee301e419e", null ],
    [ "restock", "classList.html#a024d305266ea83994704f693a0a20cea", null ],
    [ "searchByBarcode", "classList.html#aa7de13517b8ab3e8838c21c215631683", null ],
    [ "searchByCat", "classList.html#abcc1b239d81c725ee332f9ed978a1e81", null ],
    [ "searchByName", "classList.html#a32c6baa9395cbaab5b8273134f4fe616", null ],
    [ "specifySale", "classList.html#afd178488c5c9219e5a9a531461f719b1", null ]
];
/**
 * @file Status.h
 * @brief This class creates the status bar that displays output to the
 * user.  This bar should be located at the bottom of the UI.
 *
 * It is able to display text, and should be directly accessible from the GUI.
 *
 * @author TONG Haowen Joel
 * @version 0.0.001
 * @date 2013-02-15
 */
 /* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */
#pragma once
#ifndef _STATUS_H
#define _STATUS_H

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* End include header of curses library */
#include <string>

#include "Screen.h"


using namespace std;
namespace gui 
{
    class Gui;              // Forward type to break circular include

    /* ----------------------------------------------------------------*/
    /**
     * @brief The status class is an Ncurses UI widget that creates a display
     * box of dynamic display text.
     *
     * Text is accepted in the form of a C++ string.
     * 
     * @author TONG Haowen Joel
     * @version 0.0.001
     * @date 2013-02-15
     */
    /* ------------------------------------------------------------------*/
    class Status
    {
    public:
        Status();
        Status(Gui* root);
        ~Status();
        void            set_root(Gui* root);
        void            print(string arg, unsigned int printType = 0);
        void            redraw();
        void            init();
        void            clear_scr();
        void            quit();
        
        Screen*         get_scr();

    private:
        Screen*         STD_SCR;
        Screen*         STATUS_SCR;
        Gui*            _ROOT;
        unsigned int    _width, _height;
        void            create_win(int height);
        void            free_mem();

    };
}
        
#endif

/**
 * @file TextField.cpp
 * @brief Proposed TextField FormUI component
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-20
 */

/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#include "TextField.h"

/* Button.cpp */

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"
#include "ColorPairs.h"

using namespace gui::form;
using namespace gui;
using namespace std;

/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for the TextField class.  As TextField is a
 * derived class of FormUI, it inherits the methods and constructor of the
 * FormUI class.
 *
 * @param height Height of TextField.  Should be passed as 1.
 * @param width Width of TextField.  A variable number.
 * @param y Y position of TextField instance
 * @param x X position of TextField instance
 * @param form_ref Pointer to the parent form
 * @param label Static label to use
 * @param label_width Width of static label tag.  Used for pretty alignment.
 * @param input initial input text to use.  Defaults to empty if unspecified.
 */
/* ------------------------------------------------------------------*/
TextField::TextField(unsigned int   height,     // Create UI with dimensions height, width, y, x
                    unsigned int    width,
                    unsigned int    y,
                    unsigned int    x,
                    Form*           form_ref,                        
                    string          id,
                    string          label,
                    unsigned int    label_width,
                    string          input) 

            : FormUI(               height, 
                                    width, 
                                    y,
                                    x,
                                    form_ref)
{
    _id          = id;
    _label       = label;
    _input       = input;
    _label_width = label_width;
    _cur_idx     = 0;

    make_display();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Internal function used by TextField to generate the text of the
 * TextField instance (including label and input).
 *
 * @return The pretty-printed text
 */
/* ------------------------------------------------------------------*/
string TextField::get_pretty_label()
{
   stringstream aa;
   aa << setw(_label_width) << _label << ": ";
   return aa.str();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, updates the display.  Similar to redraw() for the Screen
 * class.
 */
/* ------------------------------------------------------------------*/
void TextField::make_display()
{
    string label = get_pretty_label();
    string temp  = label + _input;

    /* Print to screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 
            0, 
            0, 
            "%s", temp.c_str());

    _WIN_SELF->redraw();

    /* This allows one to move the visible cursor */
    wmove(_WIN_SELF->get_win(),
            0, 
            label.size() + _cur_idx);

    /* Update cursor location */
    wrefresh(_WIN_SELF->get_win());
}


/* ----------------------------------------------------------------*/
/**
 * @brief Default destructor for the TextField class
 */
/* ------------------------------------------------------------------*/
TextField::~TextField() {}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, brings the TextField into focus until a suitable
 * keyboard input is pressed.
 */
/* ------------------------------------------------------------------*/
void TextField::focus()
{   
    make_display();

    int keyPressed;

    keypad(_WIN_SELF->get_win(), TRUE);
    
    while(1) {
       wattron(_WIN_SELF->get_win(),
                   COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
        make_display();
        keyPressed = wgetch(_WIN_SELF->get_win());
       
        if ((keyPressed == KEY_UP) 
                || (keyPressed == KEY_DOWN) 
                || (keyPressed == '\n')) {
            wattroff(_WIN_SELF->get_win(),
                        COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
            make_display();
            break;

       } else if (keyPressed > 31 && keyPressed < 126) {
           char ch = keyPressed;
           if ((_cur_idx == 0) && (_input.size() == 0)) {
               _input = string(1,ch);

           } else if (_cur_idx == (_input.size())) {
               _input = _input + string(1,ch);

           } else {
               string tempInput = string(_input);
               tempInput = _input.substr(0, _cur_idx) 
                        + string(1,ch)
                        + _input.substr(_cur_idx, _input.size() - _cur_idx);
               _input = tempInput;
           
           }
           _cur_idx++;

       } else if (keyPressed == KEY_LEFT && 
                    _cur_idx != 0) {
           _cur_idx--;

       } else if (keyPressed == KEY_RIGHT &&
                    _cur_idx != (_input.size())) {
            _cur_idx++;

       } else if (keyPressed == KEY_BACKSPACE || keyPressed == 8) {
           if ((_cur_idx == 0) && (_input.size() == 0)) {

           } else if (_cur_idx == (_input.size())) {
               _input = _input.substr(0, _input.size() - 1);
               _cur_idx--;

           } else {
               _input = _input.substr(0, _cur_idx - 1) 
                        + _input.substr(_cur_idx + 1, _input.size());
               _cur_idx--;
           
           }

       
       }

    }

    /* Call the PARENT FORM to go to the next object on stack */
    if (keyPressed == KEY_UP)
    {
        _FORM->go_prev();

    } else if ((keyPressed == KEY_DOWN) || (keyPressed == '\n')) {
        _FORM->go_next();
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the data keyed in by the user.  If the string is
 * empty, the function returns an empty string.
 *
 * @return The input keyed in by the user.
 */
/* ------------------------------------------------------------------*/
string TextField::get_data()
{
    return _input;
}


/* ----------------------------------------------------------------*/
/**
 * @brief This function returns the ID of the TextField.
 *
 * @return The label of the TextField instance
 */
/* ------------------------------------------------------------------*/
string TextField::get_id()
{
    return _id;
}

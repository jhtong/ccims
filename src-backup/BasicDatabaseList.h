/**
* @file BasicDatabaseList.h
* @brief This file forms the base class for implementations of the derived
* classes of vector or linked list implementations of databases.
* @author Joel Haowen TONG
* @version 0.0.002
* @date 2013-03-07
*/
/* Copyright (c) <2012> <TONG Haowen Joel> 

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following 
conditions: 

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#ifndef _BASIC_DATABASE_LIST
#define _BASIC_DATABASE_LIST

#include <string>
#include <iostream>
#include <regex>

#include "BasicDatabase.h"
#include "Product.h"
#include "ProductFlags.h"
#include "AdtFlags.h"
#include "Utils.h"

using namespace std;


namespace logic
{
	namespace adt
	{
		/* ----------------------------------------------------------------*/
		/**
		* @brief The BasicDatabase class is the base class for all derived
		* ADTs that are meant to work as a database, in the ccims logic.
		* As this is an empty skeleton, all derived should implement the
		* functionalities below.
		*/
		/* ------------------------------------------------------------------*/
		class BasicDatabaseList : public BasicDatabase
		{
		public:
			BasicDatabaseList();
			~BasicDatabaseList();
			bool				insert_at(unsigned long idx, 
				logic::Product product);
			bool				sort_by(string header = "", string sort_order = "");
			bool				insert(logic::Product product);

			Product* 			get(unsigned long idx);
			bool				delete_at(unsigned long idx);

			bool				swap(unsigned long idx1, unsigned long idx2);
			bool				clear();
			bool				push(logic::Product product);
			bool				pop();

			unsigned long 		size();
			bool				isEmpty();
			void				print();

			bool                search(string term, string search_type, vector<int> &index);
			bool                search_by_prod(Product term, vector<unsigned long>& idx_vector);

		protected:
			/* data */
			struct ProductList
			{
				Product Prod;
				ProductList *next;
			};
			ProductList *_tail;
			unsigned long _size;

		};
	}
}
#endif

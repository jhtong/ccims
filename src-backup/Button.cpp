/**
 * @file Button.cpp
 * @brief Implementation file for Button class
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
#include "Button.h"

/* Button.cpp */

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"
#include "Form_constants.h"
#include "ColorPairs.h"

using namespace gui::form;
using namespace gui;
using namespace std;


/* ----------------------------------------------------------------*/
/**
 * @brief Default constructor for the Button class.  When called, 
 *
 * @param height Height of Button.  Should be passed as 1.
 * @param width Width of Button.  A variable number.
 * @param y Y position of Button instance
 * @param x X position of Button instance
 * @param label Label to use for Button instance
 * @param form_ref Pointer to the parent form
 */
/* ------------------------------------------------------------------*/
Button::Button(unsigned int         height,     // Create UI with dimensions height, width, y, x
                    unsigned int    width,
                    unsigned int    y,
                    unsigned int    x,
                    string          label,
                    Form* form_ref)
                    : FormUI(height, 
                                width, 
                                y,
                                x,
                                form_ref) 
{
    _label = label;
    _action = NULL;
    make_display();
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the Button instance
 */
/* ------------------------------------------------------------------*/
Button::~Button() {
    _action = NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, creates the display for the Button class
 */
/* ------------------------------------------------------------------*/
void Button::make_display()
{
    string temp = '[' + _label  + ']';
    /* Print to screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 
            0, 
            (_WIN_SELF->get_width() - temp.size())  / 2, 
            "%s", temp.c_str());
    _WIN_SELF->redraw();

    /* Update cursor location */
    wrefresh(_WIN_SELF->get_win());

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the data stored by the Button.  Returns form::EMPTY.
 *
 * @return form::EMPTY
 */
/* ------------------------------------------------------------------*/
string Button::get_data()
{
    return form::EMPTY;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, brings the Button instance to focus, until a suitable
 * escape key is pressed.
 */
/* ------------------------------------------------------------------*/
void Button::focus()
{   
    curs_set(0);
    wattron(_WIN_SELF->get_win(),
               COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
    make_display();

    int keyPressed;

    keypad(_WIN_SELF->get_win(), TRUE);
    
    while(1) {
        make_display();
        keyPressed = wgetch(_WIN_SELF->get_win());
       
        if ((keyPressed == KEY_UP) 
                || (keyPressed == KEY_DOWN) 
                || (keyPressed == '\n')) {
            curs_set(1);
            wattroff(_WIN_SELF->get_win(),
                       COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
            make_display();
            break;
        }
    }

    /* Call the PARENT FORM to go to the next object on stack */
    if (keyPressed == KEY_UP)
    {
        _FORM->go_prev();

    } else if (keyPressed == KEY_DOWN) {
        _FORM->go_next();

    } else if (keyPressed == '\n') {
        /* If action, then pass data for validation */
        if (_action != NULL) {
            vector<string> formData;
            _FORM->get_data(formData);

            /* If action returns false means form invalid */
            if ( !((*_action)(formData)) ) {
                focus();        // recursive 
            }
        }

        /* Return data here */

    }
}


void Button::focus(bool (*actionFn)(vector<string>))
{   
    curs_set(0);
    wattron(_WIN_SELF->get_win(),
               COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
    make_display();

    int keyPressed;

    keypad(_WIN_SELF->get_win(), TRUE);
    
    while(1) {
        make_display();
        keyPressed = wgetch(_WIN_SELF->get_win());
       
        if ((keyPressed == KEY_UP) 
                || (keyPressed == KEY_DOWN) 
                || (keyPressed == '\n')) {
            curs_set(1);
            wattroff(_WIN_SELF->get_win(),
                       COLOR_PAIR(ColorPairs::COLOR_PAIR_FORM_SEL));
            make_display();
            break;
        }
    }

    /* Call the PARENT FORM to go to the next object on stack */
    if (keyPressed == KEY_UP)
    {
        _FORM->go_prev();

    } else if (keyPressed == KEY_DOWN) {
        _FORM->go_next();

    } else if (keyPressed == '\n') {
        /* Enter key pressed */
        

        /* Else submit data */


    }
}


void Button::set_action(bool (*actionFn)(vector<string>))
{
    _action = actionFn;
}


void Button::remove_action() 
{
    _action = NULL;
}

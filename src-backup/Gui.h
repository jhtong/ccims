/**
 * @file        Gui.h
 * @brief       This class SHOULD be created at first at runtime, to initiate the CCIS GUI.
 * Use start() to start the GUI, and redraw() to refresh the GUI.  To print a
 * message to the status box, use status_print( (string) "String to print").
 *
 * @author      TONG Haowen Joel
 * @version     0.0.001
 * @date        2013-02-15
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _GUI_H
#define _GUI_H

#include <iostream>
#include <string>
#include <stdio.h>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end compile curses definition */

#include "Screen.h"
#include "Status.h"
#include "ScrollList.h"
#include "MenuHandler.h"
#include "Header.h"
#include "PrintObject.h"
#include "Interface.h"


using namespace std;


namespace gui 
{
    /* --------------------------------------------------------------------------*/
    /**
     * @brief       This class SHOULD be created at first at runtime, to initiate the CCIS GUI.
     * Use start() to start the GUI, and redraw() to refresh the GUI.  To print a
     * message to the status box, use status_print( (string) "String to print").
     * @author      TONG Haowen Joel
     * @version     0.0.001
     * @date        2013-02-15
     */
    /* ----------------------------------------------------------------------------*/
    class Gui 
    {
    public:
        Gui();
        ~Gui();
        void                quit();
        void                start();
        void                redraw();
        void                status_print(string arg, unsigned int printType = 0);
        void                update_scrollList(common::PrintObject& arg);
        void                clear_scrollList();
        Screen*             get_scr();
        bool                proc_action();
        
    private:
        unsigned int        cur_focus;                          // returns the index of the currently focused window

        /* DISPLAY WINDOWS */
        Screen*             WIN_STD_SCREEN;                     // for the main screen
        Header*             WIN_TOP_MENU;                       // for the top menubar
        Status*             WIN_BTM_STAT_BOX;                   // for the status box
        ScrollList*         WIN_MID_LST_VIEW;                   // for the list view
        MenuHandler*        HANDLER_MENU;

        unsigned int        pressed_key;                        // Returns the pressed key: used mainly for menu / commands
        void                init();                             // internal function that is called inside start()
        void                free_mem();                         // Internal function that frees memory

    };
}
#endif

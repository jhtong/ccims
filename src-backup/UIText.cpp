/**
 * @file UIText.cpp
 * @brief This file implements the UIText class.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
#include "UIText.h"

#include <vector>
#include <string>

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* end curses declaration */

#include "Form.h"
#include "FormUI.h"
#include "Screen.h"
#include "Form_constants.h"
#include "ColorPairs.h"

using namespace gui::form;
using namespace std;

/* ----------------------------------------------------------------*/
/**
 * @brief 
 *
 * @param height Height of Button.  Should be passed as 1.
 * @param width Width of Button.  A variable number.
 * @param y Y position of Button instance
 * @param x X position of Button instance
 * @param label Static text to display
 * @param form_ref Pointer to the parent form
 */
/* ------------------------------------------------------------------*/
UIText::UIText(unsigned int height,
            unsigned int width,
            unsigned int y,
            unsigned int x,
            string label,
            Form* form_ref)

            : FormUI(height, 
                    width, 
                    y,
                    x,
                    form_ref)
{
    _label = label;
    make_display();

}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the UIText component
 */
/* ------------------------------------------------------------------*/
UIText::~UIText()
{

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, brings UIText into focus.  By default, the parent form
 * is told to ignore focus and go to the next component on the list instead.
 */
/* ------------------------------------------------------------------*/
void UIText::focus()
{
    _FORM->go_next();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, updates and redraws the static text displayed.
 *
 * @param label The new static text to display
 */
/* ------------------------------------------------------------------*/
void UIText::set_label(string label)
{
    _label = label;
    make_display();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the static text stored.
 *
 * @return The static text stored.
 */
/* ------------------------------------------------------------------*/
string UIText::get_label()
{
    return _label;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, redraws the display of the UIText instance.
 */
/* ------------------------------------------------------------------*/
void UIText::make_display()
{
    /* Print to screen */
    wclear(_WIN_SELF->get_win());
    mvwprintw(_WIN_SELF->get_win(), 
            0, 
            0, 
            "%s", _label.c_str());

    _WIN_SELF->redraw();
}

/**
 * @file BasicDatabase.h
 * @brief This file forms the base class for implementations of the derived
 * classes of vector or linked list implementations of databases.
 * @author Joel Haowen TONG
 * @version 0.0.002
 * @date 2013-03-07
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef _BASIC_DATABASE_H
#define _BASIC_DATABASE_H

#include <string>
#include <vector>

#include "Product.h"

using namespace std;


namespace logic
{
    class Product;
    namespace adt
    {
        /* ----------------------------------------------------------------*/
        /**
         * @brief The BasicDatabase class is the base class for all derived
         * ADTs that are meant to work as a database, in the ccims logic.
         * As this is an empty skeleton, all derived should implement the
         * functionalities below.
         */
        /* ------------------------------------------------------------------*/
        class BasicDatabase 
        {
        public:
            BasicDatabase();
            ~BasicDatabase();
            virtual bool            insert_at(unsigned long idx, 
                                                logic::Product product);
            virtual bool            sort_by(string header = "", string sort_order = "");
			virtual bool			insert(logic::Product product);

            virtual Product*        get(unsigned long idx);
            virtual bool            delete_at(unsigned long idx);
			virtual bool			restock_for(unsigned long idx, int val);
			virtual bool			specifySale_for(unsigned long idx, int val);

            virtual unsigned long   search_by(string header, string term);
            virtual bool            search_by_prod(Product term, vector<unsigned long>& idx_vector);

            virtual bool            clear();
            virtual bool            push(const logic::Product& arg);
            virtual bool            pop();
            virtual unsigned long   size();


        protected:
            /* data */
			string _sort_order;
			string _sort_type;
			
			virtual bool compare(logic::Product &prod1, logic::Product &prod2);
        };
    }
}
#endif

#!/usr/bin/env bash

g++ main.cpp ScrollList.h ScrollList.cpp Gui.h Gui.cpp Screen.h Screen.cpp Status.h Status.cpp \
ColorPairs.h  PrintObject.h PrintObject.cpp \
ErrorChecker.h \
Form.h Form.cpp FormUI.h FormUI.cpp Button.h Button.cpp TextField.h \
TextField.cpp UIText.h UIText.cpp Form_constants.h  FormFactory.h \
FormFactory.cpp MenuHandler.h MenuHandler.cpp \
Header.h Header.cpp \
Utils.h \
Product.h Product.cpp \
Logic.h Logic.cpp \
Interface.h Interface.cpp \
ProductFlags.h \
BasicDatabase.h BasicDatabase.cpp \
BasicDatabaseVector.h BasicDatabaseVector.cpp \
BasicDatabaseList.h BasicDatabaseList.cpp \
AdtFlags.h \
Timer.h Timer.cpp TimerFlags.h \
File.h File.cpp \
BatchJob.h BatchJob.cpp \
 -O3 \
 -std=c++0x \
 -lncurses -lpanel \
-o ../deploy/a.out && ../deploy/a.out

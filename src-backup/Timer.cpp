/**
 * @file Timer.cpp
 * @brief This file contains the implementation for the Timer class.
 * @author Joel Haowen TONG and Leslie
 * @version 0.0.002
 * @date 2013-03-16
 */
#include "Timer.h"

#include <string>
#include <chrono>
#include <string>

#include "Utils.h"
#include "TimerFlags.h"


using namespace std;
using namespace common::utils;


/* ----------------------------------------------------------------*/
/**
 * @brief Constructor for the timer class
 */
/* ------------------------------------------------------------------*/
Timer::Timer() {
    _precision = timer::PREC_UNSET;
    reset();
    
}


/* ----------------------------------------------------------------*/
/**
 * @brief Destructor for the timer class
 */
/* ------------------------------------------------------------------*/
Timer::~Timer() {
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, starts the timer, if timer has not been started.
 */
/* ------------------------------------------------------------------*/
void Timer::start()
{
    if (!_isStarted) {
        _start = std::chrono::high_resolution_clock::now();
        _isStarted = true;
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, ends timer, if timer is running.
 */
/* ------------------------------------------------------------------*/
void Timer::end() 
{
    if (_isStarted) {
        _end = std::chrono::high_resolution_clock::now();
        _isStarted = false;
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, stops and resets timer, even if timer is running.
 */
/* ------------------------------------------------------------------*/
void Timer::reset() 
{
    _start = _end = std::chrono::high_resolution_clock::now();
    _isStarted = false;

}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the elapsed time of the timer, as a double.
 * Timer needs to be stopped first.
 *
 * @return elapsed time in seconds, else returns 0.
 */
/* ------------------------------------------------------------------*/
unsigned long Timer::elapsedTime() 
{
    unsigned long long elapsed_seconds;
    if (!_isStarted) {
        if (_precision == timer::PREC_SECS) {
         elapsed_seconds = std::chrono::duration_cast
                                <std::chrono::seconds>
                                (_end - _start).count();

        } else if (_precision == timer::PREC_MILI) {
         elapsed_seconds = std::chrono::duration_cast
                                <std::chrono::milliseconds>
                                (_end - _start).count();

        } else if (_precision == timer::PREC_MICRO) {
         elapsed_seconds = std::chrono::duration_cast
                                <std::chrono::microseconds>
                                (_end - _start).count();

        } else if (_precision == timer::PREC_NANO) {
         elapsed_seconds = std::chrono::duration_cast
                                <std::chrono::nanoseconds>
                                (_end - _start).count();

        }

        return (unsigned long) elapsed_seconds;

    }
    return 0;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns the elapsed time as a string, if timer has been stopped.
 *
 * @return the elapsed time as a C++ string, "ERR" otherwise.  If precision has
 * not been set, returns the C++ string, "UNSET".
 */
/* ------------------------------------------------------------------*/
string Timer::elapsedTimeStr() 
{
    string timeStr = itoa(elapsedTime());
    if (!_isStarted) {
        if (_precision == timer::PREC_SECS) {
            timeStr += " s";

        } else if (_precision == timer::PREC_MILI) {
            timeStr += " ms";

        } else if (_precision == timer::PREC_MICRO) {
            timeStr += " us";
            
        } else if (_precision == timer::PREC_NANO) {
            timeStr += " ns";

        } else {
            timeStr = "UNSET";
        }

        return timeStr;

    }
    return "ERR";
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns whether timer has been stopped.
 *
 * @return TRUE if timer has started, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Timer::is_started() 
{
    return _isStarted;

}


/* ----------------------------------------------------------------*/
/**
 * @brief Set the precision of the Timer instance.  For a list of flags that
 * can be passed, see TimerFlags.h
 *
 * @param arg A flag to pass.  Flags are located in TimerFlags.h
 */
/* ------------------------------------------------------------------*/
void Timer::set_precision(unsigned short arg) 
{
    if (arg == timer::PREC_SECS) {
        _precision = timer::PREC_SECS;

    } else if (arg == timer::PREC_MILI) {
        _precision = timer::PREC_MILI;

    } else if (arg == timer::PREC_MICRO) {
        _precision = timer::PREC_MICRO;
        
    } else if (arg == timer::PREC_NANO) {
        _precision = timer::PREC_NANO;

    } else {
        _precision = timer::PREC_UNSET;
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief Returns the precision.  Compare with the flags in TimerFlags.h to get
 * the precision value.
 *
 * @return An unsigned short, which should be compared with the value in
 * TimerFlags.h.
 */
/* ------------------------------------------------------------------*/
unsigned short Timer::get_precision()
{
    return _precision;
}

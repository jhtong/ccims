/**
* @file BatchJob.cpp
* @brief Implementation file for BatchJob class
* @author YK, Joel Haowen TONG
* @version 0.0.003
* @date 2013-04-03
*/

#include "BatchJob.h"
#include "Logic.h"          // Circular include 

#include <string>
#include <fstream>
#include <vector>

using namespace std;
using namespace logic;

/* --------------------------------------------------------------------------*/
/**
* @brief Constructor
*
*/
/* ----------------------------------------------------------------------------*/
BatchJob::BatchJob()
{
    _batchFile = NULL;
    _errFile = NULL;
}

/* --------------------------------------------------------------------------*/
/**
* @brief Destructor
*
*/
/* ----------------------------------------------------------------------------*/
BatchJob::~BatchJob()
{
	free_mem();
}

/* --------------------------------------------------------------------------*/
/**
* @brief Parameterized constructor
* @param root
*/
/* ----------------------------------------------------------------------------*/
BatchJob::BatchJob(Logic* root)
{
    _batchFile = NULL;
    _errFile = NULL;

	load_root(root);
}

/* --------------------------------------------------------------------------*/
/**
* @brief This method load a logic instance for batch jobs processing
* @param root A pointer to the Logic instance
*/
/* ----------------------------------------------------------------------------*/
void BatchJob::load_root(Logic* root)
{
	_root = root;
}

void BatchJob::free_mem()
{
	_root = NULL;
}


/* ----------------------------------------------------------------*/
/**
* @brief This method will load the batch operations from a text file 
*        into a stack of vector of jobs
*/
/* ------------------------------------------------------------------*/
void BatchJob::loadBatchToStack()
{
	Transaction trans;
	vector<string> tempJob;
	string totalNumOfTrans, id, noOfJobs, details, space;
	int totalTrans, noOfJob;

	if (_batchFile->is_open() && _batchFile->good())
	{
		getline(*_batchFile, totalNumOfTrans);
		_totalTrans = totalTrans = common::utils::str_atoi(totalNumOfTrans);


		for(int i=0; i<totalTrans; i++)
		{
			getline(*_batchFile, space);

			getline(*_batchFile, id);
			trans.id = common::utils::dosToUnix(id);

			getline(*_batchFile, noOfJobs);
			noOfJob = common::utils::str_atoi(noOfJobs);


			for(int i=0; i<noOfJob; i++)
			{	
				getline(*_batchFile, space);

				tempJob = get_job();
				trans.jobs.push_back(tempJob);
			}
			_tr_stack.push(trans);
			trans.jobs.clear();
		}
	}
	_batchFile->close();
	delete _batchFile;
}


/* --------------------------------------------------------------------------*/
/**
* @brief This method will execute all the jobs in the stack till the end
* @return status of execution of batch operations
/* ----------------------------------------------------------------------------*/
bool BatchJob::start()
{
	_batchFile = new ifstream ("batchjobs.txt");

	Transaction trans;
	vector<string> tempJob;

	loadBatchToStack();

	while(!_tr_stack.empty())
	{
		trans = _tr_stack.top();
		_tr_stack.pop();

                /* Display info in progress bar */
                string percentage =common::utils::dtoa((_totalTrans - _tr_stack.size()) / _totalTrans);
		_root->update_gui_status(percentage, 3);

		for(unsigned int i=0; i<trans.jobs.size(); i++)
		{
			if(trans.jobs.at(i).at(0)=="ADD")
			{
				for(unsigned int j=1; j<trans.jobs.at(i).size(); j++)
				{
					tempJob.push_back(trans.jobs.at(i).at(j));
				}

				if(!_root->addProduct(tempJob, false))
				{
					write_error(trans.id, trans.jobs.at(i));
				}
			}
			else if(trans.jobs.at(i).at(0)=="SALE")
			{
				for(unsigned int j=1; j<trans.jobs.at(i).size(); j++)
				{
					tempJob.push_back(trans.jobs.at(i).at(j));
				}

				if(!_root->specifySale(tempJob, false))
				{
					write_error(trans.id, trans.jobs.at(i));
				}
			}
			else if(trans.jobs.at(i).at(0)=="DELETE")
			{
				for(unsigned int j=1; j<trans.jobs.at(i).size(); j++)
				{
					tempJob.push_back(trans.jobs.at(i).at(j));
				}

				if(!_root->delProduct(tempJob, false))
				{
					write_error(trans.id, trans.jobs.at(i));
				}
			}
			else if(trans.jobs.at(i).at(0)=="RESTOCK")
			{
				for(unsigned int j=1; j<trans.jobs.at(i).size(); j++)
				{
					tempJob.push_back(trans.jobs.at(i).at(j));
				}

				if(!_root->restock(tempJob, false))
				{
					write_error(trans.id, trans.jobs.at(i));
				}
			}
			tempJob.clear();
		}
	}

    if (_errFile != NULL) {
        _errFile->close();
        delete _errFile;
        _errFile = NULL;
    }
    
	return true;
}


/* ----------------------------------------------------------------*/
/**
* @brief This method will read a job from the text file and convert it to a vector
*
* @return vector of strings of a job details with header
*/
/* ------------------------------------------------------------------*/
vector<string> BatchJob::get_job()
{
	vector<string> job;
	string op, details;

	getline(*_batchFile, op);
	op = common::utils::dosToUnix(op);
	job.push_back(op);

	if(op == "ADD")
	{	
		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("NAME");
		job.push_back(details); 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("CAT");
		job.push_back(details);

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("BARCODE");
		job.push_back(details); 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("PRICE");
		job.push_back(details); 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("MANUFACTURER");
		job.push_back(details); 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("STOCK");
		job.push_back(details);
	}

	else if(op == "SALE")
	{
		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("BARCODE");
		job.push_back(details);		 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("SOLD");
		job.push_back(details);
	}

	else if(op == "DELETE")
	{
		getline(*_batchFile, details);
		details = common::utils::dosToUnix(details);
		job.push_back("BARCODE");
		job.push_back(details); 
		//_root->update_gui_status("DELETE BARCODE: " + details);
		//_root->call_wait();
	}

	else if(op == "RESTOCK")
	{
		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("BARCODE");
		job.push_back(details); 

		getline(*_batchFile,details);
		details = common::utils::dosToUnix(details);
		job.push_back("RESTOCK");
		job.push_back(details); 
	}

	return job;
}


/* ----------------------------------------------------------------*/
/**
* @brief This method log all the errors occurrence during batch process
*
* @param transaction_id
* @param instruction
*/
/* ------------------------------------------------------------------*/
void BatchJob::write_error(string transaction_id, const vector<string>& instruction)
{
    /* If error file is not already open, create new error file */
    if (_errFile == NULL) {
        _errFile = new ofstream ("log.txt", ios::out);
    }

	string barcode;

	if (_errFile->is_open())
	{
		if(instruction.at(0) == "SALE" || instruction.at(0) == "DELETE" || instruction.at(0) == "RESTOCK")
		{
			barcode = instruction.at(2);
		}
		else if(instruction.at(0) == "ADD")
		{
			barcode = instruction.at(6);
		}

		*_errFile << transaction_id << " " << instruction.at(0) << " " << barcode << endl;

	} 
}

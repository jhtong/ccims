/**
 * @file ScrollList.cpp
 * @brief This file contains the implementation of the ScrollList class.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-28
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following 
conditions: 

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/


#include "Gui.h"                                            // Cyclical dependency

/* Compile curses with respect to OS */
#ifdef __linux__
#include <ncurses.h>
#elif _WIN32
#include <curses.h>
#else
#error OS is not supported
#endif
/* End header include for ncurses */
#include "ScrollList.h"
#include "PrintObject.h"
#include "ColorPairs.h"

#include "Form.h"

#include "Utils.h"
#include "Interface.h"

using namespace std;
using namespace gui;


/* --------------------------------------------------------------------------*/
/**
 * @brief Default contructor for ScrollList
 */
/* ----------------------------------------------------------------------------*/
ScrollList::ScrollList() {}


/* --------------------------------------------------------------------------*/
/**
 * @brief Destructor for ScrollList
 */
/* ----------------------------------------------------------------------------*/
ScrollList::~ScrollList() 
{
    /* Free memory */
    free_mem();
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This contructor creates a ScrollList instance of (height, width)
 * at (y, x).
 *
 * @param root      Reference pointer to parent GUI class
 * @param height    Height of ScrollList
 * @param width     Width of ScrollList
 * @param y         Y position of ScrollList
 * @param x         X position of ScrollList
 */
/* ----------------------------------------------------------------------------*/
ScrollList::ScrollList(Gui* root, 
                        unsigned int height, 
                        unsigned int width, 
                        unsigned int y, 
                        unsigned int x)
{
    set_root(root);
    _height         = height;
    _scroll_height  = height - 1;
    _width          = width;
    _y              = y;
    _x              = x;
    _printObject    = new common::PrintObject();
    ColorPairs::init();
}


/* --------------------------------------------------------------------------*/
/**
 * @brief    Sets the current root that is referenced.
 *
 * @param root  Root to reference
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::set_root(Gui* root)
{
    _ROOT = root;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Frees memory used by components in ScrollList, as well as deletes
 * the tabular window and display.
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::quit()
{
    free_mem();
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Frees memory used by the ScrollList.
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::free_mem()
{
    /* Free memory */
    // DO NOT DELTE ROOT - IT WILL DELETE ITSELF
    delete SCRL_LIST_SCR;
    delete _printObject;

    _ROOT = NULL;
    SCRL_LIST_SCR = NULL;
    _printObject = NULL;
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Redraws the screen.  Call when needed.
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::redraw()
{
    _ROOT->redraw();
    SCRL_LIST_SCR->redraw();
}


/* --------------------------------------------------------------------------*/
/**
 * @brief            Prints a line entry, at the correponding row, to the display.
 *
 * @param y_pos         Row to insert the line entry
 * @param color_state   For alternate coloring and active selection coloring.
 * @param list_sel_pos  Current scroll position, relative to the loaded
 * PrintObject instance.
 * @param msg           The string to display.  Note that this string should be
 * generated from the loaded PrintObject instance's pretty_print() function.
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::print_line(unsigned int y_pos, 
                            unsigned int color_state,
                            unsigned long list_sel_pos,
                            string msg)
{
    if (color_state == 2) {
        wattron(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEL));
        mvwprintw(SCRL_LIST_SCR->get_win(), y_pos, 0, msg.c_str());
        wattroff(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEL));

    /* -1 implies print headers */
    } else if (color_state == -1) {
        wattron(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_HEADER));
        wattron(SCRL_LIST_SCR->get_win(), A_BOLD);
        mvwprintw(SCRL_LIST_SCR->get_win(), 0, 0, msg.c_str());
        wattroff(SCRL_LIST_SCR->get_win(), A_BOLD);
        wattroff(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_HEADER));

    /* Implies that it might be selected */
    } else if (color_state == 3) {
        wattron(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEARCH));
        mvwprintw(SCRL_LIST_SCR->get_win(), y_pos, 0, msg.c_str());
        wattroff(SCRL_LIST_SCR->get_win(), 
                COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEARCH));

    } else {
        if ( (list_sel_pos) % 2 == 0) {
            wattron(SCRL_LIST_SCR->get_win(), 
                    COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_1));
            mvwprintw(SCRL_LIST_SCR->get_win(), y_pos, 0, msg.c_str());
            wattroff(SCRL_LIST_SCR->get_win(), 
                    COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEL));

        } else {
            wattron(SCRL_LIST_SCR->get_win(), 
                    COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_0));
            mvwprintw(SCRL_LIST_SCR->get_win(), y_pos, 0, msg.c_str());
            wattroff(SCRL_LIST_SCR->get_win(), 
                    COLOR_PAIR(ColorPairs::COLOR_PAIR_SCROLL_SEL));
        }
    }
}


/* --------------------------------------------------------------------------*/
/**
 * @brief Clears the screen
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::clear_scr()
{
    if (SCRL_LIST_SCR->get_win() != NULL) {
        wclear(SCRL_LIST_SCR->get_win());
    }
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This function writes the scrolllist content by calling
 * print_line().
 *
 * @param list_start_pos    Start position of first entry of scrolllist,
 * relative to the PrintObject instance's index.
 * @param list_sel_pos      Position of currently selected item, reative to the
 * PrintObject instance's index.
 * @param max_height        Actual height of the ScrollList.
 * @todo find a way to dynamically assign the vector of floats (if at all,
 * necessary).
 */
/* ----------------------------------------------------------------------------*/
/* ----------------------------------------------------------------*/
/**
 * @brief When called, prints the whole PrintObject to screen with the correct
 * focus and bitmasks set (as referenced by the PrintOBject instance.)
 *
 * @param list_start_pos
 * @param list_sel_pos
 * @param max_height
 */
/* ------------------------------------------------------------------*/
void ScrollList::print(unsigned int list_start_pos, 
                        unsigned long list_sel_pos,
                        unsigned short max_height)
{
    /* Print headers */

    vector<float> temp_prop;
    temp_prop.push_back(.50);
    temp_prop.push_back(.50);

    /* Print the header first */
    if (!_proportions.empty()) {
        print_line(0, -1, 0, _printObject->pretty_print(_width,
                                                        -1, 
                                                        _proportions)
                                                        .c_str() );

        for (unsigned int i = 0; 
            (i < (unsigned int) (max_height - 1)) && (i < _printObject->get_size()); 
            i++) {

            /* selected: use forward-branch prediction speedup */
            if ( (i + list_start_pos) == list_sel_pos) {
                print_line(i+1, 2, i, _printObject->pretty_print(_width, 
                                                                list_start_pos + i,
                                                                _proportions)
                                                                .c_str() );

            } else if (_printObject->get_mask_at(list_start_pos + i)) {
                print_line(i+1, 3, i, _printObject->pretty_print(_width, 
                                                                list_start_pos + i,
                                                                _proportions)
                                                                .c_str() );


            /* else selected */
            } else {
                print_line(i+1, 0, i, _printObject->pretty_print(_width, 
                                                                list_start_pos + i,
                                                                _proportions)
                                                                .c_str() );
            }
        }
    }
    redraw();
}


void ScrollList::reset()
{
    box_cur_pos = 0;
    box_cur_min = 0;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Persistent listener to get keypad movements.  Note that when
 * run, enters into an infinite for loop.  Hence, Menu key listeners have
 * to be included in this function call.
 */
/* ------------------------------------------------------------------*/
void ScrollList::run_listener()
{
    keypad(SCRL_LIST_SCR->get_win(), TRUE);
    int ch          = 0;                            // input keycode
    reset();
    print(box_cur_pos, box_cur_min, _height);


    /* Fix for seg fault caused by deleting parent and bubbling for quit */
    bool callForQuit = false;
    
    /* Call this to print output */
    //_ROOT->status_print(string("hello world"));
    
    while(1)
    {
        /* Check for quit and break if necessary */
        if (callForQuit) {
            /* Break and return */
            return;
        }

        ch = wgetch(SCRL_LIST_SCR->get_win());
        /* TEST BLOCK DELETE LATER --------------> */
        //char* ch_t;
        //sprintf(ch_t, "%d", ch);
        //_ROOT->status_print(string(ch_t));
        /* END OF TEXT BLOCK -------------------> */


        if (ch == 92) {
                _ROOT->status_print(string("Command mode"));
                callForQuit = _ROOT->proc_action();

        /* Go to next filtered */
        } else if (ch == 'n') {
            unsigned int new_search_idx = 0;
            _ROOT->status_print(string(common::utils::itoa(get_search_index(true, box_cur_pos))));


            if (get_search_max() >= 1) {
                new_search_idx = get_search_index(true, box_cur_pos);

                if (new_search_idx > box_cur_pos) {
                    if (new_search_idx >= (box_cur_min + _scroll_height)) {
                        box_cur_min = new_search_idx - _scroll_height + 1;
                    }
                
                } else {
                    if (new_search_idx < box_cur_min) {
                        box_cur_min = new_search_idx;
                    }
                }
            }

            box_cur_pos = new_search_idx;


        /* Go to prev filtered */
        } else if (ch == 'N') {
            unsigned int new_search_idx = 0;

            if (get_search_max() >= 1) {
                new_search_idx = get_search_index(false, box_cur_pos);
                if (new_search_idx > box_cur_pos) {
                    if (new_search_idx >= (box_cur_min + _scroll_height)) {
                        box_cur_min = new_search_idx - _scroll_height + 1;
                    }
                
                } else {
                    if (new_search_idx < box_cur_min) {
                        box_cur_min = new_search_idx;
                    }
                }
            }

            box_cur_pos = new_search_idx;



        /* START OF NORMAL EXECUTION BLOCK */
        } else if (ch == KEY_DOWN) {
            if (box_cur_pos == (box_cur_min + _scroll_height - 1) ) {

                if ((box_cur_pos < _printObject->get_size()) && 
                        (box_cur_pos != (_printObject->get_size() - 1))) {
                    box_cur_pos++;
                    box_cur_min++;

                } else if (box_cur_pos == _printObject->get_size()) {
                    // Do nothing
                }

            } else {
                if (box_cur_pos != (_printObject->get_size() - 1)) {
                    box_cur_pos++;
                }

            }


        } else if (ch == KEY_UP) {
            if (box_cur_pos == (box_cur_min) ) {

                if (box_cur_pos > 0) {
                    box_cur_pos--;
                    box_cur_min--;

                } else if (box_cur_pos == 0) {
                    // Do nothing
                }

            } else {
                box_cur_pos--;

            }
        } else if (ch == 'q') {
            return;
        }
        print(box_cur_min, box_cur_pos, _height);
    }
}


/* --------------------------------------------------------------------------*/
/**
 * @brief This function initializes the ScrollList instance.
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::init()
{
    SCRL_LIST_SCR = new Screen(_height, _width, _y, _x);
    redraw();

}


/* --------------------------------------------------------------------------*/
/**
 * @brief Loads a PrintObject instance to display
 *
 * @param arg The PrintObject instance to display
 */
/* ----------------------------------------------------------------------------*/
void ScrollList::load(common::PrintObject arg)
{
    delete _printObject;
    _printObject = NULL;
    _printObject = new common::PrintObject();
    _printObject->copy(arg);
    /* Reset vars and print */
    reset();
    print(box_cur_pos, box_cur_min, _height);
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, sets the proportions of each column according to a float
 * value passed as a vector of floats.  The size of this vector must be equal
 * to the number of columns present.  MUST be called after a PrintObject
 * instance has been loaded, to work properly.
 *
 * @param args The proportions to set, passed a vector of floats.
 */
/* ------------------------------------------------------------------*/
void ScrollList::set_proportions(vector<float> args)
{
    /* For safety */
    if (_printObject != NULL && 
            _printObject->get_max_fields() == args.size()) {
        _proportions = args;
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, returns the number of elements that are in the current
 * search.
 *
 * @return The number of elements that match the search criterion.
 */
/* ------------------------------------------------------------------*/
unsigned int ScrollList::get_search_max()
{
    unsigned int acc = 0;
    for (unsigned long i = 0; i < _printObject->get_size(); i++) {
        if(_printObject->get_mask_at(i)) {
            acc++;
        }
    }
    return acc;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Private function.  When called, gets the next search index to
 * navigate to, for the use of the 'n' and 'N' keys search navigation.
 *
 * @param next TRUE toggles next search result to navigate to, FALSE toggles
 * the previous search result to navigate to.
 * @param cur_pos The current selection.
 *
 * @return The search index to navigate to.
 */
/* ------------------------------------------------------------------*/
unsigned int ScrollList::get_search_index(bool next, unsigned int cur_pos)
{
    unsigned int idx = (unsigned int) cur_pos;

    unsigned long searchNo = get_search_max();

    if (searchNo == 1) {
        for (unsigned long i = 0; _printObject->get_size(); i++) {
            if (_printObject->get_mask_at(i)) {
                return i;
            }
        }

    } else if (searchNo == 0) {
        return 0;
    }

    /* search forward */
    if (next) {
        for (idx = idx + 1; idx < _printObject->get_size(); idx++)
        {
            if (_printObject->get_mask_at(idx) ) {
                return idx;
            }
        }

        /* Not found, iterate the front */
        for (idx = 0; idx < cur_pos; idx++)
        {
            if (_printObject->get_mask_at(idx) ) {
                return idx;
            }
        }

        /* Else return invalid idx, no such term */
        idx = -1;
        return idx;

    
    /* Search backward */
    } else {
        for (idx = idx - 1; idx > 0; idx--)
        {
            if (_printObject->get_mask_at(idx) ) {
                return idx;
            }
        }

        /* Not found, iterate the back */
        for (idx = _printObject->get_size(); idx > cur_pos; idx--)
        {
            if (_printObject->get_mask_at(idx) ) {
                return idx;
            }
        }

        /* Else return invalid idx, no such term */
        idx = -1;
        return idx;
    
    }

    /* For safety, return invalid number */
    return -1;
}

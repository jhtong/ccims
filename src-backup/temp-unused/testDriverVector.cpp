#include <iostream>
#include <string>

#include "Product.h"
#include "BasicDatabaseVector.h"
//#include "BasicDatabaseList.h"

#include "ProductFlags.h"
#include "AdtFlags.h"
#include "File.h"
#include "Timer.h"
#include "TimerFlags.h"

using namespace std;
using namespace logic;
using namespace logic::adt;

int main(int argc, const char *argv[])
{
    Product product;
	File file_prod;
	common::utils::Timer time;
    //BasicDatabaseVector vec;
    BasicDatabaseList vec;

	/*test for time and file
	time.set_precision(common::utils::timer::PREC_SECS);
	time.start();
	file_prod.load(vec);
	//vec.print();
	cout<<"done"<<endl;
	file_prod.save(vec);
	time.end();
	cout<<time.elapsedTime()<<time.elapsedTimeStr()<<endl;
	*/

    /*test for barcode with leading zeros
	product.setName("z");
    product.setPrice(50.90);
    vec.push(product);
    product.setName("z");
    product.setPrice(50.90);
    vec.push(product);


    product.setName("a");
    product.setPrice(22.90);
    vec.push(product);

    product.setName("c");
    product.setPrice(1.20);
    vec.push(product);

    vec.sort_by(PROD_NAME, SORT_ASCENDING);

	Product* tempProd;
	tempProd = vec.get(2);
	cout<<tempProd->getName()<<endl;
    vec.print();
	vec.clear();
	vec.print();

    return 0;
}

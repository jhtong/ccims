/**
 * @file FormFactory.h
 * @brief This is a factory abstraction for generating form objects.  This is
 * made to decouple the form generation from the form UI / Scroll list.
 * @author Joel Haowen TONG
 * @version 0.0.001
 * @date 2013-02-23
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _FORM_FACTORY_H
#define _FORM_FACTORY_H

#include "Form.h"
#include "Gui.h"

using namespace std;

namespace gui 
{
    namespace form 
    {

        /* ----------------------------------------------------------------*/
        /**
         * @brief This class should be instantiated as a singleton class.  Its main
         * purpose is to produce Form objects, using the generate() function.
         * Flags are passed to generate() to produce the appropriate form.
         */
        /* ------------------------------------------------------------------*/
        class FormFactory {
            public:
                FormFactory();
                FormFactory(gui::Gui* root);
                ~FormFactory();

                void            set_root(gui::Gui* root);
                Form*           generate(const int form_opcode,
                                            unsigned int height,
                                            unsigned int width,
                                            unsigned int y,
                                            unsigned int x,
                                            unsigned int padding,
                                            unsigned int field_padding);


            private:
                gui::Gui*            _ROOT;
        };
    }
}

#endif

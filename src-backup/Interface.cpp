/**
 * @file Interface.cpp
 * @brief Implementation for the Interface singleton class
 * @author C01-05
 * @version 0.0.001
 * @date 2013-02-23
 */

#include <string>
#include <vector>

#include "Interface.h"
#include "PrintObject.h"
#include "Gui.h"
#include "Logic.h"

using namespace common;



/* ----------------------------------------------------------------*/
/**
 * @brief Private constructor for Interface.  Not to be called externally.
 */
/* ------------------------------------------------------------------*/
Interface::Interface() {}


/* ----------------------------------------------------------------*/
/**
 * @brief Private destructor for Interface.  Not to be called externally.
 */
/* ------------------------------------------------------------------*/
Interface::~Interface() 
{

}


void Interface::quit() 
{
    //delete interface;
    //interface = NULL;
    /* Free the mem */
    gui->quit();
    delete gui;
    gui = NULL;

    logic->quit();
    delete logic;
    logic = NULL;
}


/* ----------------------------------------------------------------*/
/**
 * @brief Initialize static instances to NULL
 *
 * @param 0
 *
 */
/* ------------------------------------------------------------------*/
Interface*          Interface::interface(0);
gui::Gui*           Interface::gui(0);
logic::Logic*       Interface::logic(0);


/* ----------------------------------------------------------------*/
/**
 * @brief This method should be used to access the Interface singleton class.
 * The singleton can be accessed as follow:
 *
 * @code
 * common::Interface::get_instance()->some_function();
 * @endcode
 *
 * @return A pointer to the Interface singleton class
 */
/* ------------------------------------------------------------------*/
Interface* Interface::get_instance()
{
    if (interface == 0) {
        interface = new Interface();
        gui       = new gui::Gui();
        logic     = new logic::Logic();
    }

    return interface;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, passes a PrintObject from Logic, to the GUI, and redraws
 * the ScrollList.
 *
 * @param new_list the new list to pass.  Note that this list must be complete,
 * and have the relevant search masks toggled (if necessary).
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::logToGui_redraw_screen(PrintObject new_list) 
{
    gui->clear_scrollList();
    gui->update_scrollList(new_list);
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, updates the GUI status bar with a new message status.
 *
 * @param new_status The new status to write
 * @param printType if 0, message printed normally.  If 1, message printed
 * as an error.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool Interface::logToGui_update_status(string new_status, unsigned int printType) 
{
    gui->status_print(new_status, printType);
    return true;
}


/* ----------------------------------------------------------------*/
/**
 *
 * @brief Instructs Logic to add a new item to the List.  Called by components
 * of namespace gui.
 *
 * @param arg The product to add, in a 2 * N vector of type string.  The format
 * of the vector should be in this form:
 *
 * @code
 * {<ID>, <data>, <ID>, <data>, <ID>, <data>, ...}
 * @endcode
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_add_item (vector<string> arg)
{
    logic->addProduct(arg);
    return true;       // stub
}


/* ----------------------------------------------------------------*/
/**
 *
 * @brief Instructs Logic to scrap an item from the List.  Called by components
 * of namespace gui.
 *
 * @param arg The product to scrap, in a 2 * N vector of type string.  The format
 * of the vector should be in this form:
 *
 * @code
 * {<ID>, <data>, <ID>, <data>, <ID>, <data>, ...}
 * @endcode
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_scrap_item (vector<string> arg)
{
    logic->delProduct(arg);
    return true;       // stub
}


/* ----------------------------------------------------------------*/
/**
 *
 * @brief Instructs Logic to specify sales of an item to the List.  Called by components
 * of namespace gui.
 *
 * @param arg The sales specified, in a 2 * N vector of type string.  The format
 * of the vector should be in this form:
 *
 * @code
 * {<ID>, <data>, <ID>, <data>, <ID>, <data>, ...}
 * @endcode
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_specify_sales (vector<string> arg)
{
    logic->specifySale(arg);
    return true;       // stub
}


/* ----------------------------------------------------------------*/
/**
 *
 * @brief Instructs Logic to restock items on the List.  Called by components
 * of namespace gui.
 *
 * @param arg The item to restock, in a 2 * N vector of type string.  The format
 * of the vector should be in this form:
 *
 * @code
 * {<ID>, <data>, <ID>, <data>, <ID>, <data>, ...}
 * @endcode
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_restock_item (vector<string> arg)
{
    logic->restock(arg);
    return false;       // stub
}


/* ----------------------------------------------------------------*/
/**
 *
 * @brief Instructs Logic to search the List.  Called by components
 * of namespace gui.
 *
 * @param arg The item filter used for search, in a 2 * N vector of type string.  The format
 * of the vector should be in this form:
 *
 * @code
 * {<ID>, <data>, <ID>, <data>, <ID>, <data>, ...}
 * @endcode
 *
 * @return TRUE if successful, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_search_terms (vector<string> arg)
{
    logic->searchProduct(arg);
    return false;       // stub
}


/* ----------------------------------------------------------------*/
/**
 * @brief Top level function, used to start the Interface process.
 * MUST be called for CICMS system to work.
 */
/* ------------------------------------------------------------------*/
void Interface::start()
{
    gui->start();
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, toggles the DB mode in Logic class.
 *
 * @return 
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_toggle_dbMode() 
{
    logic->toggle_dbMode();
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, this function waits for key input before returning to
 * normal mode.
 */
/* ------------------------------------------------------------------*/
void Interface::wait() 
{
    getch();
}


void Interface::logToGui_load_file() 
{
    logic->load();
}


bool Interface::guiToLog_best_product() 
{
    logic->sortBestProduct();
    return true;
}


bool Interface::guiToLog_best_manufacturer() 
{
    logic->sortBestManufacturer();
    return true;
}


bool Interface::guiToLog_best_product_category() 
{
    logic->sortBestProductCategory();
    return true;
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, determines if a product barcode is unique.
 *
 * @param arg A 2 * N array of strings to process, from the form.
 *
 * @return TRUE if barcode is unique, FALSE otherwise.
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_validate_unique(vector<string> arg) 
{
    return logic->is_unique(arg);
}


/* ----------------------------------------------------------------*/
/**
 * @brief When called, tells Logic instance to process the batch file and
 * update the system accordingly.
 *
 * @return TRUE if successful, FALSE otherwise
 */
/* ------------------------------------------------------------------*/
bool Interface::guiToLog_run_batch() 
{
    return logic->run_batch();
}

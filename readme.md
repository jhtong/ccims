#/**
 * @file mainpage.h
 * @brief Cover page for Documentation
 * @author C01-05 for CICMS 
 * @version 0.0.001
 * @date 2013-02-27
 */
/* Copyright (c) <2012> <TONG Haowen Joel> 
  
 Permission is hereby granted, free of charge, to any person 
 obtaining a copy of this software and associated documentation 
 files (the "Software"), to deal in the Software without 
 restriction, including without limitation the rights to use, 
 copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the 
 Software is furnished to do so, subject to the following 
conditions: 
 
The above copyright notice and this permission notice shall be 
 included in all copies or substantial portions of the Software. 
  
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @mainpage API Documentation - CICMS C01-05
 *
 * @tableofcontents
 * @image html gui_screenshot.png
 * @image latex gui_screenshot.png "Screenshot of CICMS C01-05" width=5.2cm
 *
 * @section Introduction
 * Welcome to the offical homepage for the documentation of the 
 * CICMS C01-05 API.  An overview of the system can be found in our report, located 
 * in the zipped file in the directory ./doc/report/.  This documentation should be read, prior to
 * accessing the online documentation.
 *
 * @section o Offline PDF 
 * An offline PDF of the online documentation (detailing the API) can be found
 * in the zipped file.  It is named refman.pdf, and can be found in the directory 
 * ./doc/latex/
 *
 * @section Installation
 * Please follow the below for installation instructions:
 *
 *
 *   @subsection LINUX LINUX, UNIX-based and emulated systems, etc.
 *  - Install the ncurses library.  For package managers running apt, this can
 *  be done via the command @code sudo apt-get install ncurses @endcode 
 *
 *  - Run ./src/compile.sh.  This should build and run the program.  You will
 *  need g++.  The resultant build can be found in the deploy/ directory, saved
 *  as a.out.
 *
 *@subsection WINDOWS
    - Install PDCurses.  A useful guide to installing PDcurses can be found at 
 * @link http://www.brotech.co.uk/?p=1 @endlink.  Note that step 15 is wrong; 
 * the lib is called pdcurses.lib, which is located in /pdcurs34/win32/pdcurses.lib
 *
 *  - Compile the solution using the source files located in the src/ directory.
 *
 *  - A sample solution can be found in ./deploy/WINDOWS/ .  Please note,
 *  however, that updated source files should ALWAYS be retrieved from the ./src
 *  directory.
 *
 * @section Key Navigation 
 * The following keys are useful:
 *
 * @code 
 * BACKSLASH + <1 - 5>      - toggle command mode and launch forms
 * BACKSLASH + q OR q       - quit program
 * ARROW keys               - Move up down left right 
 * n                        - Next filtered search result 
 * N                        - Previous filtered search result 
 * @endcode
 *
 * @section Report 
 * A pdf version of the report is located in doc/report/
 *
 * @section Authors 
 * Joel Haowen TONG  \n
 * PEH Km Chai Alex \n
 * LAU Yew Kai \n
 * TAN JianWei Leslie
 *
 *
 */
